Compte rendu 19 octobre:

Discussion et recherche sur les nus:
 En fonction des discussions des scéances précédentes,
 nous avons recherché des nus féminins et masculins sur les périodes romantisme, réalisme,  fauvisme et pointillisme.
 De manière générale on trouve peu de nus masculins à part sur des cas d'études de modèles et cela en format portrait.

Ajout de peintures:
 Nous avons rajouté des peintures dans les périodes fauvisme, pointismes et romantismes.

Amélioration de la qualité des images:
 un grand nombre d'images libre de droit sont en faible résolution,
 suite aux discussions des scéance présedente nous avons choisi de chercher
 les images avec la résolution la plus haute possible quite à ce que les fichier ne soient pas forcément libre de droit.
Le partage de la base de données consistera à partager la liste des liens vers les images utilisées plutôt que les images elles même.



Mise au point un plan un peu plus élaboré :
I. Attention visuelle
	1. Contexte biologique -> top down/bottom up, covert/overt, gase, ...
	2. Modèles formels : cognitive,  etc voir fig 6


II. Modèles esthétiques
	1. Modèles de comparaison des styles
  2. Méthode d'apprentissage de style
  3. Rapprochement avecc l'attention visuelle

III. Expérimentations
	1. Eye tracking (definition)
	2. Protocoles expérimentaux et bases de données existantes

IV. Ouverture
	1. Problématique
		-Mesure de l’importance des méthodes artistiques dans l’attention visuelle
		- Quelle dépendance de la carte de saillance par rapport aux mouvements artistiques
	2. Modèle CNN de Stanford
		- Definition / description
		- Biais
	3. Base de données
		- Description, justification des choix
		- Biais

	4. definition de notre protocole (Pas certain)

Fichier figure 6 : fig6.png
