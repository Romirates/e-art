[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:52
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6700732822	# Message: void.jpg
Fixation L	1	1	6700748452	6702081016	1332564	776.94	374.73	22	71	-1	12.34	12.34
Fixation R	1	1	6700748452	6702081016	1332564	776.94	374.73	22	71	-1	13.52	13.52
Saccade L	1	1	6702081016	6702100884	19868	785.96	412.97	786.90	421.35	0.07	8.53	1.00	3.68	153.03	-118.37	103.22
Saccade R	1	1	6702081016	6702100884	19868	785.96	412.97	786.90	421.35	0.07	8.53	1.00	3.68	153.03	-118.37	103.22
Fixation L	1	2	6702100884	6702717496	616612	793.20	429.90	9	30	-1	13.90	13.90
Fixation R	1	2	6702100884	6702717496	616612	793.20	429.90	9	30	-1	15.29	15.29
