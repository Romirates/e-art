[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8243870862	# Message: void.jpg
Fixation L	1	1	8243881539	8244338887	457348	492.88	726.82	46	34	-1	10.62	10.62
Fixation R	1	1	8243881539	8244338887	457348	492.88	726.82	46	34	-1	12.48	12.48
Saccade L	1	1	8244338887	8244378759	39872	525.97	722.48	799.78	529.42	3.40	230.51	0.50	85.27	5520.02	-4708.71	3617.93
Saccade R	1	1	8244338887	8244378759	39872	525.97	722.48	799.78	529.42	3.40	230.51	0.50	85.27	5520.02	-4708.71	3617.93
Fixation L	1	2	8244378759	8244597497	218738	798.17	482.58	28	65	-1	10.67	10.67
Fixation R	1	2	8244378759	8244597497	218738	798.17	482.58	28	65	-1	12.07	12.07
Saccade L	1	2	8244597497	8244617421	19924	792.35	465.29	789.45	457.13	0.08	8.75	1.00	4.14	144.36	-153.50	99.72
Saccade R	1	2	8244597497	8244617421	19924	792.35	465.29	789.45	457.13	0.08	8.75	1.00	4.14	144.36	-153.50	99.72
Fixation L	1	3	8244617421	8245194228	576807	802.17	485.10	30	63	-1	11.49	11.49
Fixation R	1	3	8244617421	8245194228	576807	802.17	485.10	30	63	-1	12.43	12.43
Saccade L	1	3	8245194228	8245214094	19866	819.92	511.46	821.92	516.11	0.10	7.55	0.00	5.24	131.59	-64.35	80.47
Saccade R	1	3	8245194228	8245214094	19866	819.92	511.46	821.92	516.11	0.10	7.55	0.00	5.24	131.59	-64.35	80.47
Fixation L	1	4	8245214094	8245870457	656363	819.68	543.93	7	42	-1	12.11	12.11
Fixation R	1	4	8245214094	8245870457	656363	819.68	543.93	7	42	-1	13.00	13.00
