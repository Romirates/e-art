[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7713870975	# Message: void.jpg
Saccade L	1	1	7713875760	7713975254	99494	665.38	909.24	884.13	897.60	3.46	122.63	1.00	34.78	2833.81	-2988.03	1551.08
Saccade R	1	1	7713875760	7713975254	99494	665.38	909.24	884.13	897.60	3.46	122.63	1.00	34.78	2833.81	-2988.03	1551.08
Fixation L	1	1	7713975254	7715407321	1432067	882.74	888.68	19	62	-1	10.88	10.88
Fixation R	1	1	7713975254	7715407321	1432067	882.74	888.68	19	62	-1	12.53	12.53
Saccade L	1	2	7715407321	7715447067	39746	878.84	893.33	881.62	574.20	3.11	182.20	0.50	78.20	4169.60	-3437.50	3582.40
Saccade R	1	2	7715407321	7715447067	39746	878.84	893.33	881.62	574.20	3.11	182.20	0.50	78.20	4169.60	-3437.50	3582.40
Fixation L	1	2	7715447067	7715626184	179117	870.62	529.51	18	64	-1	10.55	10.55
Fixation R	1	2	7715447067	7715626184	179117	870.62	529.51	18	64	-1	12.80	12.80
Saccade L	1	3	7715626184	7715665934	39750	863.06	509.86	786.19	324.53	1.90	118.32	0.50	47.76	2911.91	-2518.75	2520.09
Saccade R	1	3	7715626184	7715665934	39750	863.06	509.86	786.19	324.53	1.90	118.32	0.50	47.76	2911.91	-2518.75	2520.09
Fixation L	1	3	7715665934	7715864800	198866	769.83	325.06	21	7	-1	10.81	10.81
Fixation R	1	3	7715665934	7715864800	198866	769.83	325.06	21	7	-1	12.95	12.95
