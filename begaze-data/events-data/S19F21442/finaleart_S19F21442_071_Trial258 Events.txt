[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:42
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7762781266	# Message: void.jpg
Fixation L	1	1	7762785637	7762885007	99370	865.02	559.34	2	22	-1	12.45	12.45
Fixation R	1	1	7762785637	7762885007	99370	865.02	559.34	2	22	-1	13.07	13.07
Saccade L	1	1	7762885007	7762925070	40063	866.56	570.31	659.20	532.12	1.97	111.23	1.00	49.23	2457.17	-2452.47	2399.02
Saccade R	1	1	7762885007	7762925070	40063	866.56	570.31	659.20	532.12	1.97	111.23	1.00	49.23	2457.17	-2452.47	2399.02
Fixation L	1	2	7762925070	7763939203	1014133	652.22	531.97	20	47	-1	11.56	11.56
Fixation R	1	2	7762925070	7763939203	1014133	652.22	531.97	20	47	-1	12.47	12.47
Blink L	1	1	7763939203	7764138072	198869
Blink R	1	1	7763939203	7764138072	198869
Fixation L	1	3	7764138072	7764734794	596722	802.12	542.25	11	47	-1	10.09	10.09
Fixation R	1	3	7764138072	7764734794	596722	802.12	542.25	11	47	-1	13.10	13.10
Saccade L	1	2	7764734794	7764774667	39873	796.83	537.91	871.18	546.24	0.91	73.64	0.50	22.93	1732.56	-19.09	583.89
Saccade R	1	2	7764734794	7764774667	39873	796.83	537.91	871.18	546.24	0.91	73.64	0.50	22.93	1732.56	-19.09	583.89
