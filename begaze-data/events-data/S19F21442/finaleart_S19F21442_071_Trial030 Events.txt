[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5537236482	# Message: void.jpg
Saccade L	1	1	5537254130	5537353625	99495	1184.71	532.05	1385.20	849.90	6.26	241.45	0.80	62.87	6004.49	-5603.09	2622.87
Saccade R	1	1	5537254130	5537353625	99495	1184.71	532.05	1385.20	849.90	6.26	241.45	0.80	62.87	6004.49	-5603.09	2622.87
Fixation L	1	1	5537353625	5537651980	298355	1406.37	845.35	26	21	-1	14.11	14.11
Fixation R	1	1	5537353625	5537651980	298355	1406.37	845.35	26	21	-1	17.38	17.38
Saccade L	1	2	5537651980	5537691727	39747	1401.39	845.12	1069.78	516.16	3.98	271.35	0.50	100.10	6733.24	-6575.39	5678.09
Saccade R	1	2	5537651980	5537691727	39747	1401.39	845.12	1069.78	516.16	3.98	271.35	0.50	100.10	6733.24	-6575.39	5678.09
Fixation L	1	2	5537691727	5538586811	895084	1036.42	496.12	47	51	-1	12.63	12.63
Fixation R	1	2	5537691727	5538586811	895084	1036.42	496.12	47	51	-1	15.96	15.96
Saccade L	1	3	5538586811	5538606693	19882	1022.34	526.52	1022.64	527.75	0.07	7.79	1.00	3.58	152.96	-57.30	82.18
Saccade R	1	3	5538586811	5538606693	19882	1022.34	526.52	1022.64	527.75	0.07	7.79	1.00	3.58	152.96	-57.30	82.18
Fixation L	1	3	5538606693	5539223293	616600	1024.33	538.63	13	25	-1	13.92	13.92
Fixation R	1	3	5538606693	5539223293	616600	1024.33	538.63	13	25	-1	18.13	18.13
