[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6633481370	# Message: void.jpg
Fixation L	1	1	6633500029	6634355219	855190	1097.51	192.29	22	52	-1	9.79	9.79
Fixation R	1	1	6633500029	6634355219	855190	1097.51	192.29	22	52	-1	11.75	11.75
Saccade L	1	1	6634355219	6634394970	39751	1088.28	179.66	840.09	180.10	2.08	192.63	1.00	52.27	4789.15	-4317.78	2952.14
Saccade R	1	1	6634355219	6634394970	39751	1088.28	179.66	840.09	180.10	2.08	192.63	1.00	52.27	4789.15	-4317.78	2952.14
Fixation L	1	2	6634394970	6635349676	954706	834.22	241.50	13	86	-1	10.97	10.97
Fixation R	1	2	6634394970	6635349676	954706	834.22	241.50	13	86	-1	13.30	13.30
Saccade L	1	2	6635349676	6635369668	19992	826.77	247.65	824.82	258.42	0.15	12.86	1.00	7.29	11.24	-248.72	101.54
Saccade R	1	2	6635349676	6635369668	19992	826.77	247.65	824.82	258.42	0.15	12.86	1.00	7.29	11.24	-248.72	101.54
Fixation L	1	3	6635369668	6635469046	99378	826.38	259.37	5	9	-1	11.48	11.48
Fixation R	1	3	6635369668	6635469046	99378	826.38	259.37	5	9	-1	13.90	13.90
