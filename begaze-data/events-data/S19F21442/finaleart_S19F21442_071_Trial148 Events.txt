[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6743551858	# Message: void.jpg
Fixation L	1	1	6743551960	6744088936	536976	986.64	506.77	15	77	-1	10.41	10.41
Fixation R	1	1	6743551960	6744088936	536976	986.64	506.77	15	77	-1	11.56	11.56
Blink L	1	1	6744088936	6744307811	218875
Blink R	1	1	6744088936	6744307811	218875
Fixation L	1	2	6744307811	6745182879	875068	716.75	682.95	32	67	-1	11.42	11.42
Fixation R	1	2	6744307811	6745182879	875068	716.75	682.95	32	67	-1	13.39	13.39
Saccade L	1	1	6745182879	6745202887	20008	718.14	681.51	842.80	630.22	0.93	136.34	1.00	46.65	3400.74	-3043.56	2191.81
Saccade R	1	1	6745182879	6745202887	20008	718.14	681.51	842.80	630.22	0.93	136.34	1.00	46.65	3400.74	-3043.56	2191.81
Fixation L	1	3	6745202887	6745540996	338109	875.61	603.88	43	40	-1	12.08	12.08
Fixation R	1	3	6745202887	6745540996	338109	875.61	603.88	43	40	-1	14.04	14.04
