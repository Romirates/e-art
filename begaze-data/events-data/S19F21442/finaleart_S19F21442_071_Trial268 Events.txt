[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:46
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8225515871	# Message: void.jpg
Fixation L	1	1	8225522962	8225761598	238636	582.21	242.14	17	62	-1	11.89	11.89
Fixation R	1	1	8225522962	8225761598	238636	582.21	242.14	17	62	-1	14.41	14.41
Saccade L	1	1	8225761598	8225821195	59597	593.82	289.46	1258.04	436.61	7.35	286.71	0.67	123.27	6183.74	-7071.79	4758.16
Saccade R	1	1	8225761598	8225821195	59597	593.82	289.46	1258.04	436.61	7.35	286.71	0.67	123.27	6183.74	-7071.79	4758.16
Fixation L	1	2	8225821195	8225960437	139242	1259.36	429.11	6	32	-1	13.10	13.10
Fixation R	1	2	8225821195	8225960437	139242	1259.36	429.11	6	32	-1	14.09	14.09
Blink L	1	1	8225960437	8226159421	198984
Blink R	1	1	8225960437	8226159421	198984
Fixation L	1	3	8226159421	8227511868	1352447	815.42	543.17	20	69	-1	10.65	10.65
Fixation R	1	3	8226159421	8227511868	1352447	815.42	543.17	20	69	-1	12.94	12.94
