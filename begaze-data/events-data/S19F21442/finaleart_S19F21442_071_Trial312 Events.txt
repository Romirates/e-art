[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:52:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8359802869	# Message: void.jpg
Fixation L	1	1	8359820981	8360099329	278348	958.10	151.53	19	40	-1	9.78	9.78
Fixation R	1	1	8359820981	8360099329	278348	958.10	151.53	19	40	-1	12.05	12.05
Saccade L	1	1	8360099329	8360139208	39879	958.57	171.36	734.25	254.72	2.23	170.81	0.50	55.88	3962.93	-3887.07	2764.64
Saccade R	1	1	8360099329	8360139208	39879	958.57	171.36	734.25	254.72	2.23	170.81	0.50	55.88	3962.93	-3887.07	2764.64
Fixation L	1	2	8360139208	8361133676	994468	725.46	291.79	22	75	-1	10.66	10.66
Fixation R	1	2	8360139208	8361133676	994468	725.46	291.79	22	75	-1	13.13	13.13
Saccade L	1	2	8361133676	8361153540	19864	725.37	319.57	728.65	326.90	0.07	8.13	1.00	3.62	161.95	-58.37	92.58
Saccade R	1	2	8361133676	8361153540	19864	725.37	319.57	728.65	326.90	0.07	8.13	1.00	3.62	161.95	-58.37	92.58
Fixation L	1	3	8361153540	8361790016	636476	740.90	349.70	24	66	-1	11.20	11.20
Fixation R	1	3	8361153540	8361790016	636476	740.90	349.70	24	66	-1	13.77	13.77
