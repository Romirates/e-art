[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:14
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5567782182	# Message: void.jpg
Saccade L	1	1	5567785543	5567825290	39747	954.30	571.28	1300.02	1033.27	5.94	362.40	0.50	149.53	5580.25	-8747.17	4915.44
Saccade R	1	1	5567785543	5567825290	39747	954.30	571.28	1300.02	1033.27	5.94	362.40	0.50	149.53	5580.25	-8747.17	4915.44
Fixation L	1	1	5567825290	5568024179	198889	1300.69	1025.24	7	12	-1	13.32	13.32
Fixation R	1	1	5567825290	5568024179	198889	1300.69	1025.24	7	12	-1	15.14	15.14
Saccade L	1	2	5568024179	5568044035	19856	1305.40	1025.75	1543.73	1032.53	1.57	241.05	1.00	78.97	5862.39	-5677.05	4349.57
Saccade R	1	2	5568024179	5568044035	19856	1305.40	1025.75	1543.73	1032.53	1.57	241.05	1.00	78.97	5862.39	-5677.05	4349.57
Fixation L	1	2	5568044035	5568243017	198982	1609.61	1041.22	77	22	-1	12.60	12.60
Fixation R	1	2	5568044035	5568243017	198982	1609.61	1041.22	77	22	-1	13.97	13.97
Saccade L	1	3	5568243017	5568262892	19875	1619.62	1052.37	1617.22	1058.79	0.14	11.55	1.00	7.20	162.52	-50.02	84.73
Saccade R	1	3	5568243017	5568262892	19875	1619.62	1052.37	1617.22	1058.79	0.14	11.55	1.00	7.20	162.52	-50.02	84.73
Fixation L	1	3	5568262892	5568561253	298361	1607.03	1068.43	20	22	-1	11.87	11.87
Fixation R	1	3	5568262892	5568561253	298361	1607.03	1068.43	20	22	-1	12.96	12.96
