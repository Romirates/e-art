[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6664012246	# Message: void.jpg
Fixation L	1	1	6664031315	6664289914	258599	1244.00	764.69	18	20	-1	9.40	9.40
Fixation R	1	1	6664031315	6664289914	258599	1244.00	764.69	18	20	-1	11.62	11.62
Saccade L	1	1	6664289914	6664468894	178980	1248.19	765.87	840.74	752.88	12.16	218.06	0.22	67.92	5278.48	-3818.36	1626.06
Saccade R	1	1	6664289914	6664468894	178980	1248.19	765.87	840.74	752.88	12.16	218.06	0.22	67.92	5278.48	-3818.36	1626.06
Fixation L	1	2	6664468894	6664866650	397756	843.10	738.50	16	78	-1	9.01	9.01
Fixation R	1	2	6664468894	6664866650	397756	843.10	738.50	16	78	-1	12.48	12.48
Saccade L	1	2	6664866650	6664886512	19862	847.30	755.68	849.94	767.22	0.20	17.53	1.00	10.06	31.65	-138.85	61.43
Saccade R	1	2	6664866650	6664886512	19862	847.30	755.68	849.94	767.22	0.20	17.53	1.00	10.06	31.65	-138.85	61.43
Fixation L	1	3	6664886512	6665542984	656472	854.10	761.36	12	77	-1	9.83	9.83
Fixation R	1	3	6664886512	6665542984	656472	854.10	761.36	12	77	-1	13.72	13.72
Blink L	1	1	6665542984	6665761720	218736
Blink R	1	1	6665542984	6665761720	218736
Fixation L	1	4	6665761720	6666000457	238737	863.63	535.08	22	22	-1	11.81	11.81
Fixation R	1	4	6665761720	6666000457	238737	863.63	535.08	22	22	-1	12.85	12.85
