[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6244636001	# Message: void.jpg
Saccade L	1	1	6244642358	6244682218	39860	463.56	306.72	688.98	380.90	2.57	160.97	1.00	64.40	4024.32	-4007.75	2449.69
Saccade R	1	1	6244642358	6244682218	39860	463.56	306.72	688.98	380.90	2.57	160.97	1.00	64.40	4024.32	-4007.75	2449.69
Fixation L	1	1	6244682218	6246014796	1332578	711.89	418.17	30	65	-1	13.12	13.12
Fixation R	1	1	6244682218	6246014796	1332578	711.89	418.17	30	65	-1	15.45	15.45
Saccade L	1	2	6246014796	6246034670	19874	714.55	433.50	715.10	450.08	0.16	16.78	1.00	8.01	363.54	-165.42	226.35
Saccade R	1	2	6246014796	6246034670	19874	714.55	433.50	715.10	450.08	0.16	16.78	1.00	8.01	363.54	-165.42	226.35
Fixation L	1	2	6246034670	6246631386	596716	721.78	457.82	10	27	-1	13.66	13.66
Fixation R	1	2	6246034670	6246631386	596716	721.78	457.82	10	27	-1	16.15	16.15
