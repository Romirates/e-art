[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:04
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7104026761	# Message: void.jpg
Fixation L	1	1	7104038605	7104376731	338126	1594.29	620.66	86	12	-1	11.18	11.18
Fixation R	1	1	7104038605	7104376731	338126	1594.29	620.66	86	12	-1	11.61	11.61
Saccade L	1	1	7104376731	7104396581	19850	1613.65	612.87	1613.79	608.28	0.08	4.70	1.00	4.07	17.58	-65.69	28.21
Saccade R	1	1	7104376731	7104396581	19850	1613.65	612.87	1613.79	608.28	0.08	4.70	1.00	4.07	17.58	-65.69	28.21
Fixation L	1	2	7104396581	7105212179	815598	1599.79	626.27	46	48	-1	11.30	11.30
Fixation R	1	2	7104396581	7105212179	815598	1599.79	626.27	46	48	-1	12.04	12.04
Saccade L	1	2	7105212179	7105271793	59614	1566.98	639.96	882.36	655.67	7.30	262.28	0.33	122.43	6401.74	-5616.46	4951.09
Saccade R	1	2	7105212179	7105271793	59614	1566.98	639.96	882.36	655.67	7.30	262.28	0.33	122.43	6401.74	-5616.46	4951.09
Fixation L	1	3	7105271793	7105788901	517108	833.47	671.87	55	42	-1	12.09	12.09
Fixation R	1	3	7105271793	7105788901	517108	833.47	671.87	55	42	-1	13.57	13.57
Saccade L	1	3	7105788901	7105848651	59750	830.08	696.60	962.16	544.85	2.84	163.77	0.67	47.56	3892.61	-2835.14	1713.14
Saccade R	1	3	7105788901	7105848651	59750	830.08	696.60	962.16	544.85	2.84	163.77	0.67	47.56	3892.61	-2835.14	1713.14
Fixation L	1	4	7105848651	7106007770	159119	946.34	595.61	20	75	-1	12.28	12.28
Fixation R	1	4	7105848651	7106007770	159119	946.34	595.61	20	75	-1	13.52	13.52
