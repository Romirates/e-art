[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6317918113	# Message: void.jpg
Saccade L	1	1	6317937421	6318036910	99489	1346.67	814.42	1216.79	552.46	6.37	202.21	0.80	63.99	3378.08	-4584.54	1858.40
Saccade R	1	1	6317937421	6318036910	99489	1346.67	814.42	1216.79	552.46	6.37	202.21	0.80	63.99	3378.08	-4584.54	1858.40
Fixation L	1	1	6318036910	6319011475	974565	1184.83	563.23	44	49	-1	12.86	12.86
Fixation R	1	1	6318036910	6319011475	974565	1184.83	563.23	44	49	-1	13.88	13.88
Saccade L	1	2	6319011475	6319031371	19896	1173.16	583.21	1061.74	575.22	0.63	112.99	1.00	31.90	2775.03	-2277.38	1757.99
Saccade R	1	2	6319011475	6319031371	19896	1173.16	583.21	1061.74	575.22	0.63	112.99	1.00	31.90	2775.03	-2277.38	1757.99
Fixation L	1	2	6319031371	6319826948	795577	1048.42	556.83	20	55	-1	13.29	13.29
Fixation R	1	2	6319031371	6319826948	795577	1048.42	556.83	20	55	-1	15.26	15.26
Saccade L	1	3	6319826948	6319906557	79609	1047.64	587.79	909.25	542.69	2.25	102.77	0.25	28.32	2549.85	-1914.70	1055.61
Saccade R	1	3	6319826948	6319906557	79609	1047.64	587.79	909.25	542.69	2.25	102.77	0.25	28.32	2549.85	-1914.70	1055.61
