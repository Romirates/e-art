[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:52:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8353690786	# Message: void.jpg
Fixation L	1	1	8353694748	8354291470	596722	1343.60	274.91	24	74	-1	10.03	10.03
Fixation R	1	1	8353694748	8354291470	596722	1343.60	274.91	24	74	-1	11.32	11.32
Saccade L	1	1	8354291470	8354311353	19883	1330.35	320.57	1327.29	330.24	0.12	10.27	1.00	5.94	172.72	1.85	102.36
Saccade R	1	1	8354291470	8354311353	19883	1330.35	320.57	1327.29	330.24	0.12	10.27	1.00	5.94	172.72	1.85	102.36
Fixation L	1	2	8354311353	8355226295	914942	1318.09	343.34	20	50	-1	11.67	11.67
Fixation R	1	2	8354311353	8355226295	914942	1318.09	343.34	20	50	-1	13.30	13.30
Saccade L	1	2	8355226295	8355286105	59810	1324.04	334.71	852.48	457.62	5.30	184.22	0.66	88.66	4569.45	-3880.39	3124.74
Saccade R	1	2	8355226295	8355286105	59810	1324.04	334.71	852.48	457.62	5.30	184.22	0.66	88.66	4569.45	-3880.39	3124.74
Fixation L	1	3	8355286105	8355683776	397671	803.67	468.07	58	32	-1	11.72	11.72
Fixation R	1	3	8355286105	8355683776	397671	803.67	468.07	58	32	-1	14.39	14.39
