[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:41
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6623329288	# Message: REJECTED_olympia_manet_1863.jpg
Blink L	1	1	6623335826	6626976035	3640209
Blink R	1	1	6623335826	6626976035	3640209
Fixation L	1	1	6626976035	6627214655	238620	1251.52	491.85	32	48	-1	10.96	10.96
Fixation R	1	1	6626976035	6627214655	238620	1251.52	491.85	32	48	-1	12.17	12.17
Saccade L	1	1	6627214655	6627314168	99513	1248.82	482.21	1653.13	724.28	45.32	1743.13	0.40	455.41	42704.08	-43538.99	25230.00
Saccade R	1	1	6627214655	6627314168	99513	1248.82	482.21	1653.13	724.28	45.32	1743.13	0.40	455.41	42704.08	-43538.99	25230.00
