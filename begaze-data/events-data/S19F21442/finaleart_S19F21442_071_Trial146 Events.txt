[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6737418208	# Message: void.jpg
Fixation L	1	1	6737425847	6737664459	238612	753.65	299.11	8	13	-1	10.34	10.34
Fixation R	1	1	6737425847	6737664459	238612	753.65	299.11	8	13	-1	11.66	11.66
Saccade L	1	1	6737664459	6737724208	59749	751.78	305.38	1332.93	299.43	5.99	327.98	0.67	100.20	8138.22	-7958.48	4859.66
Saccade R	1	1	6737664459	6737724208	59749	751.78	305.38	1332.93	299.43	5.99	327.98	0.67	100.20	8138.22	-7958.48	4859.66
Fixation L	1	2	6737724208	6738818156	1093948	1337.73	316.93	20	57	-1	11.71	11.71
Fixation R	1	2	6737724208	6738818156	1093948	1337.73	316.93	20	57	-1	12.24	12.24
Saccade L	1	2	6738818156	6738877775	59619	1324.01	339.55	856.04	484.27	5.25	231.22	0.33	88.01	5675.15	-4534.43	4065.09
Saccade R	1	2	6738818156	6738877775	59619	1324.01	339.55	856.04	484.27	5.25	231.22	0.33	88.01	5675.15	-4534.43	4065.09
Fixation L	1	3	6738877775	6739414761	536986	844.35	485.40	24	33	-1	13.03	13.03
Fixation R	1	3	6738877775	6739414761	536986	844.35	485.40	24	33	-1	13.84	13.84
