[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6688475269	# Message: void.jpg
Fixation L	1	1	6688476242	6688953584	477342	1600.45	215.24	19	35	-1	10.24	10.24
Fixation R	1	1	6688476242	6688953584	477342	1600.45	215.24	19	35	-1	10.74	10.74
Saccade L	1	1	6688953584	6689053079	99495	1590.65	237.61	760.97	723.17	12.34	256.46	0.60	124.05	6156.70	-6094.78	3497.77
Saccade R	1	1	6688953584	6689053079	99495	1590.65	237.61	760.97	723.17	12.34	256.46	0.60	124.05	6156.70	-6094.78	3497.77
Fixation L	1	2	6689053079	6690147038	1093959	770.32	757.73	35	58	-1	11.07	11.07
Fixation R	1	2	6689053079	6690147038	1093959	770.32	757.73	35	58	-1	13.01	13.01
Saccade L	1	2	6690147038	6690166904	19866	789.40	770.18	791.67	792.66	0.20	22.86	1.00	10.20	527.08	-545.30	454.59
Saccade R	1	2	6690147038	6690166904	19866	789.40	770.18	791.67	792.66	0.20	22.86	1.00	10.20	527.08	-545.30	454.59
Fixation L	1	3	6690166904	6690425396	258492	796.84	806.93	33	28	-1	11.88	11.88
Fixation R	1	3	6690166904	6690425396	258492	796.84	806.93	33	28	-1	13.88	13.88
Saccade L	1	3	6690425396	6690465266	39870	773.96	786.81	749.56	637.28	2.01	144.55	0.50	50.36	3474.84	-638.49	1371.11
Saccade R	1	3	6690425396	6690465266	39870	773.96	786.81	749.56	637.28	2.01	144.55	0.50	50.36	3474.84	-638.49	1371.11
