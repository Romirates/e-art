[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:38:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8930172218	# Message: void.jpg
Saccade L	1	1	8930178440	8930297805	119365	823.29	321.36	578.65	263.76	5.12	225.41	0.83	42.86	5589.06	-5291.95	1948.45
Saccade R	1	1	8930178440	8930297805	119365	823.29	321.36	578.65	263.76	5.12	225.41	0.83	42.86	5589.06	-5291.95	1948.45
Fixation L	1	1	8930297805	8930994034	696229	566.25	240.24	16	46	-1	9.30	9.30
Fixation R	1	1	8930297805	8930994034	696229	566.25	240.24	16	46	-1	9.21	9.21
Blink L	1	1	8930994034	8931451493	457459
Blink R	1	1	8930994034	8931451493	457459
Fixation L	1	2	8931451493	8932187350	735857	951.11	541.46	34	55	-1	13.44	13.44
Fixation R	1	2	8931451493	8932187350	735857	951.11	541.46	34	55	-1	10.41	10.41
