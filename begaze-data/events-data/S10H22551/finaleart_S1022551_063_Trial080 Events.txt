[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:37:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7911967433	# Message: void.jpg
Saccade L	1	1	7911977405	7912056914	79509	1304.45	153.64	659.93	123.27	9.10	418.39	0.75	114.45	10266.55	-10066.90	5132.00
Saccade R	1	1	7911977405	7912056914	79509	1304.45	153.64	659.93	123.27	9.10	418.39	0.75	114.45	10266.55	-10066.90	5132.00
Fixation L	1	1	7912056914	7912427541	370627	656.76	130.98	16	14	-1	10.52	10.52
Fixation R	1	1	7912056914	7912427541	370627	656.76	130.98	16	14	-1	9.57	9.57
Saccade L	1	2	7912427541	7912468297	40756	648.44	133.24	868.79	550.62	5.14	476.35	1.00	126.16	11854.48	-11654.03	7958.34
Saccade R	1	2	7912427541	7912468297	40756	648.44	133.24	868.79	550.62	5.14	476.35	1.00	126.16	11854.48	-11654.03	7958.34
Fixation L	1	2	7912468297	7912991717	523420	863.64	563.84	76	22	-1	11.04	11.04
Fixation R	1	2	7912468297	7912991717	523420	863.64	563.84	76	22	-1	10.22	10.22
Saccade L	1	3	7912991717	7913146708	154991	928.37	559.68	933.37	559.99	3.69	74.80	0.00	23.79	90.10	-1523.96	544.90
Saccade R	1	3	7912991717	7913146708	154991	928.37	559.68	933.37	559.99	3.69	74.80	0.00	23.79	90.10	-1523.96	544.90
Fixation L	1	3	7913146708	7913966421	819713	943.80	588.13	17	41	-1	13.12	13.12
Fixation R	1	3	7913146708	7913966421	819713	943.80	588.13	17	41	-1	11.20	11.20
