[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:37:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7746242276	# Message: void.jpg
Fixation L	1	1	7746248086	7746983916	735830	559.25	741.59	39	60	-1	14.48	14.48
Fixation R	1	1	7746248086	7746983916	735830	559.25	741.59	39	60	-1	14.08	14.08
Saccade L	1	1	7746983916	7747023790	39874	533.87	712.82	740.82	505.99	2.95	210.59	1.00	74.10	5083.95	-5175.52	3297.10
Saccade R	1	1	7746983916	7747023790	39874	533.87	712.82	740.82	505.99	2.95	210.59	1.00	74.10	5083.95	-5175.52	3297.10
Fixation L	1	2	7747023790	7747381768	357978	766.41	464.11	35	56	-1	14.14	14.14
Fixation R	1	2	7747023790	7747381768	357978	766.41	464.11	35	56	-1	13.90	13.90
Saccade L	1	2	7747381768	7747401642	19874	776.05	476.52	883.26	433.49	0.68	116.84	1.00	34.07	2847.17	-2783.20	1944.79
Saccade R	1	2	7747381768	7747401642	19874	776.05	476.52	883.26	433.49	0.68	116.84	1.00	34.07	2847.17	-2783.20	1944.79
Fixation L	1	3	7747401642	7748217101	815459	865.93	431.21	64	30	-1	14.34	14.34
Fixation R	1	3	7747401642	7748217101	815459	865.93	431.21	64	30	-1	13.34	13.34
Saccade L	1	3	7748217101	7748256983	39882	828.28	434.05	813.14	430.85	0.38	17.42	0.50	9.41	197.53	-96.86	98.13
Saccade R	1	3	7748217101	7748256983	39882	828.28	434.05	813.14	430.85	0.38	17.42	0.50	9.41	197.53	-96.86	98.13
