[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:38:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8760462156	# Message: void.jpg
Fixation L	1	1	8760466431	8760655615	189184	1470.78	659.83	19	13	-1	9.11	9.11
Fixation R	1	1	8760466431	8760655615	189184	1470.78	659.83	19	13	-1	8.27	8.27
Saccade L	1	1	8760655615	8760685190	29575	1455.97	662.86	1590.92	663.22	1.28	136.49	1.00	43.22	3193.25	-3371.86	2273.21
Saccade R	1	1	8760655615	8760685190	29575	1455.97	662.86	1590.92	663.22	1.28	136.49	1.00	43.22	3193.25	-3371.86	2273.21
Fixation L	1	2	8760685190	8760955746	270556	1586.42	660.23	8	9	-1	9.51	9.51
Fixation R	1	2	8760685190	8760955746	270556	1586.42	660.23	8	9	-1	8.66	8.66
Saccade L	1	2	8760955746	8760983525	27779	1588.19	663.47	1570.90	789.87	0.98	129.04	1.00	35.30	3116.48	-3113.37	2099.91
Saccade R	1	2	8760955746	8760983525	27779	1588.19	663.47	1570.90	789.87	0.98	129.04	1.00	35.30	3116.48	-3113.37	2099.91
Fixation L	1	3	8760983525	8761222352	238827	1546.26	790.71	44	12	-1	10.01	10.01
Fixation R	1	3	8760983525	8761222352	238827	1546.26	790.71	44	12	-1	8.89	8.89
Blink L	1	1	8761222352	8761759235	536883
Blink R	1	1	8761222352	8761759235	536883
Fixation L	1	4	8761759235	8762455452	696217	886.31	469.86	18	20	-1	10.91	10.91
Fixation R	1	4	8761759235	8762455452	696217	886.31	469.86	18	20	-1	10.11	10.11
