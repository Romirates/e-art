[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:38:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8948567309	# Message: void.jpg
Fixation L	1	1	8948576816	8949690645	1113829	1107.55	690.14	31	48	-1	12.77	12.77
Fixation R	1	1	8948576816	8949690645	1113829	1107.55	690.14	31	48	-1	11.43	11.43
Blink L	1	1	8949690645	8949949113	258468
Blink R	1	1	8949690645	8949949113	258468
Fixation L	1	2	8949949113	8950386716	437603	711.38	394.38	19	73	-1	12.52	12.52
Fixation R	1	2	8949949113	8950386716	437603	711.38	394.38	19	73	-1	11.36	11.36
Saccade L	1	1	8950386716	8950426473	39757	715.65	409.01	881.92	444.09	1.86	155.00	1.00	46.70	3834.02	-3385.75	2170.60
Saccade R	1	1	8950386716	8950426473	39757	715.65	409.01	881.92	444.09	1.86	155.00	1.00	46.70	3834.02	-3385.75	2170.60
Fixation L	1	3	8950426473	8950605583	179110	923.98	465.83	58	32	-1	12.71	12.71
Fixation R	1	3	8950426473	8950605583	179110	923.98	465.83	58	32	-1	11.61	11.61
