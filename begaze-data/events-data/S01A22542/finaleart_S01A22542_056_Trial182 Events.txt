[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9503524723	# Message: void.jpg
Fixation L	1	1	9503543870	9503921732	377862	381.75	783.14	14	67	-1	11.95	11.95
Fixation R	1	1	9503543870	9503921732	377862	381.75	783.14	14	67	-1	13.32	13.32
Blink L	1	1	9503921732	9504180352	258620
Blink R	1	1	9503921732	9504180352	258620
Fixation L	1	2	9504180352	9504637817	457465	1039.08	551.84	13	57	-1	13.52	13.52
Fixation R	1	2	9504180352	9504637817	457465	1039.08	551.84	13	57	-1	13.95	13.95
Saccade L	1	1	9504637817	9504657707	19890	1028.70	561.82	899.43	555.74	0.75	130.90	1.00	37.78	3222.19	-3121.08	2148.42
Saccade R	1	1	9504637817	9504657707	19890	1028.70	561.82	899.43	555.74	0.75	130.90	1.00	37.78	3222.19	-3121.08	2148.42
Fixation L	1	3	9504657707	9505512911	855204	888.74	542.33	17	24	-1	13.79	13.79
Fixation R	1	3	9504657707	9505512911	855204	888.74	542.33	17	24	-1	14.38	14.38
