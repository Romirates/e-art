[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9521862131	# Message: void.jpg
Fixation L	1	1	9521862513	9522319985	457472	1422.71	375.36	33	27	-1	14.19	14.19
Fixation R	1	1	9521862513	9522319985	457472	1422.71	375.36	33	27	-1	14.27	14.27
Blink L	1	1	9522319985	9522518848	198863
Blink R	1	1	9522319985	9522518848	198863
Fixation L	1	2	9522518848	9523533299	1014451	1080.16	561.78	18	41	-1	14.84	14.84
Fixation R	1	2	9522518848	9523533299	1014451	1080.16	561.78	18	41	-1	14.69	14.69
Saccade L	1	1	9523533299	9523553158	19859	1088.64	542.20	1149.66	545.93	0.48	61.85	1.00	24.02	1522.05	-1312.00	1191.95
Saccade R	1	1	9523533299	9523553158	19859	1088.64	542.20	1149.66	545.93	0.48	61.85	1.00	24.02	1522.05	-1312.00	1191.95
Fixation L	1	3	9523553158	9523851531	298373	1192.09	532.96	49	22	-1	14.44	14.44
Fixation R	1	3	9523553158	9523851531	298373	1192.09	532.96	49	22	-1	14.46	14.46
