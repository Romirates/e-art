[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8712139960	# Message: void.jpg
Fixation L	1	1	8712158498	8712357609	199111	661.44	687.46	26	21	-1	13.39	13.39
Fixation R	1	1	8712158498	8712357609	199111	661.44	687.46	26	21	-1	13.69	13.69
Saccade L	1	1	8712357609	8712397357	39748	681.12	684.76	934.88	552.78	2.66	197.84	0.50	66.80	4840.16	-4734.69	3428.38
Saccade R	1	1	8712357609	8712397357	39748	681.12	684.76	934.88	552.78	2.66	197.84	0.50	66.80	4840.16	-4734.69	3428.38
Fixation L	1	2	8712397357	8712954827	557470	950.90	522.37	21	49	-1	13.66	13.66
Fixation R	1	2	8712397357	8712954827	557470	950.90	522.37	21	49	-1	13.53	13.53
Blink L	1	1	8712954827	8713153940	199113
Blink R	1	1	8712954827	8713153940	199113
Fixation L	1	3	8713153940	8714129527	975587	911.65	513.22	31	64	-1	15.28	15.28
Fixation R	1	3	8713153940	8714129527	975587	911.65	513.22	31	64	-1	15.46	15.46
