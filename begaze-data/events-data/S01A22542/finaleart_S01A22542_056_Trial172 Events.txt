[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:21
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9473073259	# Message: void.jpg
Fixation L	1	1	9473088976	9473725430	636454	752.77	535.50	14	23	-1	12.30	12.30
Fixation R	1	1	9473088976	9473725430	636454	752.77	535.50	14	23	-1	12.21	12.21
Blink L	1	1	9473725430	9473944162	218732
Blink R	1	1	9473725430	9473944162	218732
Fixation L	1	2	9473944162	9474600500	656338	889.89	504.78	23	24	-1	13.16	13.16
Fixation R	1	2	9473944162	9474600500	656338	889.89	504.78	23	24	-1	13.10	13.10
Saccade L	1	1	9474600500	9474620377	19877	891.20	499.67	998.47	511.30	0.60	109.15	1.00	30.42	2668.47	-2240.25	1681.61
Saccade R	1	1	9474600500	9474620377	19877	891.20	499.67	998.47	511.30	0.60	109.15	1.00	30.42	2668.47	-2240.25	1681.61
Fixation L	1	3	9474620377	9474938742	318365	982.14	548.51	41	47	-1	13.72	13.72
Fixation R	1	3	9474620377	9474938742	318365	982.14	548.51	41	47	-1	13.65	13.65
Saccade L	1	2	9474938742	9474958641	19899	956.77	554.24	875.81	550.73	0.60	81.98	1.00	30.19	1961.78	-1437.24	1285.74
Saccade R	1	2	9474938742	9474958641	19899	956.77	554.24	875.81	550.73	0.60	81.98	1.00	30.19	1961.78	-1437.24	1285.74
Fixation L	1	4	9474958641	9475057991	99350	858.50	536.11	24	26	-1	13.84	13.84
Fixation R	1	4	9474958641	9475057991	99350	858.50	536.11	24	26	-1	13.99	13.99
