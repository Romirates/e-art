[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8657218668	# Message: void.jpg
Fixation L	1	1	8657235018	8657652846	417828	1386.49	410.46	9	22	-1	14.43	14.43
Fixation R	1	1	8657235018	8657652846	417828	1386.49	410.46	9	22	-1	13.80	13.80
Saccade L	1	1	8657652846	8657692613	39767	1380.14	423.77	1011.03	526.14	3.22	248.32	0.50	81.00	6187.58	-6003.83	4789.37
Saccade R	1	1	8657652846	8657692613	39767	1380.14	423.77	1011.03	526.14	3.22	248.32	0.50	81.00	6187.58	-6003.83	4789.37
Fixation L	1	2	8657692613	8658448686	756073	986.49	549.51	38	38	-1	15.46	15.46
Fixation R	1	2	8657692613	8658448686	756073	986.49	549.51	38	38	-1	14.44	14.44
Saccade L	1	2	8658448686	8658468561	19875	1001.99	527.20	1019.43	505.16	0.45	28.43	1.00	22.76	13.66	-585.95	309.43
Saccade R	1	2	8658448686	8658468561	19875	1001.99	527.20	1019.43	505.16	0.45	28.43	1.00	22.76	13.66	-585.95	309.43
Fixation L	1	3	8658468561	8658985781	517220	1032.63	481.46	20	31	-1	15.76	15.76
Fixation R	1	3	8658468561	8658985781	517220	1032.63	481.46	20	31	-1	14.75	14.75
