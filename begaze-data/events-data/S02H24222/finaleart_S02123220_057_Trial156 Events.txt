[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S02123220-finaleart-1.idf
Date:	27.03.2019 15:33:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S02123220
Description:	David Xu

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	1202802256	# Message: void.jpg
Fixation L	1	1	1202803604	1203101937	298333	722.03	621.57	36	54	-1	13.08	13.08
Fixation R	1	1	1202803604	1203101937	298333	722.03	621.57	36	54	-1	12.39	12.39
Saccade L	1	1	1203101937	1203121820	19883	750.42	609.27	1009.33	518.52	1.74	277.33	1.00	87.51	6833.95	-6811.33	4559.92
Saccade R	1	1	1203101937	1203121820	19883	750.42	609.27	1009.33	518.52	1.74	277.33	1.00	87.51	6833.95	-6811.33	4559.92
Fixation L	1	2	1203121820	1203698536	576716	1053.75	522.00	50	20	-1	13.30	13.30
Fixation R	1	2	1203121820	1203698536	576716	1053.75	522.00	50	20	-1	12.28	12.28
Saccade L	1	2	1203698536	1203718422	19886	1030.43	512.38	962.86	502.08	0.57	69.14	1.00	28.85	1667.97	-1035.66	954.55
Saccade R	1	2	1203698536	1203718422	19886	1030.43	512.38	962.86	502.08	0.57	69.14	1.00	28.85	1667.97	-1035.66	954.55
Fixation L	1	3	1203718422	1204792467	1074045	938.01	500.76	47	52	-1	13.83	13.83
Fixation R	1	3	1203718422	1204792467	1074045	938.01	500.76	47	52	-1	13.08	13.08
