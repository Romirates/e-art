[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S02123220-finaleart-1.idf
Date:	27.03.2019 15:33:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S02123220
Description:	David Xu

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2261987705	# Message: void.jpg
Fixation L	1	1	2261999815	2262735756	735941	1043.35	572.48	20	58	-1	15.61	15.61
Fixation R	1	1	2261999815	2262735756	735941	1043.35	572.48	20	58	-1	13.19	13.19
Saccade L	1	1	2262735756	2262795393	59637	1055.45	535.11	800.02	390.21	3.56	135.16	0.67	59.71	3246.82	-2436.23	2007.89
Saccade R	1	1	2262735756	2262795393	59637	1055.45	535.11	800.02	390.21	3.56	135.16	0.67	59.71	3246.82	-2436.23	2007.89
Fixation L	1	2	2262795393	2263590967	795574	814.40	346.90	26	70	-1	12.50	12.50
Fixation R	1	2	2262795393	2263590967	795574	814.40	346.90	26	70	-1	11.35	11.35
Saccade L	1	2	2263590967	2263710340	119373	826.46	385.61	981.37	450.57	5.14	189.99	0.83	43.08	4499.31	-3572.00	1306.32
Saccade R	1	2	2263590967	2263710340	119373	826.46	385.61	981.37	450.57	5.14	189.99	0.83	43.08	4499.31	-3572.00	1306.32
Fixation L	1	3	2263710340	2263968950	258610	988.83	501.14	13	66	-1	14.74	14.74
Fixation R	1	3	2263710340	2263968950	258610	988.83	501.14	13	66	-1	12.83	12.83
