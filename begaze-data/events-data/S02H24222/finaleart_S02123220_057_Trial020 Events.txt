[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S02123220-finaleart-1.idf
Date:	27.03.2019 15:33:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S02123220
Description:	David Xu

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	407258266	# Message: void.jpg
Saccade L	1	1	407269803	407886385	616582	1468.96	689.37	977.23	448.69	51.85	262.17	0.23	84.10	6163.11	-4472.65	1641.89
Saccade R	1	1	407269803	407886385	616582	1468.96	689.37	977.23	448.69	51.85	262.17	0.23	84.10	6163.11	-4472.65	1641.89
Fixation L	1	1	407886385	408642214	755829	988.60	460.82	32	58	-1	12.70	12.70
Fixation R	1	1	407886385	408642214	755829	988.60	460.82	32	58	-1	12.88	12.88
Saccade L	1	2	408642214	408662093	19879	964.07	503.17	960.20	517.10	0.31	20.38	1.00	15.50	182.08	-144.03	110.34
Saccade R	1	2	408642214	408662093	19879	964.07	503.17	960.20	517.10	0.31	20.38	1.00	15.50	182.08	-144.03	110.34
Fixation L	1	2	408662093	409238921	576828	939.77	566.52	27	70	-1	14.97	14.97
Fixation R	1	2	408662093	409238921	576828	939.77	566.52	27	70	-1	14.78	14.78
