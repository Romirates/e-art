[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6346158783	# Message: void.jpg
Fixation L	1	1	6346161164	6346578777	417613	1546.99	518.56	78	14	-1	11.26	11.26
Fixation R	1	1	6346161164	6346578777	417613	1546.99	518.56	78	14	-1	10.81	10.81
Saccade L	1	1	6346578777	6346618636	39859	1578.73	512.12	1466.75	381.16	1.71	135.70	1.00	42.88	3345.15	-3115.58	2043.55
Saccade R	1	1	6346578777	6346618636	39859	1578.73	512.12	1466.75	381.16	1.71	135.70	1.00	42.88	3345.15	-3115.58	2043.55
Fixation L	1	2	6346618636	6347672717	1054081	1421.73	368.48	61	36	-1	12.36	12.36
Fixation R	1	2	6346618636	6347672717	1054081	1421.73	368.48	61	36	-1	11.96	11.96
Saccade L	1	2	6347672717	6347692579	19862	1405.67	349.82	1401.51	350.73	0.08	5.97	0.00	3.87	28.76	-98.36	72.56
Saccade R	1	2	6347672717	6347692579	19862	1405.67	349.82	1401.51	350.73	0.08	5.97	0.00	3.87	28.76	-98.36	72.56
Fixation L	1	3	6347692579	6348189936	497357	1402.62	333.86	58	30	-1	13.06	13.06
Fixation R	1	3	6347692579	6348189936	497357	1402.62	333.86	58	30	-1	12.81	12.81
Saccade L	1	3	6348189936	6348209810	19874	1353.46	323.91	1342.66	324.37	0.45	38.76	1.00	22.39	0.00	-695.61	347.80
Saccade R	1	3	6348189936	6348209810	19874	1353.46	323.91	1342.66	324.37	0.45	38.76	1.00	22.39	0.00	-695.61	347.80
