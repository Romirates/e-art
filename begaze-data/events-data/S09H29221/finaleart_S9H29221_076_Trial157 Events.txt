[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6124010159	# Message: Paysage_a_La_Ciotat_(La_Pointe_du-Capucin)_friesz_1907.jpg
Saccade R	1	1	6124019920	6124099413	79493	859.95	851.04	863.42	602.88	11.21	453.68	0.25	141.00	28549.34	-11272.58	9035.14
Saccade L	1	1	6124079536	6124099413	19877	850.03	594.79	863.42	602.88	1.95	220.43	1.00	98.08	28549.34	-5115.27	12475.81
Blink L	1	1	6124099413	6127202635	3103222
Blink R	1	1	6124099413	6127202635	3103222
Fixation L	1	1	6127202635	6127322006	119371	1058.77	468.16	13	8	-1	11.09	11.09
Fixation R	1	1	6127202635	6127461242	258607	1034.95	472.86	73	20	-1	9.09	9.09
Blink L	1	2	6127322006	6127401627	79621
Fixation L	1	2	6127401627	6127859105	457478	993.19	469.93	35	16	-1	8.25	8.25
Fixation R	1	2	6127481125	6128057964	576839	992.23	474.43	22	38	-1	9.64	9.64
Fixation L	1	3	6127878975	6128057964	178989	994.74	485.46	13	21	-1	10.21	10.21
