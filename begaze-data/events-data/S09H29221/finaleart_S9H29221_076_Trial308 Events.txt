[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:04:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6947723684	# Message: void.jpg
Fixation L	1	1	6947733896	6949404672	1670776	958.99	512.72	23	43	-1	16.06	16.06
Fixation R	1	1	6947733896	6949404672	1670776	958.99	512.72	23	43	-1	15.75	15.75
Saccade L	1	1	6949404672	6949424561	19889	943.33	518.81	893.40	523.33	0.38	50.71	1.00	19.33	1259.13	-1128.40	889.67
Saccade R	1	1	6949404672	6949424561	19889	943.33	518.81	893.40	523.33	0.38	50.71	1.00	19.33	1259.13	-1128.40	889.67
Fixation L	1	2	6949424561	6949762659	338098	871.37	527.85	30	8	-1	17.69	17.69
Fixation R	1	2	6949424561	6949762659	338098	871.37	527.85	30	8	-1	17.01	17.01
