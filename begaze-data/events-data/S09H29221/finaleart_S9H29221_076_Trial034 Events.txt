[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:02:02
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5570301586	# Message: void.jpg
Fixation L	1	1	5570309063	5570627277	318214	1257.78	576.78	72	25	-1	10.71	10.71
Fixation R	1	1	5570309063	5570627277	318214	1257.78	576.78	72	25	-1	10.45	10.45
Saccade L	1	1	5570627277	5570647164	19887	1289.24	591.80	1291.56	595.84	0.14	9.33	1.00	6.91	118.33	-107.17	81.92
Saccade R	1	1	5570627277	5570647164	19887	1289.24	591.80	1291.56	595.84	0.14	9.33	1.00	6.91	118.33	-107.17	81.92
Fixation L	1	2	5570647164	5571184262	537098	1316.63	593.15	38	16	-1	10.63	10.63
Fixation R	1	2	5570647164	5571184262	537098	1316.63	593.15	38	16	-1	10.28	10.28
Blink L	1	1	5571184262	5571581990	397728
Blink R	1	1	5571184262	5571581990	397728
Fixation L	1	3	5571581990	5572238470	656480	986.00	444.18	37	42	-1	11.31	11.31
Fixation R	1	3	5571581990	5572238470	656480	986.00	444.18	37	42	-1	11.26	11.26
Saccade L	1	2	5572238470	5572298092	59622	1015.32	447.75	1084.74	452.29	0.94	34.83	0.33	15.69	728.44	-539.80	453.50
Saccade R	1	2	5572238470	5572298092	59622	1015.32	447.75	1084.74	452.29	0.94	34.83	0.33	15.69	728.44	-539.80	453.50
