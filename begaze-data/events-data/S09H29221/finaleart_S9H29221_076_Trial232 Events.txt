[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6612776162	# Message: void.jpg
Fixation L	1	1	6612777539	6613533239	755700	1254.82	548.08	28	67	-1	11.58	11.58
Fixation R	1	1	6612777539	6613533239	755700	1254.82	548.08	28	67	-1	10.51	10.51
Saccade L	1	1	6613533239	6613612859	79620	1248.93	510.59	1213.17	431.10	2.13	94.17	1.00	26.76	2508.82	-753.17	1099.78
Saccade R	1	1	6613533239	6613612859	79620	1248.93	510.59	1213.17	431.10	2.13	94.17	1.00	26.76	2508.82	-753.17	1099.78
Blink L	1	1	6613612859	6613831608	218749
Blink R	1	1	6613612859	6613831608	218749
Fixation L	1	2	6613831608	6613931108	99500	1305.18	196.94	64	30	-1	11.39	11.39
Fixation R	1	2	6613831608	6613931108	99500	1305.18	196.94	64	30	-1	12.01	12.01
Saccade L	1	2	6613931108	6613950982	19874	1334.40	200.08	1344.58	206.82	0.24	17.27	0.00	12.09	223.81	-48.39	110.01
Saccade R	1	2	6613931108	6613950982	19874	1334.40	200.08	1344.58	206.82	0.24	17.27	0.00	12.09	223.81	-48.39	110.01
Fixation L	1	3	6613950982	6614110110	159128	1389.57	199.57	77	13	-1	11.49	11.49
Fixation R	1	3	6613950982	6614110110	159128	1389.57	199.57	77	13	-1	11.97	11.97
