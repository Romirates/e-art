[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:02:59
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6084851246	# Message: void.jpg
Blink L	1	1	6084860420	6086109485	1249065
Blink R	1	1	6084860420	6086109485	1249065
Fixation L	1	1	6086109485	6086288599	179114	1120.14	481.97	17	15	-1	10.11	10.11
Fixation R	1	1	6086109485	6086328335	218850	1122.62	481.28	35	15	-1	8.84	8.84
Fixation L	1	2	6086328335	6086885221	556886	1122.65	502.40	18	41	-1	9.92	9.92
Fixation R	1	2	6086348218	6086885221	537003	1122.51	503.27	18	40	-1	9.68	9.68
