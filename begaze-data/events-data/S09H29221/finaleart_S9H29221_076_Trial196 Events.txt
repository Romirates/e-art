[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:26
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6395434645	# Message: void.jpg
Fixation L	1	1	6395436072	6395953157	517085	800.25	305.49	39	58	-1	11.61	11.61
Fixation R	1	1	6395436072	6395953157	517085	800.25	305.49	39	58	-1	11.75	11.75
Saccade L	1	1	6395953157	6395973042	19885	790.34	272.70	783.72	268.97	0.10	7.69	1.00	4.98	136.98	-84.92	79.17
Saccade R	1	1	6395953157	6395973042	19885	790.34	272.70	783.72	268.97	0.10	7.69	1.00	4.98	136.98	-84.92	79.17
Fixation L	1	2	6395973042	6396112267	139225	786.12	273.61	16	17	-1	11.54	11.54
Fixation R	1	2	6395973042	6396112267	139225	786.12	273.61	16	17	-1	11.32	11.32
Saccade L	1	2	6396112267	6396191875	79608	792.00	272.48	797.88	300.47	12.76	856.06	1.00	160.30	20901.87	-2309.11	4881.64
Saccade R	1	2	6396112267	6396191875	79608	792.00	272.48	797.88	300.47	12.76	856.06	1.00	160.30	20901.87	-2309.11	4881.64
Blink L	1	1	6396191875	6396848239	656364
Blink R	1	1	6396191875	6396828361	636486
Saccade R	1	3	6396828361	6396907861	79500	1047.03	324.80	1062.51	340.27	2.19	62.53	1.00	27.61	3816.15	-1119.37	1188.03
Saccade L	1	3	6396848239	6396907861	59622	1054.37	324.69	1062.51	340.27	1.59	62.53	1.00	26.61	3816.15	-1119.37	1294.80
Blink L	1	2	6396907861	6397047090	139229
Blink R	1	2	6396907861	6397047090	139229
