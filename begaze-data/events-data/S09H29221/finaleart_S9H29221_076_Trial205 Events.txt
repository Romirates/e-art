[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6422209875	# Message: bathers_cross_1902.jpg
Blink L	1	1	6422232980	6425714483	3481503
Blink R	1	1	6422232980	6425714483	3481503
Fixation L	1	1	6425714483	6425833841	119358	1187.62	613.05	11	5	-1	11.87	11.87
Fixation R	1	1	6425714483	6425853710	139227	1186.41	610.34	16	25	-1	11.65	11.65
Saccade R	1	1	6425853710	6425893471	39761	1177.89	591.40	1016.91	483.30	2.11	211.60	0.50	53.19	5225.50	-5058.55	2685.39
Fixation L	1	2	6425893471	6426231566	338095	1035.22	462.15	24	33	-1	9.57	9.57
Fixation R	1	2	6425913347	6426231566	318219	1036.30	460.91	19	26	-1	9.82	9.82
