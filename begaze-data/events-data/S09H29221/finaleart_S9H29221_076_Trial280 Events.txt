[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:04:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6861555286	# Message: void.jpg
Fixation L	1	1	6861563386	6861748408	185022	1094.40	463.68	5	2	-1	11.55	11.55
Fixation R	1	1	6861563386	6861748408	185022	1094.40	463.68	5	2	-1	11.76	11.76
Saccade L	1	1	6861748408	6861782151	33743	1091.80	464.03	909.96	477.22	1.62	184.38	1.00	48.02	4587.66	-4553.16	3082.06
Saccade R	1	1	6861748408	6861782151	33743	1091.80	464.03	909.96	477.22	1.62	184.38	1.00	48.02	4587.66	-4553.16	3082.06
Fixation L	1	2	6861782151	6863015308	1233157	896.21	502.67	34	46	-1	11.82	11.82
Fixation R	1	2	6861782151	6863015308	1233157	896.21	502.67	34	46	-1	12.11	12.11
Saccade L	1	2	6863015308	6863035193	19885	886.84	507.04	997.00	500.03	0.94	111.64	1.00	47.51	2736.56	-2605.76	2346.40
Saccade R	1	2	6863015308	6863035193	19885	886.84	507.04	997.00	500.03	0.94	111.64	1.00	47.51	2736.56	-2605.76	2346.40
Fixation L	1	3	6863035193	6863492653	457460	1062.63	485.59	71	24	-1	13.46	13.46
Fixation R	1	3	6863035193	6863492653	457460	1062.63	485.59	71	24	-1	13.40	13.40
Saccade L	1	3	6863492653	6863512538	19885	1067.92	502.90	1066.72	510.31	0.08	7.60	1.00	3.93	67.77	-21.28	39.40
Saccade R	1	3	6863492653	6863512538	19885	1067.92	502.90	1066.72	510.31	0.08	7.60	1.00	3.93	67.77	-21.28	39.40
Fixation L	1	4	6863512538	6863612030	99492	1065.41	512.00	3	6	-1	14.19	14.19
Fixation R	1	4	6863512538	6863612030	99492	1065.41	512.00	3	6	-1	13.75	13.75
