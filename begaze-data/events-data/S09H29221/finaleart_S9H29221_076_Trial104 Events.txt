[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:02:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5867177743	# Message: void.jpg
Fixation L	1	1	5867180583	5867680888	500305	589.80	780.76	31	55	-1	11.22	11.22
Fixation R	1	1	5867180583	5867680888	500305	589.80	780.76	31	55	-1	10.48	10.48
Saccade L	1	1	5867680888	5867797185	116297	604.58	750.46	729.73	433.94	5.07	122.48	0.66	43.62	2913.84	-2109.03	1280.12
Saccade R	1	1	5867680888	5867797185	116297	604.58	750.46	729.73	433.94	5.07	122.48	0.66	43.62	2913.84	-2109.03	1280.12
Fixation L	1	2	5867797185	5868015923	218738	762.44	426.79	51	39	-1	10.24	10.24
Fixation R	1	2	5867797185	5868015923	218738	762.44	426.79	51	39	-1	9.61	9.61
