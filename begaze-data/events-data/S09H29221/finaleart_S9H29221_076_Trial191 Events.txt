[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6379096457	# Message: the-pointe-de-per-kiridec-at-roscoff_Rysselberghe_0.jpg
Blink L	1	1	6379201172	6382527416	3326244
Blink R	1	1	6379201172	6382527416	3326244
Fixation L	1	1	6382527416	6382626788	99372	938.75	533.66	5	17	-1	11.50	11.50
Fixation R	1	1	6382527416	6382626788	99372	938.75	533.66	5	17	-1	11.00	11.00
Blink L	1	2	6382626788	6382706415	79627
Blink R	1	2	6382626788	6382706415	79627
Fixation L	1	2	6382706415	6382825783	119368	952.76	492.79	23	25	-1	9.39	9.39
Fixation R	1	2	6382706415	6382825783	119368	952.76	492.79	23	25	-1	9.65	9.65
Saccade L	1	1	6382825783	6382845656	19873	967.26	489.72	1048.59	503.46	0.59	83.44	1.00	29.75	1971.81	-1930.39	1349.00
Saccade R	1	1	6382825783	6382845656	19873	967.26	489.72	1048.59	503.46	0.59	83.44	1.00	29.75	1971.81	-1930.39	1349.00
Fixation L	1	3	6382845656	6383084264	238608	1040.85	539.98	17	62	-1	8.11	8.11
Fixation R	1	3	6382845656	6383084264	238608	1040.85	539.98	17	62	-1	8.70	8.70
