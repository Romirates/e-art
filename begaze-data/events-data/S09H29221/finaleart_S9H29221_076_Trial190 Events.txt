[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S9H29221-finaleart-1.idf
Date:	27.03.2019 16:03:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S9H29221
Description:	steven

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6377045637	# Message: void.jpg
Saccade L	1	1	6377050437	6377090319	39882	950.94	263.73	774.29	267.43	2.00	158.46	1.00	50.17	3961.57	-3659.96	2014.59
Saccade R	1	1	6377050437	6377090319	39882	950.94	263.73	774.29	267.43	2.00	158.46	1.00	50.17	3961.57	-3659.96	2014.59
Fixation L	1	1	6377090319	6377289178	198859	778.57	262.37	13	12	-1	7.85	7.85
Fixation R	1	1	6377090319	6377647144	556825	779.60	256.34	19	58	-1	9.17	9.17
Blink L	1	1	6377289178	6377408558	119380
Fixation L	1	2	6377408558	6377647144	238586	779.20	246.51	2	11	-1	10.15	10.15
Saccade L	1	2	6377647144	6377686885	39741	777.41	245.23	1099.72	273.54	2.87	179.59	0.50	72.10	4468.57	-3738.63	3817.71
Saccade R	1	2	6377647144	6377686885	39741	777.41	245.23	1099.72	273.54	2.87	179.59	0.50	72.10	4468.57	-3738.63	3817.71
Fixation L	1	3	6377686885	6377885906	199021	1120.82	287.04	25	29	-1	10.80	10.80
Fixation R	1	2	6377686885	6377885906	199021	1120.82	287.04	25	29	-1	9.91	9.91
Blink L	1	2	6377885906	6378442866	556960
Blink R	1	1	6377885906	6378442866	556960
