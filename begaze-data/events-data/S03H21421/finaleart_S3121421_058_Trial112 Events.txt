[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4535889270	# Message: void.jpg
Saccade L	1	1	4535891009	4535930871	39862	1092.49	380.35	832.27	303.62	3.12	214.53	0.50	78.28	1603.44	-4501.77	1842.53
Saccade R	1	1	4535891009	4535930871	39862	1092.49	380.35	832.27	303.62	3.12	214.53	0.50	78.28	1603.44	-4501.77	1842.53
Fixation L	1	1	4535930871	4536468086	537215	847.09	279.90	34	64	-1	11.34	11.34
Fixation R	1	1	4535930871	4536468086	537215	847.09	279.90	34	64	-1	11.77	11.77
Saccade L	1	2	4536468086	4536487960	19874	866.65	320.71	871.54	325.97	0.16	11.37	1.00	7.80	125.77	-94.03	82.05
Saccade R	1	2	4536468086	4536487960	19874	866.65	320.71	871.54	325.97	0.16	11.37	1.00	7.80	125.77	-94.03	82.05
Fixation L	1	2	4536487960	4537880756	1392796	873.43	347.84	17	40	-1	11.27	11.27
Fixation R	1	2	4536487960	4537880756	1392796	873.43	347.84	17	40	-1	11.85	11.85
