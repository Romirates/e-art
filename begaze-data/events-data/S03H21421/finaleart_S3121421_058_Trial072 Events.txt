[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:54
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4265597010	# Message: void.jpg
Fixation L	1	1	4265608578	4267279350	1670772	1156.65	704.68	39	57	-1	12.46	12.46
Fixation R	1	1	4265608578	4267279350	1670772	1156.65	704.68	39	57	-1	11.94	11.94
Saccade L	1	1	4267279350	4267338964	59614	1157.51	709.08	1136.84	316.03	4.60	170.73	0.33	77.11	3777.04	-3973.71	2950.41
Saccade R	1	1	4267279350	4267338964	59614	1157.51	709.08	1136.84	316.03	4.60	170.73	0.33	77.11	3777.04	-3973.71	2950.41
Fixation L	1	2	4267338964	4267597586	258622	1128.66	278.74	12	53	-1	12.70	12.70
Fixation R	1	2	4267338964	4267597586	258622	1128.66	278.74	12	53	-1	12.62	12.62
