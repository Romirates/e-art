[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:01
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4662962325	# Message: void.jpg
Fixation L	1	1	4662962452	4663360168	397716	1490.11	725.94	26	34	-1	11.47	11.47
Fixation R	1	1	4662962452	4663360168	397716	1490.11	725.94	26	34	-1	11.09	11.09
Blink L	1	1	4663360168	4663598895	238727
Blink R	1	1	4663360168	4663598895	238727
Saccade L	1	1	4663598895	4663618775	19880	988.34	381.21	917.17	425.98	5.32	975.06	1.00	267.82	-112.98	-22250.30	8047.41
Saccade R	1	1	4663598895	4663618775	19880	988.34	381.21	917.17	425.98	5.32	975.06	1.00	267.82	-112.98	-22250.30	8047.41
Fixation L	1	2	4663618775	4664971198	1352423	883.53	441.55	61	30	-1	10.97	10.97
Fixation R	1	2	4663618775	4664971198	1352423	883.53	441.55	61	30	-1	11.60	11.60
