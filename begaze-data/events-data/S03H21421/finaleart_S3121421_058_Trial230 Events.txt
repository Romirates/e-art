[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5273849130	# Message: void.jpg
Fixation L	1	1	5273865424	5273984798	119374	952.77	733.54	5	11	-1	11.58	11.58
Fixation R	1	1	5273865424	5273984798	119374	952.77	733.54	5	11	-1	11.85	11.85
Saccade L	1	1	5273984798	5274064411	79613	954.72	726.98	364.38	723.37	7.09	225.02	0.50	89.08	5487.57	-3823.10	3098.14
Saccade R	1	1	5273984798	5274064411	79613	954.72	726.98	364.38	723.37	7.09	225.02	0.50	89.08	5487.57	-3823.10	3098.14
Fixation L	1	2	5274064411	5274303141	238730	359.99	722.81	23	14	-1	10.81	10.81
Fixation R	1	2	5274064411	5274303141	238730	359.99	722.81	23	14	-1	11.68	11.68
Blink L	1	1	5274303141	5274522055	218914
Blink R	1	1	5274303141	5274621505	318364
Saccade L	1	2	5274522055	5274581760	59705	833.91	459.50	952.44	615.34	4.16	130.12	1.00	69.71	1980.45	-1713.57	1162.39
Fixation L	1	3	5274581760	5275497071	915311	1014.81	625.76	67	31	-1	11.39	11.39
Fixation R	1	3	5274621505	5275835294	1213789	1014.37	624.58	14	41	-1	11.81	11.81
Saccade L	1	3	5275497071	5275516945	19874	1014.23	623.94	1013.76	611.34	0.17	12.75	1.00	8.61	176.54	-225.48	157.73
Fixation L	1	4	5275516945	5275835294	318349	1009.58	620.24	8	34	-1	11.78	11.78
