[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4554333897	# Message: void.jpg
Fixation L	1	1	4554336257	4554535253	198996	781.66	165.20	30	33	-1	10.67	10.67
Fixation R	1	1	4554336257	4554535253	198996	781.66	165.20	30	33	-1	11.62	11.62
Saccade L	1	1	4554535253	4554555113	19860	757.69	192.38	675.32	327.10	1.08	159.70	1.00	54.31	3905.79	-3900.81	2863.09
Saccade R	1	1	4554535253	4554555113	19860	757.69	192.38	675.32	327.10	1.08	159.70	1.00	54.31	3905.79	-3900.81	2863.09
Fixation L	1	2	4554555113	4554833603	278490	673.81	330.47	45	28	-1	10.64	10.64
Fixation R	1	2	4554555113	4554833603	278490	673.81	330.47	45	28	-1	11.59	11.59
Saccade L	1	2	4554833603	4554933093	99490	702.03	342.89	856.79	373.07	2.73	55.00	0.20	27.39	852.75	-825.57	560.45
Saccade R	1	2	4554833603	4554933093	99490	702.03	342.89	856.79	373.07	2.73	55.00	0.20	27.39	852.75	-825.57	560.45
Fixation L	1	3	4554933093	4556326006	1392913	875.09	410.91	28	69	-1	11.70	11.70
Fixation R	1	3	4554933093	4556326006	1392913	875.09	410.91	28	69	-1	12.73	12.73
