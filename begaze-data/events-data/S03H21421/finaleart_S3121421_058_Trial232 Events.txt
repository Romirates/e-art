[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5279973320	# Message: void.jpg
Fixation L	1	1	5279974068	5280451523	477455	1024.94	668.04	14	72	-1	11.42	11.42
Fixation R	1	1	5279974068	5280451523	477455	1024.94	668.04	14	72	-1	11.24	11.24
Saccade L	1	1	5280451523	5280471519	19996	1024.71	617.03	1023.92	601.70	0.26	21.11	0.00	13.00	52.61	-478.92	259.58
Saccade R	1	1	5280451523	5280471519	19996	1024.71	617.03	1023.92	601.70	0.26	21.11	0.00	13.00	52.61	-478.92	259.58
Fixation L	1	2	5280471519	5281924061	1452542	996.18	635.16	35	57	-1	10.90	10.90
Fixation R	1	2	5280471519	5281924061	1452542	996.18	635.16	35	57	-1	10.70	10.70
Saccade L	1	2	5281924061	5281963802	39741	989.40	617.43	953.54	558.93	0.75	37.60	1.00	18.90	868.10	0.00	563.66
Saccade R	1	2	5281924061	5281963802	39741	989.40	617.43	953.54	558.93	0.75	37.60	1.00	18.90	868.10	0.00	563.66
