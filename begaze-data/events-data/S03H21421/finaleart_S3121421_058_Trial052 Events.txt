[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:52
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4043229230	# Message: void.jpg
Fixation L	1	1	4043241905	4043859224	617319	1137.61	282.37	27	62	-1	12.12	12.12
Fixation R	1	1	4043241905	4043859224	617319	1137.61	282.37	27	62	-1	11.46	11.46
Blink L	1	1	4043859224	4044137990	278766
Blink R	1	1	4043859224	4044137990	278766
Fixation L	1	2	4044137990	4045253153	1115163	1072.14	411.87	26	39	-1	10.97	10.97
Fixation R	1	2	4044137990	4045253153	1115163	1072.14	411.87	26	39	-1	11.45	11.45
