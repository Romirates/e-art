[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:12
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5482347594	# Message: void.jpg
Saccade L	1	1	5482362076	5482441700	79624	402.79	512.91	671.01	471.99	3.97	211.35	0.75	49.88	5188.50	-4783.62	2103.10
Saccade R	1	1	5482362076	5482441700	79624	402.79	512.91	671.01	471.99	3.97	211.35	0.75	49.88	5188.50	-4783.62	2103.10
Fixation L	1	1	5482441700	5482779794	338094	676.71	450.03	50	40	-1	10.81	10.81
Fixation R	1	1	5482441700	5482779794	338094	676.71	450.03	50	40	-1	8.79	8.79
Saccade L	1	2	5482779794	5482799670	19876	716.97	452.35	756.93	429.40	0.58	50.31	0.00	29.05	990.02	-1052.01	997.66
Saccade R	1	2	5482779794	5482799670	19876	716.97	452.35	756.93	429.40	0.58	50.31	0.00	29.05	990.02	-1052.01	997.66
Fixation L	1	2	5482799670	5482919039	119369	768.80	442.25	38	28	-1	10.65	10.65
Fixation R	1	2	5482799670	5482919039	119369	768.80	442.25	38	28	-1	8.40	8.40
Blink L	1	1	5482919039	5483197517	278478
Blink R	1	1	5482919039	5483197517	278478
Fixation L	1	3	5483197517	5484351578	1154061	935.23	499.37	35	51	-1	10.01	10.01
Fixation R	1	3	5483197517	5484351578	1154061	935.23	499.37	35	51	-1	10.78	10.78
