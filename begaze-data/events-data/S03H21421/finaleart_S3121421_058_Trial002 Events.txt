[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3834344024	# Message: void.jpg
Fixation L	1	1	3834354789	3834613272	258483	731.68	497.43	53	14	-1	11.57	11.57
Fixation R	1	1	3834354789	3834613272	258483	731.68	497.43	53	14	-1	12.61	12.61
Saccade L	1	1	3834613272	3834633138	19866	779.08	501.94	979.99	498.98	1.54	203.18	1.00	77.52	5054.10	-4758.47	3271.97
Saccade R	1	1	3834613272	3834633138	19866	779.08	501.94	979.99	498.98	1.54	203.18	1.00	77.52	5054.10	-4758.47	3271.97
Fixation L	1	2	3834633138	3835190118	556980	1030.70	487.30	64	34	-1	11.62	11.62
Fixation R	1	2	3834633138	3835209981	576843	1030.01	487.22	64	34	-1	12.41	12.41
Blink L	1	1	3835190118	3835309477	119359
Saccade R	1	2	3835209981	3835249728	39747	1010.02	484.72	1070.18	488.34	2.29	109.08	1.00	57.52	2246.59	-2196.93	1910.16
Fixation R	1	3	3835249728	3835369095	119367	1016.61	475.70	67	22	-1	12.59	12.59
Fixation L	1	3	3835309477	3836383414	1073937	1005.76	455.82	20	34	-1	12.68	12.68
Saccade R	1	3	3835369095	3835388966	19871	1002.44	466.82	1005.42	451.51	0.18	15.78	1.00	9.03	28.89	-367.95	142.88
Fixation R	1	4	3835388966	3836383414	994448	1005.77	454.54	20	24	-1	13.36	13.36
