[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:12
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5494555018	# Message: void.jpg
Fixation L	1	1	5494559123	5495156052	596929	1032.29	455.70	24	55	-1	10.89	10.89
Fixation R	1	1	5494559123	5495156052	596929	1032.29	455.70	24	55	-1	11.73	11.73
Blink L	1	1	5495156052	5495335172	179120
Blink R	1	1	5495156052	5495335172	179120
Fixation L	1	2	5495335172	5496548841	1213669	1057.02	411.64	25	61	-1	11.11	11.11
Fixation R	1	2	5495335172	5496548841	1213669	1057.02	411.64	25	61	-1	11.76	11.76
