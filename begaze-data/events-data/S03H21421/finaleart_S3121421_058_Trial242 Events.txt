[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5310753493	# Message: void.jpg
Fixation L	1	1	5310754831	5311232409	477578	544.94	482.39	46	28	-1	12.08	12.08
Fixation R	1	1	5310754831	5311232409	477578	544.94	482.39	46	28	-1	13.16	13.16
Saccade L	1	1	5311232409	5311371639	139230	578.38	481.09	807.69	520.59	4.29	58.59	0.29	30.80	771.10	-1231.75	526.18
Saccade R	1	1	5311232409	5311371639	139230	578.38	481.09	807.69	520.59	4.29	58.59	0.29	30.80	771.10	-1231.75	526.18
Blink L	1	1	5311371639	5311670136	298497
Blink R	1	1	5311371639	5311670136	298497
Fixation L	1	2	5311670136	5312406341	736205	917.35	623.93	26	50	-1	11.44	11.44
Fixation R	1	2	5311670136	5312406341	736205	917.35	623.93	26	50	-1	11.44	11.44
Saccade L	1	2	5312406341	5312446216	39875	912.73	613.93	993.60	446.87	1.69	116.23	0.50	42.39	2831.68	-2691.65	2088.10
Saccade R	1	2	5312406341	5312446216	39875	912.73	613.93	993.60	446.87	1.69	116.23	0.50	42.39	2831.68	-2691.65	2088.10
Fixation L	1	3	5312446216	5312744562	298346	1008.68	440.09	19	21	-1	10.82	10.82
Fixation R	1	3	5312446216	5312744562	298346	1008.68	440.09	19	21	-1	10.90	10.90
