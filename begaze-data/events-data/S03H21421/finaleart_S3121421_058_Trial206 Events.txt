[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4987988642	# Message: void.jpg
Fixation L	1	1	4987990442	4988468023	477581	836.13	388.94	21	46	-1	9.92	9.92
Fixation R	1	1	4987990442	4988468023	477581	836.13	388.94	21	46	-1	10.28	10.28
Blink L	1	1	4988468023	4988766504	298481
Blink R	1	1	4988468023	4988766504	298481
Fixation L	1	2	4988766504	4989104615	338111	1021.86	369.47	11	43	-1	9.44	9.44
Fixation R	1	2	4988766504	4989104615	338111	1021.86	369.47	11	43	-1	9.78	9.78
Saccade L	1	1	4989104615	4989144489	39874	1021.21	370.12	1019.14	363.06	2.95	159.75	0.50	73.91	3469.18	-3688.21	3328.24
Saccade R	1	1	4989104615	4989144489	39874	1021.21	370.12	1019.14	363.06	2.95	159.75	0.50	73.91	3469.18	-3688.21	3328.24
Fixation L	1	3	4989144489	4989979812	835323	1022.74	361.87	49	38	-1	11.29	11.29
Fixation R	1	3	4989144489	4989979812	835323	1022.74	361.87	49	38	-1	11.64	11.64
