[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3932603797	# Message: void.jpg
Fixation L	1	1	3932611845	3932890573	278728	1463.26	354.12	24	29	-1	13.13	13.13
Fixation R	1	1	3932611845	3932890573	278728	1463.26	354.12	24	29	-1	13.33	13.33
Saccade L	1	1	3932890573	3932930449	39876	1473.06	341.08	1155.91	310.41	2.88	270.53	0.50	72.28	6730.16	-5969.76	3746.52
Saccade R	1	1	3932890573	3932930449	39876	1473.06	341.08	1155.91	310.41	2.88	270.53	0.50	72.28	6730.16	-5969.76	3746.52
Fixation L	1	2	3932930449	3933149433	218984	1138.73	267.87	24	54	-1	12.34	12.34
Fixation R	1	2	3932930449	3933149433	218984	1138.73	267.87	24	54	-1	12.55	12.55
Blink L	1	1	3933149433	3933388433	239000
Blink R	1	1	3933149433	3933388433	239000
Saccade L	1	2	3933388433	3933448166	59733	647.83	325.10	793.84	481.76	4.57	156.40	1.00	76.53	2363.94	-1902.63	1385.20
Saccade R	1	2	3933388433	3933448166	59733	647.83	325.10	793.84	481.76	4.57	156.40	1.00	76.53	2363.94	-1902.63	1385.20
Fixation L	1	3	3933448166	3934603099	1154933	822.05	495.59	37	61	-1	13.32	13.32
Fixation R	1	3	3933448166	3934603099	1154933	822.05	495.59	37	61	-1	14.63	14.63
