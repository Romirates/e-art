[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5298460028	# Message: void.jpg
Fixation L	1	1	5298478939	5298896781	417842	346.48	479.17	30	68	-1	8.88	8.88
Fixation R	1	1	5298478939	5298896781	417842	346.48	479.17	30	68	-1	9.73	9.73
Blink L	1	1	5298896781	5299055891	159110
Blink R	1	1	5298896781	5299055891	159110
Blink L	1	2	5299195265	5299374247	178982
Blink R	1	2	5299215142	5299374247	159105
Fixation L	1	2	5299374247	5300448312	1074065	1021.71	577.25	35	33	-1	9.86	9.86
Fixation R	1	2	5299374247	5300448312	1074065	1021.71	577.25	35	33	-1	9.98	9.98
