[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5617784593	# Message: void.jpg
Fixation L	1	1	5617786780	5617926273	139493	1396.40	832.67	72	20	-1	10.59	10.59
Fixation R	1	1	5617786780	5617926273	139493	1396.40	832.67	72	20	-1	10.13	10.13
Saccade L	1	1	5617926273	5617946140	19867	1459.01	820.48	1556.04	739.02	1.13	128.14	1.00	57.04	3010.11	-3038.18	2519.04
Saccade R	1	1	5617926273	5617946140	19867	1459.01	820.48	1556.04	739.02	1.13	128.14	1.00	57.04	3010.11	-3038.18	2519.04
Fixation L	1	2	5617946140	5618224868	278728	1560.16	720.65	48	22	-1	10.30	10.30
Fixation R	1	2	5617946140	5618224868	278728	1560.16	720.65	48	22	-1	10.04	10.04
Saccade L	1	2	5618224868	5618344355	119487	1519.81	717.23	1170.45	679.64	6.00	96.62	0.33	50.25	1667.86	-1704.39	1018.43
Saccade R	1	2	5618224868	5618344355	119487	1519.81	717.23	1170.45	679.64	6.00	96.62	0.33	50.25	1667.86	-1704.39	1018.43
Blink L	1	1	5618344355	5618682742	338387
Blink R	1	1	5618344355	5618682742	338387
Fixation L	1	3	5618682742	5619776653	1093911	831.36	281.07	19	53	-1	10.46	10.46
Fixation R	1	3	5618682742	5619776653	1093911	831.36	281.07	19	53	-1	10.19	10.19
