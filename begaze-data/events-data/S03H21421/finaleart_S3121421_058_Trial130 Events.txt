[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:59
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4591055600	# Message: void.jpg
Saccade L	1	1	4591067420	4591147038	79618	1360.53	523.23	1374.60	414.43	2.64	95.75	1.00	33.18	1810.13	-2092.75	971.49
Saccade R	1	1	4591067420	4591147038	79618	1360.53	523.23	1374.60	414.43	2.64	95.75	1.00	33.18	1810.13	-2092.75	971.49
Fixation L	1	1	4591147038	4591445518	298480	1373.56	472.79	9	89	-1	12.09	12.09
Fixation R	1	1	4591147038	4591445518	298480	1373.56	472.79	9	89	-1	11.59	11.59
Saccade L	1	2	4591445518	4591485255	39737	1371.29	492.11	1319.42	492.54	1.46	71.88	1.00	36.68	1028.72	-1463.18	994.40
Saccade R	1	2	4591445518	4591505146	59628	1371.29	492.11	1266.21	500.96	2.44	71.88	1.00	40.90	1028.72	-1463.18	823.37
Blink L	1	1	4591485255	4591823493	338238
Blink R	1	1	4591505146	4591843357	338211
Fixation L	1	2	4591823493	4593017417	1193924	892.76	397.46	26	73	-1	11.27	11.27
Fixation R	1	2	4591843357	4593017417	1174060	892.94	396.74	26	73	-1	12.25	12.25
Saccade L	1	3	4593017417	4593057173	39756	901.62	423.90	906.25	393.80	0.63	25.95	0.00	15.91	322.51	-372.03	231.52
Saccade R	1	3	4593017417	4593057173	39756	901.62	423.90	906.25	393.80	0.63	25.95	0.00	15.91	322.51	-372.03	231.52
