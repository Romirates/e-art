[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4548232425	# Message: void.jpg
Fixation L	1	1	4548247498	4548406735	159237	1144.95	307.97	9	12	-1	14.15	14.15
Fixation R	1	1	4548247498	4548406735	159237	1144.95	307.97	9	12	-1	14.39	14.39
Saccade L	1	1	4548406735	4548446485	39750	1138.11	313.63	1343.43	425.19	2.11	182.72	0.50	53.12	4396.99	-4279.78	2761.18
Saccade R	1	1	4548406735	4548446485	39750	1138.11	313.63	1343.43	425.19	2.11	182.72	0.50	53.12	4396.99	-4279.78	2761.18
Fixation L	1	2	4548446485	4548685217	238732	1346.63	445.33	12	69	-1	14.32	14.32
Fixation R	1	2	4548446485	4548685217	238732	1346.63	445.33	12	69	-1	14.27	14.27
Saccade L	1	2	4548685217	4548764842	79625	1349.15	492.42	1329.24	598.14	2.22	86.23	0.25	27.85	1878.29	-1922.83	887.09
Saccade R	1	2	4548685217	4548764842	79625	1349.15	492.42	1329.24	598.14	2.22	86.23	0.25	27.85	1878.29	-1922.83	887.09
Fixation L	1	3	4548764842	4549063335	298493	1290.18	624.06	51	42	-1	13.33	13.33
Fixation R	1	3	4548764842	4549063335	298493	1290.18	624.06	51	42	-1	12.77	12.77
Blink L	1	1	4549063335	4549262316	198981
Blink R	1	1	4549063335	4549262316	198981
Fixation L	1	4	4549262316	4550217369	955053	983.28	326.14	27	69	-1	12.23	12.23
Fixation R	1	4	4549262316	4550217369	955053	983.28	326.14	27	69	-1	12.87	12.87
