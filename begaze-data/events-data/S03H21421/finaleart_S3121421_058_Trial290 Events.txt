[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:14
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5556314136	# Message: void.jpg
Fixation L	1	1	5556329609	5556528473	198864	879.46	678.59	24	55	-1	9.22	9.22
Fixation R	1	1	5556329609	5556528473	198864	879.46	678.59	24	55	-1	8.18	8.18
Saccade L	1	1	5556528473	5556568217	39744	893.73	645.90	1034.61	647.26	2.64	156.65	0.50	66.36	3716.38	-615.35	1183.33
Saccade R	1	1	5556568217	5556588224	20007	1034.61	647.26	911.25	689.94	1.70	156.65	1.00	84.97	400.17	-3188.58	1401.37
Fixation R	1	2	5556588224	5557025687	437463	926.52	648.73	20	63	-1	10.68	10.68
Fixation L	1	2	5556627967	5557025687	397720	927.57	646.33	14	34	-1	10.43	10.43
Blink L	1	1	5557025687	5557244553	218866
Blink R	1	1	5557025687	5557244553	218866
Fixation L	1	3	5557244553	5557861010	616457	947.66	518.33	36	32	-1	10.42	10.42
Fixation R	1	3	5557244553	5557861010	616457	947.66	518.33	36	32	-1	11.12	11.12
Saccade L	1	2	5557861010	5557881003	19993	964.50	520.79	988.48	397.75	0.70	126.79	1.00	35.05	3074.14	-2835.94	2028.95
Saccade R	1	2	5557861010	5557881003	19993	964.50	520.79	988.48	397.75	0.70	126.79	1.00	35.05	3074.14	-2835.94	2028.95
Fixation L	1	4	5557881003	5558318490	437487	1011.34	396.33	31	19	-1	10.43	10.43
Fixation R	1	4	5557881003	5558318490	437487	1011.34	396.33	31	19	-1	11.22	11.22
