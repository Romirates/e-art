[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:00
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4632400929	# Message: void.jpg
Fixation L	1	1	4632412668	4633108752	696084	645.62	611.72	34	33	-1	11.05	11.05
Fixation R	1	1	4632412668	4633108752	696084	645.62	611.72	34	33	-1	12.00	12.00
Blink L	1	1	4633108752	4633307737	198985
Blink R	1	1	4633108752	4633307737	198985
Fixation L	1	2	4633307737	4634023689	715952	1048.60	607.24	34	65	-1	12.16	12.16
Fixation R	1	2	4633307737	4634023689	715952	1048.60	607.24	34	65	-1	13.10	13.10
Saccade L	1	1	4634023689	4634043556	19867	1032.35	631.83	1032.47	650.12	0.31	33.43	1.00	15.66	749.81	116.88	382.62
Saccade R	1	1	4634023689	4634043556	19867	1032.35	631.83	1032.47	650.12	0.31	33.43	1.00	15.66	749.81	116.88	382.62
Fixation L	1	3	4634043556	4634401540	357984	1032.69	634.79	7	46	-1	12.36	12.36
Fixation R	1	3	4634043556	4634401540	357984	1032.69	634.79	7	46	-1	13.38	13.38
