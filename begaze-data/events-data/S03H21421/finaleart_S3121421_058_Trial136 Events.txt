[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:00
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4613780445	# Message: void.jpg
Fixation L	1	1	4613790557	4615800315	2009758	1248.46	522.91	30	50	-1	12.51	12.51
Fixation R	1	1	4613790557	4615800315	2009758	1248.46	522.91	30	50	-1	11.66	11.66
