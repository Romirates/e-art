[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:53
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4234908271	# Message: void.jpg
Fixation L	1	1	4234927077	4236219871	1292794	820.68	276.02	23	59	-1	13.07	13.07
Fixation R	1	1	4234927077	4236219871	1292794	820.68	276.02	23	59	-1	13.24	13.24
Saccade L	1	1	4236219871	4236279604	59733	811.80	265.78	1099.08	369.40	3.64	151.90	0.67	60.94	3684.58	-2950.95	2120.46
Saccade R	1	1	4236219871	4236279604	59733	811.80	265.78	1099.08	369.40	3.64	151.90	0.67	60.94	3684.58	-2950.95	2120.46
Fixation L	1	2	4236279604	4237035308	755704	1103.37	393.29	26	65	-1	13.57	13.57
Fixation R	1	2	4236279604	4237035308	755704	1103.37	393.29	26	65	-1	13.50	13.50
