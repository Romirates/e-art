[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:05
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4969477011	# Message: void.jpg
Fixation L	1	1	4969488807	4969687671	198864	694.87	922.14	2	94	-1	10.33	10.33
Fixation R	1	1	4969488807	4969687671	198864	694.87	922.14	2	94	-1	10.82	10.82
Saccade L	1	1	4969687671	4969767294	79623	694.61	907.67	462.90	691.33	5.76	222.89	0.75	72.34	4230.88	-5429.35	2619.28
Saccade R	1	1	4969687671	4969767294	79623	694.61	907.67	462.90	691.33	5.76	222.89	0.75	72.34	4230.88	-5429.35	2619.28
Fixation L	1	2	4969767294	4970165018	397724	477.64	708.73	23	66	-1	10.26	10.26
Fixation R	1	2	4969767294	4970165018	397724	477.64	708.73	23	66	-1	11.44	11.44
Saccade L	1	2	4970165018	4970363998	198980	471.01	686.44	1057.04	558.90	12.68	123.15	0.80	63.71	2076.99	-1849.12	698.53
Saccade R	1	2	4970165018	4970363998	198980	471.01	686.44	1057.04	558.90	12.68	123.15	0.80	63.71	2076.99	-1849.12	698.53
Fixation L	1	3	4970363998	4971358459	994461	1081.63	589.35	35	59	-1	10.75	10.75
Fixation R	1	3	4970363998	4971358459	994461	1081.63	589.35	35	59	-1	11.12	11.12
Saccade L	1	3	4971358459	4971378308	19849	1088.54	617.90	1087.17	631.98	0.17	14.31	1.00	8.57	534.72	-182.03	327.16
Saccade R	1	3	4971358459	4971378308	19849	1088.54	617.90	1087.17	631.98	0.17	14.31	1.00	8.57	534.72	-182.03	327.16
Fixation L	1	4	4971378308	4971477810	99502	1086.49	623.36	2	49	-1	10.64	10.64
Fixation R	1	4	4971378308	4971477810	99502	1086.49	623.36	2	49	-1	11.19	11.19
