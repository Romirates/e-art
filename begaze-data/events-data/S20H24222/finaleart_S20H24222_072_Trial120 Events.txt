[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:52:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11637971444	# Message: void.jpg
Fixation L	1	1	11637978853	11638177870	199017	899.73	258.84	11	24	-1	10.22	10.22
Fixation R	1	1	11637978853	11638177870	199017	899.73	258.84	11	24	-1	8.29	8.29
Blink L	1	1	11638177870	11638336978	159108
Blink R	1	1	11638177870	11638336978	159108
Fixation L	1	2	11638336978	11639749166	1412188	959.01	598.53	23	52	-1	11.50	11.50
Fixation R	1	2	11638336978	11639749166	1412188	959.01	598.53	23	52	-1	11.52	11.52
Saccade L	1	1	11639749166	11639769025	19859	941.47	617.32	909.63	518.93	0.75	104.60	1.00	37.78	2538.68	-2086.79	1607.21
Saccade R	1	1	11639749166	11639769025	19859	941.47	617.32	909.63	518.93	0.75	104.60	1.00	37.78	2538.68	-2086.79	1607.21
Fixation L	1	3	11639769025	11639967903	198878	898.87	527.64	16	24	-1	12.62	12.62
Fixation R	1	3	11639769025	11639967903	198878	898.87	527.64	16	24	-1	12.79	12.79
