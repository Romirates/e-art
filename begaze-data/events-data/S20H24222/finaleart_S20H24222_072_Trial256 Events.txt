[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:54
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12222544759	# Message: void.jpg
Saccade L	1	1	12222545892	12222605636	59744	1068.47	612.01	833.33	490.58	3.57	139.33	0.67	59.76	3483.21	-2783.00	2391.58
Saccade R	1	1	12222545892	12222605636	59744	1068.47	612.01	833.33	490.58	3.57	139.33	0.67	59.76	3483.21	-2783.00	2391.58
Fixation L	1	1	12222605636	12223102852	497216	849.72	468.16	30	32	-1	10.79	10.79
Fixation R	1	1	12222605636	12223102852	497216	849.72	468.16	30	32	-1	11.46	11.46
Blink L	1	1	12223102852	12223301742	198890
Blink R	1	1	12223102852	12223301742	198890
Fixation L	1	2	12223301742	12224534923	1233181	893.10	660.02	28	37	-1	11.17	11.17
Fixation R	1	2	12223301742	12224534923	1233181	893.10	660.02	28	37	-1	12.93	12.93
