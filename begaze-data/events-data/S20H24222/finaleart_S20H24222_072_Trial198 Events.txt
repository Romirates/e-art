[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:29
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11917856579	# Message: void.jpg
Fixation L	1	1	11917872539	11918349875	477336	621.40	503.35	25	15	-1	11.69	11.69
Fixation R	1	1	11917872539	11918349875	477336	621.40	503.35	25	15	-1	12.49	12.49
Blink L	1	1	11918349875	11918568614	218739
Blink R	1	1	11918349875	11918568614	218739
Fixation L	1	2	11918568614	11919165347	596733	835.83	689.21	17	64	-1	12.53	12.53
Fixation R	1	2	11918568614	11919165347	596733	835.83	689.21	17	64	-1	12.61	12.61
Saccade L	1	1	11919165347	11919185227	19880	834.09	654.53	963.85	614.16	0.92	137.45	1.00	46.35	3177.24	-3288.53	2391.61
Saccade R	1	1	11919165347	11919185227	19880	834.09	654.53	963.85	614.16	0.92	137.45	1.00	46.35	3177.24	-3288.53	2391.61
Fixation L	1	3	11919185227	11919563201	377974	944.09	592.57	31	66	-1	11.68	11.68
Fixation R	1	3	11919185227	11919563201	377974	944.09	592.57	31	66	-1	11.82	11.82
Saccade L	1	2	11919563201	11919583080	19879	937.94	577.91	897.67	576.82	0.29	40.75	1.00	14.79	1007.68	-557.26	529.34
Saccade R	1	2	11919563201	11919583080	19879	937.94	577.91	897.67	576.82	0.29	40.75	1.00	14.79	1007.68	-557.26	529.34
Fixation L	1	4	11919583080	11919841587	258507	861.63	560.96	49	30	-1	11.69	11.69
Fixation R	1	4	11919583080	11919841587	258507	861.63	560.96	49	30	-1	11.96	11.96
