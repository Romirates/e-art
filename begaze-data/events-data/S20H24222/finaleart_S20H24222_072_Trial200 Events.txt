[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:30
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11923959390	# Message: void.jpg
Fixation L	1	1	11923978764	11924356618	377854	1242.35	155.42	22	63	-1	12.13	12.13
Fixation R	1	1	11923978764	11924356618	377854	1242.35	155.42	22	63	-1	12.08	12.08
Blink L	1	1	11924356618	11924456120	99502
Blink R	1	1	11924356618	11924456120	99502
Blink L	1	2	11924475994	11924654990	178996
Blink R	1	2	11924475994	11924654990	178996
Fixation L	1	2	11924654990	11925550072	895082	992.19	438.94	26	44	-1	13.85	13.85
Fixation R	1	2	11924654990	11925550072	895082	992.19	438.94	26	44	-1	13.11	13.11
Saccade L	1	1	11925550072	11925589815	39743	979.72	452.57	918.81	591.32	1.64	106.64	1.00	41.15	2337.25	-2343.94	1829.68
Saccade R	1	1	11925550072	11925589815	39743	979.72	452.57	918.81	591.32	1.64	106.64	1.00	41.15	2337.25	-2343.94	1829.68
Fixation L	1	3	11925589815	11925908053	318238	906.69	580.89	15	31	-1	14.58	14.58
Fixation R	1	3	11925589815	11925908053	318238	906.69	580.89	15	31	-1	13.92	13.92
Saccade L	1	2	11925908053	11925947927	39874	903.59	585.40	854.67	596.46	0.99	70.03	0.50	24.89	1643.12	0.00	716.08
Saccade R	1	2	11925908053	11925947927	39874	903.59	585.40	854.67	596.46	0.99	70.03	0.50	24.89	1643.12	0.00	716.08
