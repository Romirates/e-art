[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:14
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11807823992	# Message: void.jpg
Fixation L	1	1	11807840456	11808417195	576739	924.25	554.16	13	58	-1	16.95	16.95
Fixation R	1	1	11807840456	11808417195	576739	924.25	554.16	13	58	-1	17.24	17.24
Blink L	1	1	11808417195	11808576409	159214
Blink R	1	1	11808417195	11808576409	159214
Fixation L	1	2	11808576409	11809531129	954720	980.27	571.78	19	63	-1	13.11	13.11
Fixation R	1	2	11808576409	11809531129	954720	980.27	571.78	19	63	-1	14.15	14.15
Saccade L	1	1	11809531129	11809551001	19872	965.37	556.76	896.74	468.38	0.81	113.18	1.00	40.77	2668.86	-2334.41	1757.11
Saccade R	1	1	11809531129	11809551001	19872	965.37	556.76	896.74	468.38	0.81	113.18	1.00	40.77	2668.86	-2334.41	1757.11
Fixation L	1	3	11809551001	11809809489	258488	896.32	494.11	23	47	-1	13.78	13.78
Fixation R	1	3	11809551001	11809809489	258488	896.32	494.11	23	47	-1	15.51	15.51
