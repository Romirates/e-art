[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S13H22431-finaleart-1.idf
Date:	27.03.2019 15:42:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S13H22431
Description:	Jimmy Petit

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	28340997669	# Message: void.jpg
Fixation L	1	1	28340997783	28341534795	537012	574.55	409.08	27	42	-1	13.80	13.80
Fixation R	1	1	28340997783	28341534795	537012	574.55	409.08	27	42	-1	13.31	13.31
Saccade L	1	1	28341534795	28341574555	39760	569.77	397.24	599.30	162.47	2.30	123.96	0.50	57.82	3073.93	-2484.10	2585.58
Saccade R	1	1	28341534795	28341574555	39760	569.77	397.24	599.30	162.47	2.30	123.96	0.50	57.82	3073.93	-2484.10	2585.58
Fixation L	1	2	28341574555	28341892779	318224	630.87	150.01	56	36	-1	13.35	13.35
Fixation R	1	2	28341574555	28341892779	318224	630.87	150.01	56	36	-1	12.13	12.13
Blink R	1	1	28341952403	28342051897	99494
Fixation L	1	3	28342032023	28342588881	556858	641.73	395.85	28	56	-1	14.28	14.28
Fixation R	1	3	28342051897	28342588881	536984	641.24	396.37	28	56	-1	15.22	15.22
Saccade L	1	2	28342588881	28342608890	20009	627.58	423.98	618.96	529.34	0.72	106.93	1.00	36.12	2582.50	-2397.18	1845.50
Saccade R	1	2	28342588881	28342608890	20009	627.58	423.98	618.96	529.34	0.72	106.93	1.00	36.12	2582.50	-2397.18	1845.50
Fixation L	1	4	28342608890	28342986729	377839	592.55	526.95	40	15	-1	14.70	14.70
Fixation R	1	4	28342608890	28342986729	377839	592.55	526.95	40	15	-1	15.70	15.70
