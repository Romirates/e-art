[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31388185308	# Message: void.jpg
Fixation L	1	1	31388190735	31388309959	119224	1665.26	792.80	2	5	-1	13.01	13.01
Fixation R	1	1	31388190735	31388309959	119224	1665.26	792.80	2	5	-1	12.62	12.62
Saccade L	1	1	31388309959	31388349840	39881	1665.14	791.19	1408.14	761.54	2.30	154.01	1.00	57.70	3835.52	-3072.84	2931.20
Saccade R	1	1	31388309959	31388349840	39881	1665.14	791.19	1408.14	761.54	2.30	154.01	1.00	57.70	3835.52	-3072.84	2931.20
Fixation L	1	2	31388349840	31388608321	258481	1383.15	751.39	31	24	-1	13.34	13.34
Fixation R	1	2	31388349840	31388608321	258481	1383.15	751.39	31	24	-1	13.08	13.08
Blink L	1	1	31388608321	31389125541	517220
Blink R	1	1	31388608321	31389125541	517220
Fixation L	1	3	31389125541	31390179630	1054089	757.61	595.30	28	59	-1	11.62	11.62
Fixation R	1	3	31389125541	31390179630	1054089	757.61	595.30	28	59	-1	12.02	12.02
