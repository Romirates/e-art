[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30681165855	# Message: void.jpg
Fixation L	1	1	30681171432	30681270923	99491	617.74	434.05	8	14	-1	11.58	11.58
Fixation R	1	1	30681171432	30681270923	99491	617.74	434.05	8	14	-1	12.92	12.92
Saccade L	1	1	30681270923	30681310731	39808	620.87	439.11	328.32	603.05	2.85	223.09	1.00	71.69	5530.71	-5378.17	4110.33
Saccade R	1	1	30681270923	30681310731	39808	620.87	439.11	328.32	603.05	2.85	223.09	1.00	71.69	5530.71	-5378.17	4110.33
Fixation L	1	2	30681310731	30681569287	258556	344.55	635.93	42	56	-1	12.24	12.24
Fixation R	1	2	30681310731	30681569287	258556	344.55	635.93	42	56	-1	13.10	13.10
Blink L	1	1	30681569287	30682265504	696217
Blink R	1	1	30681569287	30682265504	696217
Fixation L	1	3	30682265504	30683160463	894959	873.88	577.41	32	62	-1	11.83	11.83
Fixation R	1	3	30682265504	30683160463	894959	873.88	577.41	32	62	-1	12.04	12.04
