[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:42:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30312711937	# Message: void.jpg
Fixation L	1	1	30312718455	30312817828	99373	384.56	861.71	7	8	-1	13.06	13.06
Fixation R	1	1	30312718455	30312817828	99373	384.56	861.71	7	8	-1	13.18	13.18
Saccade L	1	1	30312817828	30312877606	59778	380.29	858.17	808.61	953.39	5.36	233.84	0.33	89.70	5680.31	-5076.55	4328.12
Saccade R	1	1	30312817828	30312877606	59778	380.29	858.17	808.61	953.39	5.36	233.84	0.33	89.70	5680.31	-5076.55	4328.12
Fixation L	1	2	30312877606	30313156043	278437	804.14	941.91	12	29	-1	17.55	17.55
Fixation R	1	2	30312877606	30313156043	278437	804.14	941.91	12	29	-1	13.42	13.42
Blink L	1	1	30313156043	30313673149	517106
Blink R	1	1	30313156043	30313673149	517106
Fixation L	1	3	30313673149	30314230136	556987	805.89	521.00	15	32	-1	13.90	13.90
Fixation R	1	3	30313673149	30314230136	556987	805.89	521.00	15	32	-1	14.34	14.34
Saccade L	1	2	30314230136	30314249998	19862	816.51	531.99	747.91	704.97	1.04	188.18	1.00	52.15	4626.54	-4428.73	3113.04
Saccade R	1	2	30314230136	30314249998	19862	816.51	531.99	747.91	704.97	1.04	188.18	1.00	52.15	4626.54	-4428.73	3113.04
Fixation L	1	4	30314249998	30314707510	457512	763.67	754.05	24	69	-1	13.47	13.47
Fixation R	1	4	30314249998	30314707510	457512	763.67	754.05	24	69	-1	13.95	13.95
