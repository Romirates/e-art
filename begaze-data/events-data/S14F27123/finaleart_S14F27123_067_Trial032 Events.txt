[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:42:52
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30361535237	# Message: void.jpg
Saccade L	1	1	30361548702	30361588442	39740	1203.91	670.58	1090.17	683.93	1.57	112.15	1.00	39.41	2803.71	-2442.71	1506.60
Saccade R	1	1	30361548702	30361588442	39740	1203.91	670.58	1090.17	683.93	1.57	112.15	1.00	39.41	2803.71	-2442.71	1506.60
Fixation L	1	1	30361588442	30361787321	198879	1052.79	704.56	52	38	-1	13.53	13.53
Fixation R	1	1	30361588442	30361787321	198879	1052.79	704.56	52	38	-1	13.09	13.09
Saccade L	1	2	30361787321	30361807203	19882	1037.22	722.33	1019.13	725.90	0.21	18.65	1.00	10.46	429.66	-221.18	279.41
Saccade R	1	2	30361787321	30361807203	19882	1037.22	722.33	1019.13	725.90	0.21	18.65	1.00	10.46	429.66	-221.18	279.41
Fixation L	1	2	30361807203	30361986171	178968	1006.19	724.50	18	9	-1	13.36	13.36
Fixation R	1	2	30361807203	30361986171	178968	1006.19	724.50	18	9	-1	13.24	13.24
Blink L	1	1	30361986171	30362085683	99512
Blink R	1	1	30361986171	30362085683	99512
Blink L	1	2	30362105540	30362463529	357989
Blink R	1	2	30362105540	30362463529	357989
Fixation L	1	3	30362463529	30363537610	1074081	696.12	648.23	22	61	-1	13.54	13.54
Fixation R	1	3	30362463529	30363537610	1074081	696.12	648.23	22	61	-1	13.30	13.30
