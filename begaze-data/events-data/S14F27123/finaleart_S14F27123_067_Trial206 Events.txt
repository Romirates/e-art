[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31412652893	# Message: void.jpg
Saccade L	1	1	31412654909	31412714657	59748	627.62	252.34	284.50	283.10	4.48	330.35	0.67	75.05	8258.71	-8044.19	3427.28
Saccade R	1	1	31412654909	31412714657	59748	627.62	252.34	284.50	283.10	4.48	330.35	0.67	75.05	8258.71	-8044.19	3427.28
Fixation L	1	1	31412714657	31412853771	139114	286.53	328.45	16	77	-1	14.18	14.18
Fixation R	1	1	31412714657	31412853771	139114	286.53	328.45	16	77	-1	14.61	14.61
Saccade L	1	2	31412853771	31412893637	39866	288.72	358.31	293.55	800.09	4.06	285.13	0.50	101.74	7006.29	-6938.83	5891.97
Saccade R	1	2	31412853771	31412893637	39866	288.72	358.31	293.55	800.09	4.06	285.13	0.50	101.74	7006.29	-6938.83	5891.97
Fixation L	1	2	31412893637	31413171996	278359	299.86	788.19	19	40	-1	14.00	14.00
Fixation R	1	2	31412893637	31413171996	278359	299.86	788.19	19	40	-1	14.84	14.84
Blink L	1	1	31413171996	31413669230	497234
Blink R	1	1	31413171996	31413669230	497234
Fixation L	1	3	31413669230	31414643947	974717	736.95	634.99	30	49	-1	12.31	12.31
Fixation R	1	3	31413669230	31414643947	974717	736.95	634.99	30	49	-1	12.43	12.43
