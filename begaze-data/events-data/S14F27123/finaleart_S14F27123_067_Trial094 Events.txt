[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:11
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30638382232	# Message: void.jpg
Fixation L	1	1	30638387922	30639263007	875085	943.28	703.83	21	43	-1	13.55	13.55
Fixation R	1	1	30638387922	30639263007	875085	943.28	703.83	21	43	-1	13.22	13.22
Saccade L	1	1	30639263007	30639282882	19875	957.34	726.79	955.76	794.73	0.70	68.74	1.00	35.32	1482.07	-1559.47	1472.76
Saccade R	1	1	30639263007	30639282882	19875	957.34	726.79	955.76	794.73	0.70	68.74	1.00	35.32	1482.07	-1559.47	1472.76
Fixation L	1	2	30639282882	30640376824	1093942	936.98	749.67	27	63	-1	15.98	15.98
Fixation R	1	2	30639282882	30640376824	1093942	936.98	749.67	27	63	-1	15.82	15.82
