[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31394299707	# Message: void.jpg
Saccade L	1	1	31394316831	31394356574	39743	586.55	404.16	352.24	814.55	5.55	343.05	0.50	139.59	4750.73	-7944.59	4318.97
Saccade R	1	1	31394316831	31394356574	39743	586.55	404.16	352.24	814.55	5.55	343.05	0.50	139.59	4750.73	-7944.59	4318.97
Fixation L	1	1	31394356574	31394734551	377977	383.71	843.15	51	38	-1	14.57	14.57
Fixation R	1	1	31394356574	31394734551	377977	383.71	843.15	51	38	-1	15.54	15.54
Blink L	1	1	31394734551	31395251660	517109
Blink R	1	1	31394734551	31395251660	517109
Fixation L	1	2	31395251660	31396285986	1034326	755.61	536.37	20	55	-1	11.25	11.25
Fixation R	1	2	31395251660	31396285986	1034326	755.61	536.37	20	55	-1	11.66	11.66
