[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31290530268	# Message: void.jpg
Fixation L	1	1	31290532209	31290751066	218857	263.57	876.13	30	57	-1	12.44	12.44
Fixation R	1	1	31290532209	31290751066	218857	263.57	876.13	30	57	-1	13.36	13.36
Saccade L	1	1	31290751066	31290770944	19878	257.36	870.63	255.87	840.17	0.37	35.93	1.00	18.50	797.86	-548.47	679.17
Saccade R	1	1	31290751066	31290770944	19878	257.36	870.63	255.87	840.17	0.37	35.93	1.00	18.50	797.86	-548.47	679.17
Fixation L	1	2	31290770944	31291009554	238610	252.26	809.12	9	43	-1	12.39	12.39
Fixation R	1	2	31290770944	31291009554	238610	252.26	809.12	9	43	-1	13.27	13.27
Blink L	1	1	31291009554	31291367665	358111
Blink R	1	1	31291009554	31291367665	358111
Fixation L	1	3	31291367665	31292521240	1153575	868.20	579.96	20	41	-1	13.14	13.14
Fixation R	1	3	31291367665	31292521240	1153575	868.20	579.96	20	41	-1	13.43	13.43
