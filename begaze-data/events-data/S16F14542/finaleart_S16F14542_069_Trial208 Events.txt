[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7086444788	# Message: void.jpg
Saccade L	1	1	7086447052	7086486789	39737	1111.94	646.61	1015.14	743.72	1.47	137.56	1.00	36.93	3438.98	-3055.38	1672.94
Saccade R	1	1	7086447052	7086486789	39737	1111.94	646.61	1015.14	743.72	1.47	137.56	1.00	36.93	3438.98	-3055.38	1672.94
Fixation L	1	1	7086486789	7087123296	636507	1007.06	745.97	11	49	-1	13.38	13.38
Fixation R	1	1	7086486789	7087123296	636507	1007.06	745.97	11	49	-1	13.02	13.02
Blink L	1	1	7087123296	7087600602	477306
Blink R	1	1	7087123296	7087600602	477306
Fixation L	1	2	7087600602	7088435955	835353	1119.65	516.07	15	72	-1	14.37	14.37
Fixation R	1	2	7087600602	7088435955	835353	1119.65	516.07	15	72	-1	13.90	13.90
