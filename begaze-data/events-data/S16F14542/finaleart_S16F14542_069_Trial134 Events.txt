[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:52
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6830906164	# Message: void.jpg
Fixation L	1	1	6830907247	6831543714	636467	818.33	471.43	16	54	-1	15.65	15.65
Fixation R	1	1	6830907247	6831543714	636467	818.33	471.43	16	54	-1	16.28	16.28
Blink L	1	1	6831543714	6831722700	178986
Blink R	1	1	6831543714	6831722700	178986
Fixation L	1	2	6831722700	6832896279	1173579	818.03	472.70	16	30	-1	15.03	15.03
Fixation R	1	2	6831722700	6832896279	1173579	818.03	472.70	16	30	-1	15.39	15.39
