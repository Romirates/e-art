[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7418164156	# Message: void.jpg
Fixation L	1	1	7418179202	7418378072	198870	559.37	732.97	5	13	-1	12.40	12.40
Fixation R	1	1	7418179202	7418378072	198870	559.37	732.97	5	13	-1	12.44	12.44
Saccade L	1	1	7418378072	7418397935	19863	556.33	723.86	692.06	719.36	1.02	137.35	1.00	51.48	3269.45	-3204.40	2655.92
Saccade R	1	1	7418378072	7418397935	19863	556.33	723.86	692.06	719.36	1.02	137.35	1.00	51.48	3269.45	-3204.40	2655.92
Fixation L	1	2	7418397935	7418537184	139249	752.90	706.15	72	24	-1	12.44	12.44
Fixation R	1	2	7418397935	7418537184	139249	752.90	706.15	72	24	-1	13.15	13.15
Saccade L	1	2	7418537184	7418636674	99490	764.76	695.17	776.43	696.80	1.62	99.64	1.00	16.28	27508.80	-33.41	4297.50
Saccade R	1	2	7418537184	7418636674	99490	764.76	695.17	776.43	696.80	1.62	99.64	1.00	16.28	27508.80	-33.41	4297.50
Blink L	1	1	7418636674	7418935030	298356
Blink R	1	1	7418636674	7418935030	298356
Fixation L	1	3	7418935030	7420168231	1233201	1078.16	542.47	40	58	-1	14.47	14.47
Fixation R	1	3	7418935030	7420168231	1233201	1078.16	542.47	40	58	-1	13.73	13.73
