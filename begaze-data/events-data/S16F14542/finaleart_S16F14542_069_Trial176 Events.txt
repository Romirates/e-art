[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:07
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6988766606	# Message: void.jpg
Fixation L	1	1	6988777455	6989274675	497220	978.41	673.81	7	33	-1	14.82	14.82
Fixation R	1	1	6988777455	6989274675	497220	978.41	673.81	7	33	-1	15.03	15.03
Blink L	1	1	6989274675	6990338882	1064207
Blink R	1	1	6989274675	6990338882	1064207
Fixation L	1	2	6990338882	6990458251	119369	996.48	523.34	5	14	-1	13.61	13.61
Fixation R	1	2	6990338882	6990537868	198986	996.85	512.20	5	55	-1	11.55	11.55
Blink L	1	2	6990458251	6990537868	79617
Fixation L	1	3	6990537868	6990756606	218738	991.27	468.93	14	20	-1	12.11	12.11
Fixation R	1	3	6990557749	6990756606	198857	990.64	468.36	13	20	-1	12.06	12.06
