[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:42
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7369357353	# Message: void.jpg
Fixation L	1	1	7369369160	7370124856	755696	699.98	607.07	19	41	-1	15.33	15.33
Fixation R	1	1	7369369160	7370124856	755696	699.98	607.07	19	41	-1	17.47	17.47
Blink L	1	1	7370124856	7370343730	218874
Blink R	1	1	7370124856	7370343730	218874
Fixation L	1	2	7370343730	7371338191	994461	941.84	466.48	21	53	-1	13.50	13.50
Fixation R	1	2	7370343730	7371338191	994461	941.84	466.48	21	53	-1	14.14	14.14
