[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:59
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6885838764	# Message: void.jpg
Saccade L	1	1	6885839506	6885919014	79508	378.01	754.00	942.16	641.24	7.72	280.61	0.75	97.15	6999.15	-7011.38	3883.47
Saccade R	1	1	6885839506	6885919014	79508	378.01	754.00	942.16	641.24	7.72	280.61	0.75	97.15	6999.15	-7011.38	3883.47
Fixation L	1	1	6885919014	6886396343	477329	957.39	621.71	28	32	-1	10.16	10.16
Fixation R	1	1	6885919014	6886396343	477329	957.39	621.71	28	32	-1	10.64	10.64
Blink L	1	1	6886396343	6886873825	477482
Blink R	1	1	6886396343	6886873825	477482
Fixation L	1	2	6886873825	6887390925	517100	1023.00	644.56	36	39	-1	12.29	12.29
Fixation R	1	2	6886873825	6887390925	517100	1023.00	644.56	36	39	-1	12.41	12.41
Saccade L	1	2	6887390925	6887430672	39747	1011.34	655.73	932.43	582.64	0.91	69.63	1.00	22.94	1670.68	-1458.35	1264.13
Saccade R	1	2	6887390925	6887430672	39747	1011.34	655.73	932.43	582.64	0.91	69.63	1.00	22.94	1670.68	-1458.35	1264.13
Fixation L	1	3	6887430672	6887828536	397864	929.67	572.58	23	30	-1	9.98	9.98
Fixation R	1	3	6887430672	6887828536	397864	929.67	572.58	23	30	-1	10.00	10.00
