[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6867545131	# Message: void.jpg
Fixation L	1	1	6867556747	6867954476	397729	296.35	1042.33	45	19	-1	14.04	14.04
Fixation R	1	1	6867556747	6867954476	397729	296.35	1042.33	45	19	-1	14.21	14.21
Blink L	1	1	6867954476	6868173373	218897
Blink R	1	1	6867954476	6868173373	218897
Fixation L	1	2	6868173373	6869247409	1074036	1029.40	451.75	55	37	-1	13.06	13.06
Fixation R	1	2	6868173373	6869247409	1074036	1029.40	451.75	55	37	-1	13.74	13.74
Saccade L	1	1	6869247409	6869267294	19885	990.03	443.86	980.34	446.40	0.33	34.88	1.00	16.61	25.42	-618.67	241.06
Saccade R	1	1	6869247409	6869267294	19885	990.03	443.86	980.34	446.40	0.33	34.88	1.00	16.61	25.42	-618.67	241.06
Fixation L	1	3	6869267294	6869525900	258606	963.79	427.77	28	28	-1	13.31	13.31
Fixation R	1	3	6869267294	6869525900	258606	963.79	427.77	28	28	-1	14.18	14.18
