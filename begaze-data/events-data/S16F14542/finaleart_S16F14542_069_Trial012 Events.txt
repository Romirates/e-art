[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6314964577	# Message: void.jpg
Blink L	1	1	6314977395	6315056899	79504
Blink R	1	1	6314977395	6315056899	79504
Fixation L	1	1	6315056899	6315593997	537098	1390.88	375.55	23	60	-1	16.15	16.15
Fixation R	1	1	6315056899	6315593997	537098	1390.88	375.55	23	60	-1	15.54	15.54
Saccade L	1	1	6315593997	6315673484	79487	1394.93	348.08	1390.65	350.00	1.80	83.68	1.00	22.65	7350.72	-248.78	1681.89
Saccade R	1	1	6315593997	6315673484	79487	1394.93	348.08	1390.65	350.00	1.80	83.68	1.00	22.65	7350.72	-248.78	1681.89
Blink L	1	2	6315673484	6315951972	278488
Blink R	1	2	6315673484	6315951972	278488
Fixation L	1	2	6315951972	6316508943	556971	779.39	519.69	7	34	-1	15.03	15.03
Fixation R	1	2	6315951972	6316508943	556971	779.39	519.69	7	34	-1	14.72	14.72
Saccade L	1	2	6316508943	6316528825	19882	783.46	519.12	933.91	519.02	0.81	152.16	1.00	40.84	3759.02	-3709.34	2504.17
Saccade R	1	2	6316508943	6316528825	19882	783.46	519.12	933.91	519.02	0.81	152.16	1.00	40.84	3759.02	-3709.34	2504.17
Fixation L	1	3	6316528825	6316966299	437474	930.76	523.09	17	23	-1	13.68	13.68
Fixation R	1	3	6316528825	6316966299	437474	930.76	523.09	17	23	-1	12.86	12.86
