[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:04
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6964292827	# Message: void.jpg
Fixation L	1	1	6964309756	6966000425	1690669	1035.53	473.01	22	68	-1	13.35	13.35
Fixation R	1	1	6964309756	6966000425	1690669	1035.53	473.01	22	68	-1	13.29	13.29
Saccade L	1	1	6966000425	6966020292	19867	1030.51	499.05	983.51	492.99	0.46	47.94	1.00	23.17	1138.43	-966.67	994.49
Saccade R	1	1	6966000425	6966020292	19867	1030.51	499.05	983.51	492.99	0.46	47.94	1.00	23.17	1138.43	-966.67	994.49
Fixation L	1	2	6966020292	6966278900	258608	939.71	496.16	52	12	-1	14.11	14.11
Fixation R	1	2	6966020292	6966278900	258608	939.71	496.16	52	12	-1	14.59	14.59
