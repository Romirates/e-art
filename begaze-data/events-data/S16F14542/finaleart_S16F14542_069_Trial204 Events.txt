[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7074198567	# Message: void.jpg
Fixation L	1	1	7074214633	7074473238	258605	305.97	1138.85	91	7	-1	12.44	12.44
Fixation R	1	1	7074214633	7074473238	258605	305.97	1138.85	91	7	-1	12.98	12.98
Saccade L	1	1	7074473238	7074493104	19866	291.64	1134.31	290.27	1133.06	0.03	2.43	1.00	1.55	6.81	-33.40	17.99
Saccade R	1	1	7074473238	7074493104	19866	291.64	1134.31	290.27	1133.06	0.03	2.43	1.00	1.55	6.81	-33.40	17.99
Fixation L	1	2	7074493104	7074711851	218747	289.56	1131.05	10	5	-1	11.99	11.99
Fixation R	1	2	7074493104	7074711851	218747	289.56	1131.05	10	5	-1	12.75	12.75
Blink L	1	1	7074711851	7074950592	238741
Blink R	1	1	7074711851	7074950592	238741
Fixation L	1	3	7074950592	7075825671	875079	1186.39	587.72	18	30	-1	13.05	13.05
Fixation R	1	3	7074950592	7075825671	875079	1186.39	587.72	18	30	-1	12.69	12.69
Blink L	1	2	7075825671	7076044534	218863
Blink R	1	2	7075825671	7076044534	218863
Fixation L	1	4	7076044534	7076183791	139257	755.58	408.31	19	55	-1	14.13	14.13
Fixation R	1	4	7076044534	7076183791	139257	755.58	408.31	19	55	-1	13.67	13.67
