[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:11
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7019250384	# Message: void.jpg
Fixation L	1	1	7019259010	7019497752	238742	952.95	619.78	10	17	-1	12.19	12.19
Fixation R	1	1	7019259010	7019497752	238742	952.95	619.78	10	17	-1	12.37	12.37
Blink L	1	1	7019497752	7019636980	139228
Blink R	1	1	7019497752	7019636980	139228
Fixation L	1	2	7019636980	7020273448	636468	1169.82	815.96	24	62	-1	12.37	12.37
Fixation R	1	2	7019636980	7020273448	636468	1169.82	815.96	24	62	-1	12.20	12.20
Saccade L	1	1	7020273448	7020313199	39751	1158.25	822.48	817.51	717.95	3.18	216.83	1.00	80.12	5392.44	-5033.56	4225.80
Saccade R	1	1	7020273448	7020313199	39751	1158.25	822.48	817.51	717.95	3.18	216.83	1.00	80.12	5392.44	-5033.56	4225.80
Fixation L	1	3	7020313199	7020512067	198868	775.21	687.40	54	44	-1	12.71	12.71
Fixation R	1	3	7020313199	7020512067	198868	775.21	687.40	54	44	-1	12.41	12.41
Saccade L	1	2	7020512067	7020531939	19872	763.15	676.78	762.23	670.59	0.10	6.56	1.00	4.97	-1.18	-106.97	37.92
Saccade R	1	2	7020512067	7020531939	19872	763.15	676.78	762.23	670.59	0.10	6.56	1.00	4.97	-1.18	-106.97	37.92
Fixation L	1	4	7020531939	7021228151	696212	757.01	675.44	27	22	-1	13.22	13.22
Fixation R	1	4	7020531939	7021228151	696212	757.01	675.44	27	22	-1	13.39	13.39
