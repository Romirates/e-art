[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7442553523	# Message: void.jpg
Blink L	1	1	7442564411	7442643916	79505
Blink R	1	1	7442564411	7442643916	79505
Saccade L	1	1	7442643916	7442683783	39867	692.98	612.23	699.42	547.73	8.15	919.33	1.00	204.53	1465.26	-22682.99	6422.82
Saccade R	1	1	7442643916	7442683783	39867	692.98	612.23	699.42	547.73	8.15	919.33	1.00	204.53	1465.26	-22682.99	6422.82
Fixation L	1	1	7442683783	7444553456	1869673	703.97	532.25	24	52	-1	13.15	13.15
Fixation R	1	1	7442683783	7444553456	1869673	703.97	532.25	24	52	-1	14.13	14.13
