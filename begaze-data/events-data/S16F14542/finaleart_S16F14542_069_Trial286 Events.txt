[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7412061547	# Message: void.jpg
Fixation L	1	1	7412072996	7412232113	159117	712.71	739.23	23	37	-1	13.37	13.37
Fixation R	1	1	7412072996	7412232113	159117	712.71	739.23	23	37	-1	13.62	13.62
Saccade L	1	1	7412232113	7412252017	19904	731.60	711.70	940.68	615.91	1.58	232.52	1.00	79.44	5766.68	-5442.29	3915.14
Saccade R	1	1	7412232113	7412252017	19904	731.60	711.70	940.68	615.91	1.58	232.52	1.00	79.44	5766.68	-5442.29	3915.14
Fixation L	1	2	7412252017	7412590083	338066	1004.41	618.61	74	24	-1	13.42	13.42
Fixation R	1	2	7412252017	7412590083	338066	1004.41	618.61	74	24	-1	12.94	12.94
Blink L	1	1	7412590083	7413087454	497371
Blink R	1	1	7412590083	7413087454	497371
Fixation L	1	3	7413087454	7414022150	934696	947.38	376.52	27	47	-1	14.44	14.44
Fixation R	1	3	7413087454	7414022150	934696	947.38	376.52	27	47	-1	14.27	14.27
Saccade L	1	2	7414022150	7414042015	19865	934.80	387.90	915.62	403.01	0.24	24.70	1.00	12.27	408.18	0.00	204.09
Saccade R	1	2	7414022150	7414042015	19865	934.80	387.90	915.62	403.01	0.24	24.70	1.00	12.27	408.18	0.00	204.09
