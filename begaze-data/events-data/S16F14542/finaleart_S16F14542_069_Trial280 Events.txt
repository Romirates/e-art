[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7393758345	# Message: void.jpg
Fixation L	1	1	7393774255	7394072469	298214	828.11	501.70	57	27	-1	17.21	17.21
Fixation R	1	1	7393774255	7394072469	298214	828.11	501.70	57	27	-1	16.01	16.01
Saccade L	1	1	7394072469	7394092488	20019	878.16	489.57	1004.24	487.67	0.98	127.55	1.00	48.97	3108.60	-3090.08	2442.17
Saccade R	1	1	7394072469	7394092488	20019	878.16	489.57	1004.24	487.67	0.98	127.55	1.00	48.97	3108.60	-3090.08	2442.17
Fixation L	1	2	7394092488	7395047177	954689	1010.71	480.38	30	25	-1	16.22	16.22
Fixation R	1	2	7394092488	7395047177	954689	1010.71	480.38	30	25	-1	14.91	14.91
Blink L	1	1	7395047177	7395226173	178996
Blink R	1	1	7395047177	7395126669	79492
Blink R	1	2	7395126669	7395226173	99504
Fixation L	1	3	7395226173	7395743269	517096	862.87	488.65	4	59	-1	14.72	14.72
Fixation R	1	3	7395226173	7395743269	517096	862.87	488.65	4	59	-1	14.76	14.76
