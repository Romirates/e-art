[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6540211911	# Message: void.jpg
Saccade L	1	1	6540218014	6540238001	19987	1069.95	433.49	932.32	543.24	1.67	178.02	1.00	83.58	1818.00	-3919.11	1912.37
Saccade R	1	1	6540218014	6540238001	19987	1069.95	433.49	932.32	543.24	1.67	178.02	1.00	83.58	1818.00	-3919.11	1912.37
Fixation L	1	1	6540238001	6542047918	1809917	936.01	601.22	19	76	-1	14.03	14.03
Fixation R	1	1	6540238001	6542047918	1809917	936.01	601.22	19	76	-1	14.37	14.37
