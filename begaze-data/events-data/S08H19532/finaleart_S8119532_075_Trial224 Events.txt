[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 16:00:59
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3427370402	# Message: void.jpg
Fixation L	1	1	3427382574	3427859903	477329	463.96	639.07	23	26	-1	16.51	16.51
Fixation R	1	1	3427382574	3427859903	477329	463.96	639.07	23	26	-1	17.23	17.23
Saccade L	1	1	3427859903	3427879917	20014	473.63	646.93	605.96	694.85	1.01	142.34	1.00	50.30	3287.41	-2958.21	2207.69
Saccade R	1	1	3427859903	3427879917	20014	473.63	646.93	605.96	694.85	1.01	142.34	1.00	50.30	3287.41	-2958.21	2207.69
Fixation L	1	2	3427879917	3428257770	377853	621.31	672.07	25	41	-1	15.96	15.96
Fixation R	1	2	3427879917	3428257770	377853	621.31	672.07	25	41	-1	16.33	16.33
Saccade L	1	2	3428257770	3428317395	59625	616.09	654.55	954.16	556.82	3.89	177.80	0.67	65.32	4283.40	-4128.15	2841.80
Saccade R	1	2	3428257770	3428317395	59625	616.09	654.55	954.16	556.82	3.89	177.80	0.67	65.32	4283.40	-4128.15	2841.80
Fixation L	1	3	3428317395	3429272088	954693	950.75	533.62	17	81	-1	14.60	14.60
Fixation R	1	3	3428317395	3429272088	954693	950.75	533.62	17	81	-1	14.88	14.88
Saccade L	1	3	3429272088	3429371594	99506	956.35	514.61	955.62	509.10	1.03	20.32	0.40	10.34	141.90	-482.48	133.95
Saccade R	1	3	3429272088	3429371594	99506	956.35	514.61	955.62	509.10	1.03	20.32	0.40	10.34	141.90	-482.48	133.95
