[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 16:00:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3087347074	# Message: void.jpg
Fixation L	1	1	3087362857	3087979460	616603	411.85	198.90	59	39	-1	14.12	14.12
Fixation R	1	1	3087362857	3087979460	616603	411.85	198.90	59	39	-1	15.38	15.38
Saccade L	1	1	3087979460	3088098823	119363	387.91	217.51	582.20	346.95	6.81	186.74	0.67	57.03	19217.39	-4181.79	4510.24
Saccade R	1	1	3087979460	3088098823	119363	387.91	217.51	582.20	346.95	6.81	186.74	0.67	57.03	19217.39	-4181.79	4510.24
Blink L	1	1	3088098823	3088317679	218856
Blink R	1	1	3088098823	3088317679	218856
Fixation L	1	2	3088317679	3088894412	576733	809.22	596.50	17	59	-1	16.40	16.40
Fixation R	1	2	3088317679	3088894412	576733	809.22	596.50	17	59	-1	18.17	18.17
Saccade L	1	2	3088894412	3088934155	39743	812.73	592.98	797.59	344.65	2.76	158.12	0.50	69.34	3604.59	-2466.13	2265.19
Saccade R	1	2	3088894412	3088934155	39743	812.73	592.98	797.59	344.65	2.76	158.12	0.50	69.34	3604.59	-2466.13	2265.19
Fixation L	1	3	3088934155	3089391632	457477	776.74	363.47	29	60	-1	14.44	14.44
Fixation R	1	3	3088934155	3089391632	457477	776.74	363.47	29	60	-1	16.54	16.54
