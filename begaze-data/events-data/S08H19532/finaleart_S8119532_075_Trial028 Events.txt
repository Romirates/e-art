[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 15:59:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2509573749	# Message: void.jpg
Fixation L	1	1	2509584468	2509839863	255395	1122.42	946.84	53	15	-1	14.07	14.07
Fixation R	1	1	2509584468	2509839863	255395	1122.42	946.84	53	15	-1	14.76	14.76
Saccade L	1	1	2509839863	2509862954	23091	1134.23	946.19	598.52	421.86	5.00	753.94	1.00	216.63	17528.25	-18735.94	12502.42
Saccade R	1	1	2509839863	2509862954	23091	1134.23	946.19	598.52	421.86	5.00	753.94	1.00	216.63	17528.25	-18735.94	12502.42
Fixation L	1	2	2509862954	2510181183	318229	594.43	405.23	6	51	-1	13.38	13.38
Fixation R	1	2	2509862954	2510181183	318229	594.43	405.23	6	51	-1	14.68	14.68
Blink L	1	1	2510181183	2510419915	238732
Blink R	1	1	2510181183	2510419915	238732
Fixation L	1	3	2510419915	2511155780	735865	734.23	433.58	45	46	-1	15.86	15.86
Fixation R	1	3	2510419915	2511155780	735865	734.23	433.58	45	46	-1	17.18	17.18
Saccade L	1	2	2511155780	2511175750	19970	769.14	463.37	899.05	493.81	1.19	134.96	1.00	59.61	2818.59	-2585.59	1957.29
Saccade R	1	2	2511155780	2511175750	19970	769.14	463.37	899.05	493.81	1.19	134.96	1.00	59.61	2818.59	-2585.59	1957.29
Fixation L	1	4	2511175750	2511633112	457362	899.53	498.68	23	61	-1	16.02	16.02
Fixation R	1	4	2511175750	2511633112	457362	899.53	498.68	23	61	-1	17.41	17.41
