[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 16:01:19
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3661430845	# Message: void.jpg
Saccade L	1	1	3661442731	3661542234	99503	932.44	368.15	626.20	302.43	5.45	171.18	1.00	54.82	3727.22	-3877.94	2038.52
Saccade R	1	1	3661442731	3661542234	99503	932.44	368.15	626.20	302.43	5.45	171.18	1.00	54.82	3727.22	-3877.94	2038.52
Fixation L	1	1	3661542234	3661736391	194157	603.11	282.19	50	37	-1	11.45	11.45
Fixation R	1	1	3661542234	3661736391	194157	603.11	282.19	50	37	-1	11.98	11.98
Saccade L	1	2	3661736391	3661760955	24564	575.36	265.09	567.03	235.82	1.35	190.02	1.00	54.77	19496.72	123.75	6027.15
Saccade R	1	2	3661736391	3661760955	24564	575.36	265.09	567.03	235.82	1.35	190.02	1.00	54.77	19496.72	123.75	6027.15
Blink L	1	1	3661760955	3661920060	159105
Blink R	1	1	3661760955	3661920060	159105
Fixation L	1	2	3661920060	3663451618	1531558	519.04	370.28	55	42	-1	14.44	14.44
Fixation R	1	2	3661920060	3663451618	1531558	519.04	370.28	55	42	-1	14.67	14.67
