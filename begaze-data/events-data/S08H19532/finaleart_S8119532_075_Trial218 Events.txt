[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 16:00:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3408892065	# Message: void.jpg
Fixation L	1	1	3408904839	3410317002	1412163	834.07	367.76	20	74	-1	13.00	13.00
Fixation R	1	1	3408904839	3410317002	1412163	834.07	367.76	20	74	-1	14.27	14.27
Saccade L	1	1	3410317002	3410336870	19868	834.48	406.43	836.05	413.12	0.15	11.78	1.00	7.48	42.72	-120.68	80.86
Saccade R	1	1	3410317002	3410336870	19868	834.48	406.43	836.05	413.12	0.15	11.78	1.00	7.48	42.72	-120.68	80.86
Fixation L	1	2	3410336870	3410714732	377862	841.23	412.38	21	26	-1	14.73	14.73
Fixation R	1	2	3410336870	3410714732	377862	841.23	412.38	21	26	-1	16.14	16.14
Saccade L	1	2	3410714732	3410754489	39757	840.71	416.70	839.67	408.73	1.82	101.81	1.00	45.88	2302.67	-2335.89	2180.45
Saccade R	1	2	3410714732	3410754489	39757	840.71	416.70	839.67	408.73	1.82	101.81	1.00	45.88	2302.67	-2335.89	2180.45
Fixation L	1	3	3410754489	3410933603	179114	846.56	417.51	14	19	-1	15.45	15.45
Fixation R	1	3	3410754489	3410933603	179114	846.56	417.51	14	19	-1	16.57	16.57
