[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:20
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5333938474	# Message: void.jpg
Fixation L	1	1	5333953299	5334610137	656838	680.39	637.20	28	31	-1	13.27	13.27
Fixation R	1	1	5333953299	5334610137	656838	680.39	637.20	28	31	-1	15.27	15.27
Saccade L	1	1	5334610137	5334689771	79634	691.93	627.83	944.67	548.11	3.71	100.51	0.50	46.64	2490.49	-1363.43	1262.27
Saccade R	1	1	5334610137	5334689771	79634	691.93	627.83	944.67	548.11	3.71	100.51	0.50	46.64	2490.49	-1363.43	1262.27
Fixation L	1	2	5334689771	5335923839	1234068	967.62	510.20	29	67	-1	13.33	13.33
Fixation R	1	2	5334689771	5335923839	1234068	967.62	510.20	29	67	-1	15.15	15.15
