[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5285115014	# Message: void.jpg
Fixation L	1	1	5285121939	5286633615	1511676	786.80	376.25	30	63	-1	12.59	12.59
Fixation R	1	1	5285121939	5286633615	1511676	786.80	376.25	30	63	-1	14.09	14.09
Saccade L	1	1	5286633615	5286653492	19877	789.31	395.87	789.15	413.71	0.20	18.06	1.00	10.17	351.26	-409.66	272.33
Saccade R	1	1	5286633615	5286653492	19877	789.31	395.87	789.15	413.71	0.20	18.06	1.00	10.17	351.26	-409.66	272.33
Fixation L	1	2	5286653492	5287110967	457475	782.25	404.00	12	29	-1	14.28	14.28
Fixation R	1	2	5286653492	5287110967	457475	782.25	404.00	12	29	-1	16.42	16.42
