[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:02
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5074340809	# Message: void.jpg
Fixation L	1	1	5074341272	5074460649	119377	857.07	820.81	4	5	-1	13.77	13.77
Fixation R	1	1	5074341272	5074460649	119377	857.07	820.81	4	5	-1	17.08	17.08
Saccade L	1	1	5074460649	5074500403	39754	858.06	818.01	1316.26	488.96	4.68	327.32	0.50	117.80	8116.79	-7884.45	6992.49
Saccade R	1	1	5074460649	5074500403	39754	858.06	818.01	1316.26	488.96	4.68	327.32	0.50	117.80	8116.79	-7884.45	6992.49
Fixation L	1	2	5074500403	5076331065	1830662	1312.03	528.32	31	64	-1	13.64	13.64
Fixation R	1	2	5074500403	5076331065	1830662	1312.03	528.32	31	64	-1	16.95	16.95
