[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5539920419	# Message: still-life-post-bottle-cup-and-fruit_cezanne_1871.jpg
Fixation L	1	1	5539933434	5540848764	915330	1120.55	837.97	10	45	-1	15.90	15.90
Fixation R	1	1	5539933434	5540848764	915330	1120.55	837.97	10	45	-1	17.27	17.27
Saccade L	1	1	5540848764	5540888502	39738	1122.31	831.43	817.58	686.50	3.09	256.51	1.00	77.69	6368.95	-6115.38	3980.36
Saccade R	1	1	5540848764	5540888502	39738	1122.31	831.43	817.58	686.50	3.09	256.51	1.00	77.69	6368.95	-6115.38	3980.36
Fixation L	1	2	5540888502	5542420684	1532182	773.37	684.26	60	30	-1	14.80	14.80
Fixation R	1	2	5540888502	5542420684	1532182	773.37	684.26	60	30	-1	16.12	16.12
Saccade L	1	2	5542420684	5542440561	19877	757.01	668.85	575.70	437.49	1.94	297.09	1.00	97.77	7379.94	-6635.17	5065.16
Saccade R	1	2	5542420684	5542440561	19877	757.01	668.85	575.70	437.49	1.94	297.09	1.00	97.77	7379.94	-6635.17	5065.16
Fixation L	1	3	5542440561	5542898160	457599	527.72	424.87	59	40	-1	14.78	14.78
Fixation R	1	3	5542440561	5542898160	457599	527.72	424.87	59	40	-1	16.41	16.41
Saccade L	1	3	5542898160	5542918172	20012	531.65	442.33	532.02	444.14	0.06	3.79	1.00	3.21	7.91	-48.03	24.81
Saccade R	1	3	5542898160	5542918172	20012	531.65	442.33	532.02	444.14	0.06	3.79	1.00	3.21	7.91	-48.03	24.81
Fixation L	1	4	5542918172	5543913004	994832	537.37	444.94	19	29	-1	14.68	14.68
Fixation R	1	4	5542918172	5543913004	994832	537.37	444.94	19	29	-1	16.42	16.42
