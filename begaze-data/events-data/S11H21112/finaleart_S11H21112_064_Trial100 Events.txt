[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:38:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4391771336	# Message: void.jpg
Fixation L	1	1	4391783365	4393394410	1611045	1274.49	479.63	36	63	-1	13.65	13.65
Fixation R	1	1	4391783365	4393255167	1471802	1276.01	478.22	25	63	-1	15.51	15.51
Fixation R	1	2	4393295046	4393772394	477348	1247.82	513.47	22	41	-1	18.26	18.26
Saccade L	1	1	4393394410	4393414286	19876	1254.59	494.39	1252.76	501.27	0.17	11.27	1.00	8.32	35.51	-101.69	47.87
Fixation L	1	2	4393414286	4393772394	358108	1244.73	519.22	13	30	-1	14.79	14.79
