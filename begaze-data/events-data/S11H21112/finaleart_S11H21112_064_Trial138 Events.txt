[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5117154270	# Message: void.jpg
Saccade L	1	1	5117159030	5117198771	39741	1095.20	497.00	1461.42	522.30	4.19	228.13	0.50	105.31	3670.64	-4546.13	2933.23
Saccade R	1	1	5117159030	5117198771	39741	1095.20	497.00	1461.42	522.30	4.19	228.13	0.50	105.31	3670.64	-4546.13	2933.23
Fixation L	1	1	5117198771	5119148809	1950038	1454.82	556.66	36	56	-1	14.53	14.53
Fixation R	1	1	5117198771	5119148809	1950038	1454.82	556.66	36	56	-1	15.62	15.62
