[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5147731065	# Message: void.jpg
Fixation L	1	1	5147734583	5148252286	517703	1055.13	666.77	21	39	-1	12.14	12.14
Fixation R	1	1	5147734583	5148252286	517703	1055.13	666.77	21	39	-1	14.53	14.53
Blink L	1	1	5148252286	5148650523	398237
Blink R	1	1	5148252286	5148650523	398237
Saccade L	1	1	5148650523	5149407262	756739	1831.89	338.57	1608.04	117.33	43.79	206.08	0.76	57.86	4630.85	-2975.52	1135.56
Saccade R	1	1	5148650523	5149407262	756739	1831.89	338.57	1608.04	117.33	43.79	206.08	0.76	57.86	4630.85	-2975.52	1135.56
Fixation L	1	2	5149407262	5149506735	99473	1634.08	97.49	46	50	-1	12.06	12.06
Fixation R	1	2	5149407262	5149506735	99473	1634.08	97.49	46	50	-1	12.43	12.43
