[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6102844654	# Message: void.jpg
Fixation L	1	1	6102851492	6102990708	139216	1110.09	538.97	6	14	-1	12.06	12.06
Fixation R	1	1	6102851492	6102990708	139216	1110.09	538.97	6	14	-1	14.13	14.13
Saccade L	1	1	6102990708	6103010715	20007	1114.90	530.85	1184.24	489.59	0.70	81.62	1.00	34.91	1848.30	-1969.19	1429.55
Saccade R	1	1	6102990708	6103010715	20007	1114.90	530.85	1184.24	489.59	0.70	81.62	1.00	34.91	1848.30	-1969.19	1429.55
Fixation L	1	2	6103010715	6104363772	1353057	1184.46	490.54	38	57	-1	12.34	12.34
Fixation R	1	2	6103010715	6104363772	1353057	1184.46	490.54	38	57	-1	14.28	14.28
Saccade L	1	2	6104363772	6104383656	19884	1151.38	495.59	1101.28	495.43	0.48	50.68	1.00	23.94	1142.51	-1258.56	941.16
Saccade R	1	2	6104363772	6104383656	19884	1151.38	495.59	1101.28	495.43	0.48	50.68	1.00	23.94	1142.51	-1258.56	941.16
Fixation L	1	3	6104383656	6104841256	457600	1080.29	481.36	29	23	-1	13.21	13.21
Fixation R	1	3	6104383656	6104841256	457600	1080.29	481.36	29	23	-1	15.25	15.25
