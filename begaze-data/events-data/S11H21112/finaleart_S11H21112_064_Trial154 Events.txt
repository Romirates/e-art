[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5166033535	# Message: void.jpg
Fixation L	1	1	5166034386	5166293264	258878	1146.24	629.13	3	19	-1	12.76	12.76
Fixation R	1	1	5166034386	5166293264	258878	1146.24	629.13	3	19	-1	14.92	14.92
Saccade L	1	1	5166293264	5166332995	39731	1148.05	626.11	798.91	523.62	3.54	240.86	1.00	89.00	5957.74	-5431.91	4082.93
Saccade R	1	1	5166293264	5166332995	39731	1148.05	626.11	798.91	523.62	3.54	240.86	1.00	89.00	5957.74	-5431.91	4082.93
Fixation L	1	2	5166332995	5166571986	238991	755.47	507.93	54	39	-1	12.45	12.45
Fixation R	1	2	5166332995	5166571986	238991	755.47	507.93	54	39	-1	14.82	14.82
Saccade L	1	2	5166571986	5167308583	736597	758.98	512.44	0.00	5.25	162.95	1794.20	1.00	221.23	41337.24	-44794.61	5697.29
Blink R	1	1	5166571986	5166771110	199124
Saccade R	1	2	5166771110	5167308583	537473	1443.47	-5.58	0.00	5.25	135.18	1794.20	1.00	251.51	41337.24	-44794.61	6961.50
Saccade L	1	3	5167447827	5167668798	220971	0.00	-0.85	0.00	-2.17	103.03	1796.05	0.09	466.24	44891.14	-44265.76	26099.27
Saccade R	1	3	5167447827	5167668798	220971	0.00	-0.85	0.00	-2.17	103.03	1796.05	0.09	466.24	44891.14	-44265.76	26099.27
Fixation L	1	3	5167668798	5168021137	352339	0.00	-2.13	0	2	-1	0.00	0.00
Fixation R	1	3	5167668798	5168021137	352339	0.00	-2.13	0	2	-1	0.00	0.00
