[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5315641577	# Message: void.jpg
Saccade L	1	1	5315657346	5315756857	99511	921.94	654.50	917.70	631.04	5.96	186.91	0.80	59.87	4322.45	-3985.23	2238.90
Saccade R	1	1	5315657346	5315756857	99511	921.94	654.50	917.70	631.04	5.96	186.91	0.80	59.87	4322.45	-3985.23	2238.90
Fixation L	1	1	5315756857	5316393439	636582	933.27	654.79	33	51	-1	12.46	12.46
Fixation R	1	1	5315756857	5316811302	1054445	931.01	652.67	33	51	-1	16.15	16.15
Fixation L	1	2	5316433305	5316870921	437616	926.98	642.21	13	85	-1	14.72	14.72
Saccade R	1	2	5316811302	5316831173	19871	920.81	657.75	921.87	607.53	0.47	50.81	1.00	23.54	1134.75	-548.31	867.49
Fixation R	1	2	5316831173	5317209155	377982	940.42	563.66	35	63	-1	16.96	16.96
Saccade L	1	2	5316870921	5316890794	19873	927.50	600.31	934.56	563.78	0.53	37.64	1.00	26.79	7.26	-639.92	400.02
Fixation L	1	3	5316890794	5317627015	736221	958.59	549.43	44	42	-1	14.96	14.96
Saccade R	1	3	5317209155	5317229025	19870	957.44	543.75	956.84	542.02	0.11	8.65	1.00	5.70	154.79	-102.04	104.51
Fixation R	1	3	5317229025	5317627015	397990	971.43	542.16	19	20	-1	16.97	16.97
