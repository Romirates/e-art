[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6108959079	# Message: void.jpg
Fixation L	1	1	6108960204	6110034659	1074455	1231.59	765.59	35	61	-1	12.50	12.50
Fixation R	1	1	6108960204	6110034659	1074455	1231.59	765.59	35	61	-1	14.15	14.15
Saccade L	1	1	6110034659	6110054533	19874	1211.59	726.05	1209.33	722.94	0.13	16.79	1.00	6.77	3.42	-322.44	111.87
Saccade R	1	1	6110034659	6110054533	19874	1211.59	726.05	1209.33	722.94	0.13	16.79	1.00	6.77	3.42	-322.44	111.87
Fixation L	1	2	6110054533	6110949995	895462	1184.46	707.79	41	28	-1	12.80	12.80
Fixation R	1	2	6110054533	6110949995	895462	1184.46	707.79	41	28	-1	14.64	14.64
