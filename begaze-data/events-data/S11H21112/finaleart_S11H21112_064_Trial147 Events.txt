[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5143666984	# Message: ronde_cross_1905.jpg
Fixation L	1	1	5143672391	5144030924	358533	1268.65	298.25	40	43	-1	13.92	13.92
Fixation R	1	1	5143672391	5144030924	358533	1268.65	298.25	40	43	-1	16.73	16.73
Saccade L	1	1	5144030924	5144070615	39691	1236.09	323.79	987.30	505.21	3.07	186.10	0.50	77.24	4073.82	-4541.39	3529.83
Saccade R	1	1	5144030924	5144070615	39691	1236.09	323.79	987.30	505.21	3.07	186.10	0.50	77.24	4073.82	-4541.39	3529.83
Fixation L	1	2	5144070615	5144448952	378337	983.03	490.49	17	29	-1	13.33	13.33
Fixation R	1	2	5144070615	5144448952	378337	983.03	490.49	17	29	-1	15.95	15.95
Saccade L	1	2	5144448952	5144488830	39878	991.36	479.57	780.49	493.07	1.86	138.77	1.00	46.61	3220.58	-3292.43	2512.35
Saccade R	1	2	5144448952	5144488830	39878	991.36	479.57	780.49	493.07	1.86	138.77	1.00	46.61	3220.58	-3292.43	2512.35
Fixation L	1	3	5144488830	5145305174	816344	785.09	499.77	16	29	-1	11.38	11.38
Fixation R	1	3	5144488830	5145305174	816344	785.09	499.77	16	29	-1	13.78	13.78
Saccade L	1	3	5145305174	5145325169	19995	793.27	508.08	947.49	505.24	1.24	156.00	1.00	62.12	3839.10	-3808.62	3205.50
Saccade R	1	3	5145305174	5145325169	19995	793.27	508.08	947.49	505.24	1.24	156.00	1.00	62.12	3839.10	-3808.62	3205.50
Fixation L	1	4	5145325169	5145504288	179119	1024.52	503.68	87	11	-1	11.02	11.02
Fixation R	1	4	5145325169	5145504288	179119	1024.52	503.68	87	11	-1	13.54	13.54
Saccade L	1	4	5145504288	5145544160	39872	1033.84	500.79	1219.02	500.92	1.79	184.09	1.00	44.89	4519.88	-4495.02	2417.21
Saccade R	1	4	5145504288	5145544160	39872	1033.84	500.79	1219.02	500.92	1.79	184.09	1.00	44.89	4519.88	-4495.02	2417.21
Fixation L	1	5	5145544160	5146141501	597341	1227.86	532.80	14	61	-1	11.62	11.62
Fixation R	1	5	5145544160	5146141501	597341	1227.86	532.80	14	61	-1	13.79	13.79
Saccade L	1	5	5146141501	5146181383	39882	1221.48	560.35	966.38	546.80	2.27	192.95	1.00	56.94	4787.54	-4703.57	3035.06
Saccade R	1	5	5146141501	5146181383	39882	1221.48	560.35	966.38	546.80	2.27	192.95	1.00	56.94	4787.54	-4703.57	3035.06
Fixation L	1	6	5146181383	5146460124	278741	941.55	539.66	32	28	-1	11.72	11.72
Fixation R	1	6	5146181383	5146460124	278741	941.55	539.66	32	28	-1	13.67	13.67
Saccade L	1	6	5146460124	5146480119	19995	952.62	523.94	1025.89	523.15	0.52	74.12	1.00	25.84	1740.72	-1738.58	1271.51
Saccade R	1	6	5146460124	5146480119	19995	952.62	523.94	1025.89	523.15	0.52	74.12	1.00	25.84	1740.72	-1738.58	1271.51
Fixation L	1	7	5146480119	5146699106	218987	1040.98	517.56	21	16	-1	11.95	11.95
Fixation R	1	7	5146480119	5146699106	218987	1040.98	517.56	21	16	-1	13.84	13.84
Saccade L	1	7	5146699106	5146738980	39874	1045.46	509.93	1036.46	718.18	1.83	128.06	0.50	45.84	3172.95	-2863.71	2481.79
Saccade R	1	7	5146699106	5146738980	39874	1045.46	509.93	1036.46	718.18	1.83	128.06	0.50	45.84	3172.95	-2863.71	2481.79
Fixation L	1	8	5146738980	5146898217	159237	1008.84	725.20	60	18	-1	12.23	12.23
Fixation R	1	8	5146738980	5146898217	159237	1008.84	725.20	60	18	-1	14.34	14.34
Saccade L	1	8	5146898217	5146918101	19884	976.35	734.36	758.31	702.72	1.28	222.77	1.00	64.39	5536.28	-5380.88	3802.58
Saccade R	1	8	5146898217	5146918101	19884	976.35	734.36	758.31	702.72	1.28	222.77	1.00	64.39	5536.28	-5380.88	3802.58
Fixation L	1	9	5146918101	5147236713	318612	768.70	700.18	15	24	-1	12.03	12.03
Fixation R	1	9	5146918101	5147236713	318612	768.70	700.18	15	24	-1	14.20	14.20
Saccade L	1	9	5147236713	5147276576	39863	770.53	699.75	506.16	692.80	2.36	197.35	0.50	59.24	4626.63	-4876.77	3319.70
Saccade R	1	9	5147236713	5147276576	39863	770.53	699.75	506.16	692.80	2.36	197.35	0.50	59.24	4626.63	-4876.77	3319.70
Fixation L	1	10	5147276576	5147634940	358364	504.20	699.86	6	25	-1	11.65	11.65
Fixation R	1	10	5147276576	5147634940	358364	504.20	699.86	6	25	-1	14.25	14.25
Saccade L	1	10	5147634940	5147654946	20006	505.05	708.07	749.74	680.06	1.72	249.00	1.00	85.74	6168.67	0.00	3084.33
Saccade R	1	10	5147634940	5147654946	20006	505.05	708.07	749.74	680.06	1.72	249.00	1.00	85.74	6168.67	0.00	3084.33
