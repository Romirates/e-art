[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:38:41
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4023417708	# Message: void.jpg
Fixation L	1	1	4023426244	4024062712	636468	1351.37	343.36	18	54	-1	13.83	13.83
Fixation R	1	1	4023426244	4024062712	636468	1351.37	343.36	18	54	-1	17.37	17.37
Blink L	1	1	4024062712	4024182069	119357
Blink R	1	1	4024062712	4024182069	119357
Blink L	1	2	4024281429	4024480437	199008
Blink R	1	2	4024281429	4024361069	79640
Blink R	1	3	4024380929	4024480437	99508
Fixation L	1	2	4024480437	4024699175	218738	856.18	470.35	27	65	-1	14.12	14.12
Fixation R	1	2	4024480437	4024699175	218738	856.18	470.35	27	65	-1	16.28	16.28
Saccade L	1	1	4024699175	4024778797	79622	844.52	483.92	834.00	480.33	8.01	323.38	1.00	100.62	7583.93	-7818.36	5183.73
Saccade R	1	1	4024699175	4024778797	79622	844.52	483.92	834.00	480.33	8.01	323.38	1.00	100.62	7583.93	-7818.36	5183.73
Fixation L	1	3	4024778797	4025415257	636460	840.46	480.91	12	35	-1	14.67	14.67
Fixation R	1	3	4024778797	4025415257	636460	840.46	480.91	12	35	-1	16.90	16.90
