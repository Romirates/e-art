[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5537871670	# Message: void.jpg
Fixation L	1	1	5537883925	5539794065	1910140	1117.98	804.28	24	72	-1	13.76	13.76
Fixation R	1	1	5537883925	5539794065	1910140	1117.98	804.28	24	72	-1	15.05	15.05
Saccade L	1	1	5539794065	5539873676	79611	1113.49	838.91	1110.48	850.10	0.27	7.03	0.25	3.35	118.04	-127.49	64.90
Saccade R	1	1	5539794065	5539873676	79611	1113.49	838.91	1110.48	850.10	0.27	7.03	0.25	3.35	118.04	-127.49	64.90
