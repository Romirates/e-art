[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:26
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10128422671	# Message: void.jpg
Fixation L	1	1	10128423445	10128861033	437588	778.86	723.00	74	17	-1	14.64	14.64
Fixation R	1	1	10128423445	10128861033	437588	778.86	723.00	74	17	-1	14.24	14.24
