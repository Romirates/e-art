[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:34:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9040074534	# Message: void.jpg
Saccade L	1	1	9040083109	9040162603	79494	780.85	1023.62	782.29	1040.01	17.42	1294.05	1.00	219.10	32311.33	-332.43	5502.80
Saccade R	1	1	9040083109	9040162603	79494	780.85	1023.62	782.29	1040.01	17.42	1294.05	1.00	219.10	32311.33	-332.43	5502.80
Blink L	1	1	9040162603	9040321706	159103
Blink R	1	1	9040162603	9040321706	159103
Fixation L	1	1	9040321706	9040639938	318232	1069.71	588.48	17	38	-1	16.00	16.00
Fixation R	1	1	9040321706	9040639938	318232	1069.71	588.48	17	38	-1	16.24	16.24
Blink L	1	2	9040639938	9040819066	179128
Blink R	1	2	9040639938	9040819066	179128
Fixation L	1	2	9040819066	9040918428	99362	945.60	561.46	10	27	-1	16.09	16.09
Fixation R	1	2	9040819066	9040918428	99362	945.60	561.46	10	27	-1	16.19	16.19
Blink L	1	3	9040918428	9041137296	218868
Blink R	1	3	9040918428	9041137296	218868
Fixation L	1	3	9041137296	9042072106	934810	998.33	596.70	15	29	-1	16.85	16.85
Fixation R	1	3	9041137296	9042072106	934810	998.33	596.70	15	29	-1	16.46	16.46
