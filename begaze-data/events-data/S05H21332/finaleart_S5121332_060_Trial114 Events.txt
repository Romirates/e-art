[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9505003194	# Message: void.jpg
Fixation L	1	1	9505006045	9505185030	178985	1041.98	460.31	3	10	-1	16.20	16.20
Fixation R	1	1	9505006045	9505185030	178985	1041.98	460.31	3	10	-1	16.21	16.21
Blink L	1	1	9505185030	9505403890	218860
Blink R	1	1	9505185030	9505403890	218860
Fixation L	1	2	9505403890	9505682252	278362	988.80	441.59	6	20	-1	16.26	16.26
Fixation R	1	2	9505403890	9505682252	278362	988.80	441.59	6	20	-1	15.83	15.83
Blink L	1	2	9505682252	9505861263	179011
Blink R	1	2	9505682252	9505861263	179011
Fixation L	1	3	9505861263	9506597207	735944	1047.64	444.81	24	60	-1	15.45	15.45
Fixation R	1	3	9505861263	9506597207	735944	1047.64	444.81	24	60	-1	15.92	15.92
Saccade L	1	1	9506597207	9506617065	19858	1033.94	468.81	1001.71	486.47	0.43	37.18	1.00	21.76	896.25	-589.49	636.60
Saccade R	1	1	9506597207	9506617065	19858	1033.94	468.81	1001.71	486.47	0.43	37.18	1.00	21.76	896.25	-589.49	636.60
Fixation L	1	4	9506617065	9506995053	377988	989.21	501.60	20	36	-1	16.60	16.60
Fixation R	1	4	9506617065	9506995053	377988	989.21	501.60	20	36	-1	16.71	16.71
