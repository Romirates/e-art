[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10409016034	# Message: void.jpg
Blink L	1	1	10409033520	10409113006	79486
Blink R	1	1	10409033520	10409113006	79486
Fixation L	1	1	10409113006	10409272120	159114	979.32	455.35	7	9	-1	14.09	14.09
Fixation R	1	1	10409113006	10409272120	159114	979.32	455.35	7	9	-1	14.00	14.00
Blink L	1	2	10409272120	10409431227	159107
Blink R	1	2	10409272120	10409431227	159107
Fixation L	1	2	10409431227	10409868839	437612	998.17	506.13	11	40	-1	14.51	14.51
Fixation R	1	2	10409431227	10409868839	437612	998.17	506.13	11	40	-1	14.33	14.33
Saccade L	1	1	10409868839	10409888716	19877	991.55	519.65	960.39	548.24	0.38	42.78	1.00	19.25	1008.46	-752.67	693.80
Saccade R	1	1	10409868839	10409888716	19877	991.55	519.65	960.39	548.24	0.38	42.78	1.00	19.25	1008.46	-752.67	693.80
Fixation L	1	3	10409888716	10411002637	1113921	930.89	550.50	36	41	-1	14.27	14.27
Fixation R	1	3	10409888716	10411002637	1113921	930.89	550.50	36	41	-1	13.88	13.88
