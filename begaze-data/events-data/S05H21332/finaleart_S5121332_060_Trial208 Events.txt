[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9917366679	# Message: void.jpg
Fixation L	1	1	9917381796	9917879004	497208	1239.37	424.92	8	15	-1	14.60	14.60
Fixation R	1	1	9917381796	9917879004	497208	1239.37	424.92	8	15	-1	14.05	14.05
Blink L	1	1	9917879004	9918077995	198991
Blink R	1	1	9917879004	9918077995	198991
Blink L	1	2	9918077995	9918535471	457476
Blink R	1	2	9918077995	9918535471	457476
Fixation L	1	2	9918535471	9918793949	258478	1124.86	383.55	23	15	-1	15.59	15.59
Fixation R	1	2	9918535471	9918793949	258478	1124.86	383.55	23	15	-1	15.21	15.21
Saccade L	1	1	9918793949	9918813816	19867	1105.23	392.16	1087.13	446.57	0.50	58.01	1.00	25.09	1397.37	-1158.25	932.12
Saccade R	1	1	9918793949	9918813816	19867	1105.23	392.16	1087.13	446.57	0.50	58.01	1.00	25.09	1397.37	-1158.25	932.12
Fixation L	1	3	9918813816	9919132053	318237	1061.48	454.10	30	13	-1	15.33	15.33
Fixation R	1	3	9918813816	9919132053	318237	1061.48	454.10	30	13	-1	15.03	15.03
Blink L	1	3	9919132053	9919291289	159236
Blink R	1	3	9919132053	9919291289	159236
