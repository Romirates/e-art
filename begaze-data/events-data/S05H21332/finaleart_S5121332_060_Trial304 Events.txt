[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10476930386	# Message: void.jpg
Blink L	1	1	10476946917	10477066287	119370
Blink R	1	1	10476946917	10477066287	119370
Fixation L	1	1	10477066287	10477364630	298343	773.94	436.31	12	40	-1	12.44	12.44
Fixation R	1	1	10477066287	10477364630	298343	773.94	436.31	12	40	-1	12.53	12.53
Blink L	1	2	10477364630	10477543617	178987
Blink R	1	2	10477364630	10477543617	178987
Fixation L	1	2	10477543617	10477841975	298358	1038.53	456.31	36	56	-1	13.37	13.37
Fixation R	1	2	10477543617	10477841975	298358	1038.53	456.31	36	56	-1	13.68	13.68
Saccade L	1	1	10477841975	10477861881	19906	1067.05	488.39	1076.90	486.87	0.19	10.61	1.00	9.54	46.85	-105.18	55.16
Saccade R	1	1	10477841975	10477861881	19906	1067.05	488.39	1076.90	486.87	0.19	10.61	1.00	9.54	46.85	-105.18	55.16
Fixation L	1	3	10477861881	10478836422	974541	1096.63	509.44	28	47	-1	14.44	14.44
Fixation R	1	3	10477861881	10478836422	974541	1096.63	509.44	28	47	-1	14.43	14.43
Saccade L	1	2	10478836422	10478916037	79615	1085.73	534.40	1025.04	548.91	1.07	45.57	0.25	13.41	1113.68	-1020.26	480.19
Saccade R	1	2	10478836422	10478916037	79615	1085.73	534.40	1025.04	548.91	1.07	45.57	0.25	13.41	1113.68	-1020.26	480.19
