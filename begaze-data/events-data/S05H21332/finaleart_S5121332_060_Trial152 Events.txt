[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:14
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9622076856	# Message: void.jpg
Fixation L	1	1	9622092485	9622689193	596708	1022.65	660.93	6	28	-1	14.03	14.03
Fixation R	1	1	9622092485	9622689193	596708	1022.65	660.93	6	28	-1	14.35	14.35
Blink L	1	1	9622689193	9622888065	198872
Blink R	1	1	9622689193	9622888065	198872
Saccade L	1	1	9622888065	9623027304	139239	938.52	510.93	968.31	534.09	21.46	1078.30	1.00	154.15	3671.81	-26234.13	4894.39
Saccade R	1	1	9622888065	9623027304	139239	938.52	510.93	968.31	534.09	21.46	1078.30	1.00	154.15	3671.81	-26234.13	4894.39
Blink L	1	2	9623027304	9623325675	298371
Blink R	1	2	9623027304	9623325675	298371
Fixation L	1	2	9623325675	9624061632	735957	948.77	542.30	19	55	-1	15.25	15.25
Fixation R	1	2	9623325675	9624061632	735957	948.77	542.30	19	55	-1	15.03	15.03
