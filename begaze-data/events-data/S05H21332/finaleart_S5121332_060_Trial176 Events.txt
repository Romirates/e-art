[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9819001747	# Message: void.jpg
Fixation L	1	1	9819003612	9819640184	636572	1103.97	664.96	6	15	-1	14.92	14.92
Fixation R	1	1	9819003612	9819640184	636572	1103.97	664.96	6	15	-1	15.18	15.18
Saccade L	1	1	9819640184	9819660059	19875	1101.31	659.67	966.18	660.05	0.92	136.67	1.00	46.48	3404.81	-3115.18	2491.12
Saccade R	1	1	9819640184	9819660059	19875	1101.31	659.67	966.18	660.05	0.92	136.67	1.00	46.48	3404.81	-3115.18	2491.12
Fixation L	1	2	9819660059	9819898670	238611	916.78	665.47	57	28	-1	15.21	15.21
Fixation R	1	2	9819660059	9819898670	238611	916.78	665.47	57	28	-1	15.65	15.65
Blink L	1	1	9819898670	9820077651	178981
Blink R	1	1	9819898670	9820077651	178981
Fixation L	1	3	9820077651	9820992596	914945	887.51	548.82	13	18	-1	16.67	16.67
Fixation R	1	3	9820077651	9820992596	914945	887.51	548.82	13	18	-1	15.20	15.20
