[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9782295044	# Message: void.jpg
Saccade L	1	1	9782310948	9782350818	39870	837.57	542.70	838.38	554.04	0.73	49.79	1.00	18.22	25377.82	0.00	6718.35
Saccade R	1	1	9782310948	9782350818	39870	837.57	542.70	838.38	554.04	0.73	49.79	1.00	18.22	25377.82	0.00	6718.35
Blink L	1	1	9782350818	9782569543	218725
Blink R	1	1	9782350818	9782569543	218725
Fixation L	1	1	9782569543	9783365126	795583	915.99	446.99	12	34	-1	14.82	14.82
Fixation R	1	1	9782569543	9783365126	795583	915.99	446.99	12	34	-1	14.46	14.46
Saccade L	1	2	9783365126	9783384988	19862	914.95	458.30	872.83	483.35	0.47	49.58	1.00	23.72	1190.69	-1195.84	1044.56
Saccade R	1	2	9783365126	9783384988	19862	914.95	458.30	872.83	483.35	0.47	49.58	1.00	23.72	1190.69	-1195.84	1044.56
Fixation L	1	2	9783384988	9783762967	377979	847.14	523.33	31	54	-1	14.71	14.71
Fixation R	1	2	9783384988	9783762967	377979	847.14	523.33	31	54	-1	14.40	14.40
Blink L	1	2	9783762967	9783941961	178994
Blink R	1	2	9783762967	9783941961	178994
Fixation L	1	3	9783941961	9784299949	357988	900.32	457.60	21	48	-1	14.94	14.94
Fixation R	1	3	9783941961	9784299949	357988	900.32	457.60	21	48	-1	14.60	14.60
