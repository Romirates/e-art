[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:13
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9597343049	# Message: void.jpg
Fixation L	1	1	9597354351	9597672578	318227	1025.28	521.67	6	8	-1	15.87	15.87
Fixation R	1	1	9597354351	9597672578	318227	1025.28	521.67	6	8	-1	15.38	15.38
Blink L	1	1	9597672578	9597831691	159113
Blink R	1	1	9597672578	9597831691	159113
Fixation L	1	2	9597831691	9597931062	99371	1024.75	497.22	8	50	-1	15.88	15.88
Fixation R	1	2	9597831691	9597931062	99371	1024.75	497.22	8	50	-1	15.87	15.87
Blink L	1	2	9597931062	9598209550	278488
Blink R	1	2	9597931062	9598209550	278488
Fixation L	1	3	9598209550	9599343225	1133675	1058.73	483.04	14	34	-1	16.63	16.63
Fixation R	1	3	9598209550	9599343225	1133675	1058.73	483.04	14	34	-1	15.95	15.95
