[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:11
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9548022192	# Message: void.jpg
Fixation L	1	1	9548027704	9548166992	139288	876.39	528.69	7	16	-1	16.40	16.40
Fixation R	1	1	9548027704	9548166992	139288	876.39	528.69	7	16	-1	16.15	16.15
Saccade L	1	1	9548166992	9548186809	19817	870.88	523.01	784.90	487.65	0.61	94.05	1.00	30.78	2296.36	-2236.79	1629.41
Saccade R	1	1	9548166992	9548186809	19817	870.88	523.01	784.90	487.65	0.61	94.05	1.00	30.78	2296.36	-2236.79	1629.41
Fixation L	1	2	9548186809	9548544785	357976	766.98	457.70	24	40	-1	15.50	15.50
Fixation R	1	2	9548186809	9548544785	357976	766.98	457.70	24	40	-1	15.34	15.34
Blink L	1	1	9548544785	9548723808	179023
Blink R	1	1	9548544785	9548723808	179023
Fixation L	1	3	9548723808	9548843149	119341	910.81	437.08	3	66	-1	15.06	15.06
Fixation R	1	3	9548723808	9548843149	119341	910.81	437.08	3	66	-1	15.26	15.26
Blink L	1	2	9548843149	9548982396	139247
Blink R	1	2	9548843149	9548982396	139247
Fixation L	1	4	9548982396	9550016584	1034188	1018.00	481.65	20	62	-1	16.37	16.37
Fixation R	1	4	9548982396	9550016584	1034188	1018.00	481.65	20	62	-1	16.40	16.40
