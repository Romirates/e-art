[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9794516756	# Message: void.jpg
Fixation L	1	1	9794523335	9794742059	218724	1035.09	613.46	74	9	-1	12.96	12.96
Fixation R	1	1	9794523335	9794742059	218724	1035.09	613.46	74	9	-1	12.80	12.80
Saccade L	1	1	9794742059	9794761930	19871	969.31	613.41	949.72	613.34	0.51	57.80	0.00	25.60	60.63	-1260.32	596.81
Saccade R	1	1	9794742059	9794761930	19871	969.31	613.41	949.72	613.34	0.51	57.80	0.00	25.60	60.63	-1260.32	596.81
Fixation L	1	2	9794761930	9794960928	198998	952.05	603.21	7	17	-1	12.92	12.92
Fixation R	1	2	9794761930	9794960928	198998	952.05	603.21	7	17	-1	13.19	13.19
Blink L	1	1	9794960928	9796067985	1107057
Blink R	1	1	9794960928	9796028235	1067307
Fixation R	1	3	9796048110	9796505575	457465	1005.63	517.56	34	34	-1	13.40	13.40
Fixation L	1	3	9796067985	9796505575	437590	1004.86	516.87	34	32	-1	14.03	14.03
