[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9911241686	# Message: void.jpg
Fixation L	1	1	9911255785	9911454643	198858	1255.53	1046.96	21	22	-1	19.08	19.08
Fixation R	1	1	9911255785	9911454643	198858	1255.53	1046.96	21	22	-1	18.71	18.71
Saccade L	1	1	9911454643	9911474518	19875	1240.58	1039.31	1126.51	957.85	1.10	141.78	1.00	55.53	3406.12	-3459.00	2393.53
Saccade R	1	1	9911454643	9911474518	19875	1240.58	1039.31	1126.51	957.85	1.10	141.78	1.00	55.53	3406.12	-3459.00	2393.53
Fixation L	1	2	9911474518	9911713259	238741	1092.81	926.38	45	39	-1	18.07	18.07
Fixation R	1	2	9911474518	9911713259	238741	1092.81	926.38	45	39	-1	18.00	18.00
Blink L	1	1	9911713259	9912110996	397737
Blink R	1	1	9911713259	9912110996	397737
Fixation L	1	3	9912110996	9913224922	1113926	960.45	510.62	18	53	-1	15.28	15.28
Fixation R	1	3	9912110996	9913224922	1113926	960.45	510.62	18	53	-1	14.87	14.87
