[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:34:54
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8978640851	# Message: void.jpg
Blink L	1	1	8978643566	8978782799	139233
Blink R	1	1	8978643566	8978782799	139233
Fixation L	1	1	8978782799	8979120903	338104	678.98	707.12	9	33	-1	17.55	17.55
Fixation R	1	1	8978782799	8979120903	338104	678.98	707.12	9	33	-1	18.20	18.20
Saccade L	1	1	8979120903	8979160648	39745	678.26	710.04	982.98	636.90	2.95	165.67	0.50	74.19	4092.82	-3638.92	3642.96
Saccade R	1	1	8979120903	8979160648	39745	678.26	710.04	982.98	636.90	2.95	165.67	0.50	74.19	4092.82	-3638.92	3642.96
Fixation L	1	2	8979160648	8979976230	815582	1017.69	598.21	49	46	-1	16.65	16.65
Fixation R	1	2	8979160648	8979976230	815582	1017.69	598.21	49	46	-1	16.93	16.93
Saccade L	1	2	8979976230	8979996101	19871	977.62	605.19	969.14	607.67	0.13	11.68	1.00	6.37	-56.33	-110.08	78.39
Saccade R	1	2	8979976230	8979996101	19871	977.62	605.19	969.14	607.67	0.13	11.68	1.00	6.37	-56.33	-110.08	78.39
Fixation L	1	3	8979996101	8980632556	636455	965.05	598.87	10	24	-1	16.95	16.95
Fixation R	1	3	8979996101	8980632556	636455	965.05	598.87	10	24	-1	17.20	17.20
