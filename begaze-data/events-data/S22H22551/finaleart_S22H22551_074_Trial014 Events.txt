[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:56:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4711388028	# Message: void.jpg
Blink L	1	1	4711403433	4711562539	159106
Blink R	1	1	4711403433	4711562539	159106
Fixation L	1	1	4711562539	4711841021	278482	1026.96	666.01	12	70	-1	19.21	19.21
Fixation R	1	1	4711562539	4711841021	278482	1026.96	666.01	12	70	-1	19.03	19.03
Saccade L	1	1	4711841021	4711860898	19877	1034.92	607.59	1089.92	487.02	1.04	134.04	1.00	52.38	3325.14	-3333.10	2765.12
Saccade R	1	1	4711841021	4711860898	19877	1034.92	607.59	1089.92	487.02	1.04	134.04	1.00	52.38	3325.14	-3333.10	2765.12
Fixation L	1	2	4711860898	4713312948	1452050	1084.38	500.17	59	39	-1	19.56	19.56
Fixation R	1	2	4711860898	4713312948	1452050	1084.38	500.17	59	39	-1	19.39	19.39
Saccade L	1	2	4713312948	4713412338	99390	1040.49	510.77	1016.74	506.88	0.47	16.84	0.80	4.76	374.25	-47.80	108.42
Saccade R	1	2	4713312948	4713412338	99390	1040.49	510.77	1016.74	506.88	0.47	16.84	0.80	4.76	374.25	-47.80	108.42
