[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:58:39
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5656658985	# Message: void.jpg
Fixation L	1	1	5656675599	5656775089	99490	1713.13	713.40	12	10	-1	17.70	17.70
Fixation R	1	1	5656675599	5656775089	99490	1713.13	713.40	12	10	-1	17.50	17.50
Blink L	1	1	5656775089	5657033694	258605
Blink R	1	1	5656775089	5657033694	258605
Saccade L	1	1	5657033694	5657113190	79496	382.34	604.41	380.79	603.10	1.94	139.76	1.00	24.45	20973.51	-439.52	4178.40
Saccade R	1	1	5657033694	5657113190	79496	382.34	604.41	380.79	603.10	1.94	139.76	1.00	24.45	20973.51	-439.52	4178.40
Blink L	1	2	5657113190	5657331929	218739
Blink R	1	2	5657113190	5657331929	218739
Fixation L	1	2	5657331929	5658445880	1113951	1139.30	258.14	48	49	-1	16.14	16.14
Fixation R	1	2	5657331929	5658445880	1113951	1139.30	258.14	48	49	-1	16.53	16.53
Saccade L	1	2	5658445880	5658465757	19877	1116.50	285.47	1106.12	300.61	0.29	20.41	1.00	14.50	421.75	-207.30	235.48
Saccade R	1	2	5658445880	5658465757	19877	1116.50	285.47	1106.12	300.61	0.29	20.41	1.00	14.50	421.75	-207.30	235.48
Fixation L	1	3	5658465757	5658664626	198869	1078.23	335.03	40	50	-1	16.55	16.55
Fixation R	1	3	5658465757	5658664626	198869	1078.23	335.03	40	50	-1	16.82	16.82
