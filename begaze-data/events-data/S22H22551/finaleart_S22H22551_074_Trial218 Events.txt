[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:58:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5552049357	# Message: void.jpg
Saccade L	1	1	5552054543	5552114178	59635	1435.62	1029.32	1583.99	968.79	2.35	95.40	1.00	39.38	2324.72	-1976.97	1514.59
Saccade R	1	1	5552054543	5552114178	59635	1435.62	1029.32	1583.99	968.79	2.35	95.40	1.00	39.38	2324.72	-1976.97	1514.59
Fixation L	1	1	5552114178	5552233532	119354	1579.19	974.55	9	21	-1	14.92	14.92
Fixation R	1	1	5552114178	5552233532	119354	1579.19	974.55	9	21	-1	15.95	15.95
Saccade L	1	2	5552233532	5552273285	39753	1577.21	972.63	1518.82	590.26	3.34	209.51	1.00	83.99	5138.93	-5113.24	4719.45
Saccade R	1	2	5552233532	5552273285	39753	1577.21	972.63	1518.82	590.26	3.34	209.51	1.00	83.99	5138.93	-5113.24	4719.45
Fixation L	1	2	5552273285	5552492139	218854	1509.75	568.42	23	31	-1	14.82	14.82
Fixation R	1	2	5552273285	5552492139	218854	1509.75	568.42	23	31	-1	15.42	15.42
Blink L	1	1	5552492139	5552651259	159120
Blink R	1	1	5552492139	5552651259	159120
Fixation L	1	3	5552651259	5552949625	298366	1082.49	373.32	13	82	-1	15.83	15.83
Fixation R	1	3	5552651259	5552949625	298366	1082.49	373.32	13	82	-1	16.54	16.54
Saccade L	1	3	5552949625	5552969490	19865	1075.11	393.40	1073.88	397.70	0.05	4.53	1.00	2.57	61.42	-73.51	59.83
Saccade R	1	3	5552949625	5552969490	19865	1075.11	393.40	1073.88	397.70	0.05	4.53	1.00	2.57	61.42	-73.51	59.83
Fixation L	1	4	5552969490	5554043560	1074070	1092.56	403.57	41	24	-1	16.92	16.92
Fixation R	1	4	5552969490	5554043560	1074070	1092.56	403.57	41	24	-1	17.56	17.56
