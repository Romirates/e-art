[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:58:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5369307599	# Message: void.jpg
Fixation L	1	1	5369311190	5369530031	218841	621.86	328.56	12	36	-1	16.95	16.95
Fixation R	1	1	5369311190	5369530031	218841	621.86	328.56	12	36	-1	16.91	16.91
Saccade L	1	1	5369530031	5369569788	39757	632.01	343.00	1155.00	348.76	4.53	352.98	0.50	113.99	8539.09	-8338.61	6277.66
Saccade R	1	1	5369530031	5369569788	39757	632.01	343.00	1155.00	348.76	4.53	352.98	0.50	113.99	8539.09	-8338.61	6277.66
Fixation L	1	2	5369569788	5369987410	417622	1140.95	354.48	19	36	-1	16.72	16.72
Fixation R	1	2	5369569788	5369987410	417622	1140.95	354.48	19	36	-1	16.63	16.63
Blink L	1	1	5369987410	5370146623	159213
Blink R	1	1	5369987410	5370146623	159213
Fixation L	1	3	5370146623	5371121207	974584	1117.16	364.62	79	19	-1	17.81	17.81
Fixation R	1	3	5370146623	5371121207	974584	1117.16	364.62	79	19	-1	17.43	17.43
Saccade L	1	2	5371121207	5371141098	19891	1157.59	361.98	1159.13	361.21	0.07	5.66	0.00	3.34	-17.25	-104.61	63.42
Saccade R	1	2	5371121207	5371141098	19891	1157.59	361.98	1159.13	361.21	0.07	5.66	0.00	3.34	-17.25	-104.61	63.42
Fixation L	1	4	5371141098	5371300193	159095	1159.88	359.65	2	3	-1	17.76	17.76
Fixation R	1	4	5371141098	5371300193	159095	1159.88	359.65	2	3	-1	17.52	17.52
