[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:59:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5843413447	# Message: void.jpg
Fixation L	1	1	5843422263	5843859852	437589	1162.29	464.92	24	10	-1	15.98	15.98
Fixation R	1	1	5843422263	5843859852	437589	1162.29	464.92	24	10	-1	15.71	15.71
Blink L	1	1	5843859852	5844018969	159117
Blink R	1	1	5843859852	5844018969	159117
Fixation L	1	2	5844018969	5844874178	855209	1040.09	420.67	28	69	-1	16.61	16.61
Fixation R	1	2	5844018969	5844874178	855209	1040.09	420.67	28	69	-1	16.52	16.52
Saccade L	1	1	5844874178	5844894051	19873	1057.05	415.42	1060.77	415.08	0.05	3.78	1.00	2.65	26.59	-74.34	37.38
Saccade R	1	1	5844874178	5844894051	19873	1057.05	415.42	1060.77	415.08	0.05	3.78	1.00	2.65	26.59	-74.34	37.38
Fixation L	1	3	5844894051	5845411281	517230	1065.17	426.17	9	19	-1	16.65	16.65
Fixation R	1	3	5844894051	5845411281	517230	1065.17	426.17	9	19	-1	16.48	16.48
