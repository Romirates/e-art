[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:57:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4984257830	# Message: void.jpg
Fixation L	1	1	4984275843	4984852679	576836	1184.18	218.28	26	54	-1	15.60	15.60
Fixation R	1	1	4984275843	4984852679	576836	1184.18	218.28	26	54	-1	15.40	15.40
Blink L	1	1	4984852679	4985031670	178991
Blink R	1	1	4984852679	4985031670	178991
Fixation L	1	2	4985031670	4986006246	974576	1119.56	306.75	68	31	-1	17.61	17.61
Fixation R	1	2	4985031670	4986006246	974576	1119.56	306.75	68	31	-1	17.73	17.73
Saccade L	1	1	4986006246	4986026125	19879	1079.26	320.88	1078.62	320.65	0.03	2.16	1.00	1.34	30.51	-22.86	23.42
Saccade R	1	1	4986006246	4986026125	19879	1079.26	320.88	1078.62	320.65	0.03	2.16	1.00	1.34	30.51	-22.86	23.42
Fixation L	1	3	4986026125	4986244858	218733	1060.29	325.45	43	9	-1	17.95	17.95
Fixation R	1	3	4986026125	4986244858	218733	1060.29	325.45	43	9	-1	18.32	18.32
