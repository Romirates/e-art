[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:56:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4686983232	# Message: void.jpg
Fixation L	1	1	4686998449	4687595165	596716	964.15	526.86	10	33	-1	23.02	23.02
Fixation R	1	1	4686998449	4687595165	596716	964.15	526.86	10	33	-1	22.14	22.14
Blink L	1	1	4687595165	4687794045	198880
Blink R	1	1	4687595165	4687794045	198880
Fixation L	1	2	4687794045	4688470368	676323	998.18	492.49	53	46	-1	18.92	18.92
Fixation R	1	2	4687794045	4688470368	676323	998.18	492.49	53	46	-1	19.12	19.12
Saccade L	1	1	4688470368	4688490245	19877	966.80	512.73	965.01	511.55	0.05	6.30	1.00	2.70	203.74	20.73	118.85
Saccade R	1	1	4688470368	4688490245	19877	966.80	512.73	965.01	511.55	0.05	6.30	1.00	2.70	203.74	20.73	118.85
Fixation L	1	3	4688490245	4688987476	497231	999.53	511.59	44	11	-1	19.75	19.75
Fixation R	1	3	4688490245	4688987476	497231	999.53	511.59	44	11	-1	19.82	19.82
