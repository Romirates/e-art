[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:56:46
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4705299359	# Message: void.jpg
Fixation L	1	1	4705317124	4705456364	139240	1088.53	471.85	93	5	-1	16.76	16.76
Fixation R	1	1	4705317124	4705456364	139240	1088.53	471.85	93	5	-1	16.94	16.94
Saccade L	1	1	4705456364	4705476236	19872	1017.46	469.57	1016.54	465.53	0.50	82.28	1.00	25.14	-41.16	-1952.06	721.71
Saccade R	1	1	4705456364	4705476236	19872	1017.46	469.57	1016.54	465.53	0.50	82.28	1.00	25.14	-41.16	-1952.06	721.71
Fixation L	1	2	4705476236	4706371311	895075	1026.93	455.43	49	47	-1	18.05	18.05
Fixation R	1	2	4705476236	4706371311	895075	1026.93	455.43	49	47	-1	18.12	18.12
Saccade L	1	2	4706371311	4706391193	19882	1052.09	470.66	1056.38	472.98	0.05	4.94	1.00	2.53	81.04	-58.88	64.47
Saccade R	1	2	4706371311	4706391193	19882	1052.09	470.66	1056.38	472.98	0.05	4.94	1.00	2.53	81.04	-58.88	64.47
Fixation L	1	3	4706391193	4706828791	437598	1057.14	473.57	25	8	-1	19.60	19.60
Fixation R	1	3	4706391193	4706828791	437598	1057.14	473.57	25	8	-1	19.51	19.51
Blink L	1	1	4706828791	4707007781	178990
Blink R	1	1	4706828791	4707007781	178990
Fixation L	1	4	4707007781	4707286261	278480	1049.60	508.17	26	22	-1	22.04	22.04
Fixation R	1	4	4707007781	4707286261	278480	1049.60	508.17	26	22	-1	21.66	21.66
