[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:54:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	1687638109	# Message: void.jpg
Fixation L	1	1	1687648737	1687768101	119364	1162.91	597.93	16	37	-1	17.18	17.18
Fixation R	1	1	1687648737	1687768101	119364	1162.91	597.93	16	37	-1	15.53	15.53
Saccade L	1	1	1687768101	1687788161	20060	1149.72	572.83	1145.30	495.90	0.77	77.95	1.00	38.61	1684.58	-1585.61	1105.78
Saccade R	1	1	1687768101	1687788161	20060	1149.72	572.83	1145.30	495.90	0.77	77.95	1.00	38.61	1684.58	-1585.61	1105.78
Fixation L	1	2	1687788161	1688165831	377670	1116.89	505.88	36	38	-1	17.10	17.10
Fixation R	1	2	1687788161	1688165831	377670	1116.89	505.88	36	38	-1	15.23	15.23
Blink L	1	1	1688165831	1688464206	298375
Blink R	1	1	1688165831	1688464206	298375
Fixation L	1	3	1688464206	1689021164	556958	1025.65	473.90	13	37	-1	18.48	18.48
Fixation R	1	3	1688464206	1689021164	556958	1025.65	473.90	13	37	-1	16.96	16.96
Blink L	1	2	1689021164	1689200156	178992
Blink R	1	2	1689021164	1689200156	178992
Fixation L	1	4	1689200156	1689637757	437601	1001.51	473.18	19	24	-1	18.69	18.69
Fixation R	1	4	1689200156	1689637757	437601	1001.51	473.18	19	24	-1	17.27	17.27
