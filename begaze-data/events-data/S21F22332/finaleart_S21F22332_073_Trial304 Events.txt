[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3248424893	# Message: void.jpg
Fixation L	1	1	3248429143	3248588384	159241	1002.10	-3.56	5	13	-1	14.97	14.97
Fixation R	1	1	3248429143	3248588384	159241	1002.10	-3.56	5	13	-1	13.88	13.88
Saccade L	1	1	3248588384	3248647999	59615	999.52	-11.86	992.15	664.49	7.01	319.97	0.33	117.67	7861.26	-5696.98	5440.75
Saccade R	1	1	3248588384	3248647999	59615	999.52	-11.86	992.15	664.49	7.01	319.97	0.33	117.67	7861.26	-5696.98	5440.75
Fixation L	1	2	3248647999	3249005975	357976	986.96	658.07	10	20	-1	15.31	15.31
Fixation R	1	2	3248647999	3249005975	357976	986.96	658.07	10	20	-1	13.96	13.96
Blink L	1	1	3249005975	3249264607	258632
Blink R	1	1	3249005975	3249284469	278494
Fixation L	1	3	3249264607	3250418151	1153544	948.09	504.30	55	33	-1	13.19	13.19
Fixation R	1	3	3249284469	3250418151	1133682	948.50	504.30	55	33	-1	11.94	11.94
