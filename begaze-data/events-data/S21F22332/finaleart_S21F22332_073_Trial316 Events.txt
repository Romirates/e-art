[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:41
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3285045069	# Message: void.jpg
Fixation L	1	1	3285046617	3285165981	119364	1419.41	684.78	22	13	-1	15.27	15.27
Fixation R	1	1	3285046617	3285165981	119364	1419.41	684.78	22	13	-1	14.21	14.21
Saccade L	1	1	3285165981	3285185865	19884	1437.66	679.95	1557.68	678.65	0.73	121.40	1.00	36.78	2967.33	-2972.59	2140.08
Saccade R	1	1	3285165981	3285185865	19884	1437.66	679.95	1557.68	678.65	0.73	121.40	1.00	36.78	2967.33	-2972.59	2140.08
Fixation L	1	2	3285185865	3285484212	298347	1556.07	677.05	5	10	-1	15.34	15.34
Fixation R	1	2	3285185865	3285484212	298347	1556.07	677.05	5	10	-1	14.40	14.40
Blink L	1	1	3285484212	3285762573	278361
Blink R	1	1	3285484212	3285762573	278361
Fixation L	1	3	3285762573	3286717400	954827	872.59	508.10	27	38	-1	12.99	12.99
Fixation R	1	3	3285762573	3286717400	954827	872.59	508.10	27	38	-1	12.12	12.12
Saccade L	1	2	3286717400	3286737284	19884	880.30	514.05	973.53	526.44	0.56	95.14	1.00	28.21	2231.46	-2234.03	1552.53
Saccade R	1	2	3286717400	3286737284	19884	880.30	514.05	973.53	526.44	0.56	95.14	1.00	28.21	2231.46	-2234.03	1552.53
Fixation L	1	4	3286737284	3286936133	198849	974.77	551.33	7	50	-1	13.91	13.91
Fixation R	1	4	3286737284	3286936133	198849	974.77	551.33	7	50	-1	13.07	13.07
