[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2583973781	# Message: void.jpg
Saccade L	1	1	2583976484	2584195352	218868	331.33	811.16	385.13	215.61	17.88	418.26	0.45	81.70	10336.19	-9264.79	2539.79
Saccade R	1	1	2583976484	2584195352	218868	331.33	811.16	385.13	215.61	17.88	418.26	0.45	81.70	10336.19	-9264.79	2539.79
Fixation L	1	1	2584195352	2584414089	218737	397.96	213.19	25	40	-1	11.97	11.97
Fixation R	1	1	2584195352	2584414089	218737	397.96	213.19	25	40	-1	11.56	11.56
Blink L	1	1	2584414089	2584493579	79490
Blink R	1	1	2584414089	2584493579	79490
Blink L	1	2	2584513583	2584811940	298357
Blink R	1	2	2584513583	2584632827	119244
Blink R	1	3	2584652827	2584811940	159113
Fixation L	1	2	2584811940	2585428427	616487	910.21	430.89	21	41	-1	13.80	13.80
Fixation R	1	2	2584811940	2585428427	616487	910.21	430.89	21	41	-1	12.46	12.46
Blink L	1	3	2585428427	2585627401	198974
Blink R	1	4	2585428427	2585627401	198974
Fixation L	1	3	2585627401	2585965507	338106	937.82	479.21	8	13	-1	12.81	12.81
Fixation R	1	3	2585627401	2585965507	338106	937.82	479.21	8	13	-1	12.13	12.13
