[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3217774097	# Message: void.jpg
Fixation L	1	1	3217778861	3218057215	278354	692.20	242.41	45	21	-1	11.41	11.41
Fixation R	1	1	3217778861	3218057215	278354	692.20	242.41	45	21	-1	11.87	11.87
Saccade L	1	1	3218057215	3218077227	20012	720.44	243.10	752.17	257.84	0.44	35.39	1.00	22.06	140.78	-702.96	320.19
Saccade R	1	1	3218057215	3218077227	20012	720.44	243.10	752.17	257.84	0.44	35.39	1.00	22.06	140.78	-702.96	320.19
Fixation L	1	2	3218077227	3218236337	159110	757.54	242.60	24	33	-1	11.98	11.98
Fixation R	1	2	3218077227	3218236337	159110	757.54	242.60	24	33	-1	12.26	12.26
Blink L	1	1	3218236337	3218653936	417599
Blink R	1	1	3218236337	3218653936	417599
Fixation L	1	3	3218653936	3218932424	278488	1022.74	582.87	20	55	-1	16.52	16.52
Fixation R	1	3	3218653936	3218932424	278488	1022.74	582.87	20	55	-1	12.66	12.66
Blink L	1	2	3218932424	3219111410	178986
Blink R	1	2	3218932424	3219111410	178986
Fixation L	1	4	3219111410	3219767765	656355	1044.57	518.36	9	56	-1	13.16	13.16
Fixation R	1	4	3219111410	3219767765	656355	1044.57	518.36	9	56	-1	12.21	12.21
