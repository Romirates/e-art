[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3260630437	# Message: void.jpg
Fixation L	1	1	3260641633	3260860376	218743	1261.04	63.33	17	22	-1	11.63	11.63
Fixation R	1	1	3260641633	3260860376	218743	1261.04	63.33	17	22	-1	11.02	11.02
Saccade L	1	1	3260860376	3260900242	39866	1256.60	53.43	1271.28	515.37	3.93	250.00	1.00	98.66	6110.89	-6054.35	5636.59
Saccade R	1	1	3260860376	3260900242	39866	1256.60	53.43	1271.28	515.37	3.93	250.00	1.00	98.66	6110.89	-6054.35	5636.59
Fixation L	1	2	3260900242	3261118975	218733	1283.29	530.60	20	34	-1	12.63	12.63
Fixation R	1	2	3260900242	3261118975	218733	1283.29	530.60	20	34	-1	11.66	11.66
Blink L	1	1	3261118975	3261457084	338109
Blink R	1	1	3261118975	3261457084	338109
Blink R	1	2	3261457084	3261596326	139242
Fixation L	1	3	3261556575	3261874828	318253	941.75	645.97	33	66	-1	13.86	13.86
Fixation R	1	3	3261596326	3261914565	318239	937.80	643.69	27	27	-1	12.98	12.98
Saccade L	1	2	3261874828	3261914565	39737	928.42	651.05	927.20	643.84	2.68	325.54	1.00	67.54	33028.35	16.45	10321.40
Blink L	1	2	3261914565	3262133445	218880
Blink R	1	3	3261914565	3262133445	218880
Fixation L	1	4	3262133445	3262630649	497204	960.38	545.45	19	15	-1	13.10	13.10
Fixation R	1	4	3262133445	3262630649	497204	960.38	545.45	19	15	-1	12.39	12.39
