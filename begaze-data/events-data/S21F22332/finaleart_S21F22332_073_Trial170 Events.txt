[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2729428905	# Message: void.jpg
Fixation L	1	1	2729433413	2729592547	159134	867.65	872.83	30	26	-1	13.95	13.95
Fixation R	1	1	2729433413	2729592547	159134	867.65	872.83	30	26	-1	12.81	12.81
Saccade L	1	1	2729592547	2729612395	19848	880.77	880.71	1112.55	1005.66	1.49	266.19	1.00	75.05	6479.13	-6561.17	4462.09
Saccade R	1	1	2729592547	2729612395	19848	880.77	880.71	1112.55	1005.66	1.49	266.19	1.00	75.05	6479.13	-6561.17	4462.09
Fixation L	1	2	2729612395	2729851017	238622	1099.69	1002.28	27	19	-1	14.12	14.12
Fixation R	1	2	2729612395	2729851017	238622	1099.69	1002.28	27	19	-1	12.85	12.85
Blink L	1	1	2729851017	2730189119	338102
Blink R	1	1	2729851017	2730189119	338102
Fixation L	1	3	2730189119	2731422302	1233183	958.17	744.51	18	75	-1	12.99	12.99
Fixation R	1	3	2730189119	2731422302	1233183	958.17	744.51	18	75	-1	11.63	11.63
