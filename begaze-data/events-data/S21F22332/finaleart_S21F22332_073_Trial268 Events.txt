[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:19
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3138144797	# Message: void.jpg
Saccade L	1	1	3138159384	3138238891	79507	455.20	690.66	955.73	685.25	6.99	305.24	0.75	87.87	7422.22	-7361.26	4053.10
Saccade R	1	1	3138159384	3138238891	79507	455.20	690.66	955.73	685.25	6.99	305.24	0.75	87.87	7422.22	-7361.26	4053.10
Fixation L	1	1	3138238891	3138576982	338091	987.44	671.70	39	20	-1	14.75	14.75
Fixation R	1	1	3138238891	3138576982	338091	987.44	671.70	39	20	-1	14.96	14.96
Blink L	1	1	3138576982	3138915214	338232
Blink R	1	1	3138576982	3138915214	338232
Fixation L	1	2	3138915214	3139571552	656338	864.37	556.61	42	56	-1	13.79	13.79
Fixation R	1	2	3138915214	3139571552	656338	864.37	556.61	42	56	-1	13.17	13.17
Saccade L	1	2	3139571552	3139651047	79495	896.72	513.69	919.55	505.35	2.93	186.63	1.00	36.91	28635.88	-563.12	5650.75
Saccade R	1	2	3139571552	3139651047	79495	896.72	513.69	919.55	505.35	2.93	186.63	1.00	36.91	28635.88	-563.12	5650.75
Blink L	1	2	3139651047	3139869911	218864
Blink R	1	2	3139651047	3139869911	218864
Fixation L	1	3	3139869911	3140148290	278379	915.27	454.50	8	42	-1	13.84	13.84
Fixation R	1	3	3139869911	3140148290	278379	915.27	454.50	8	42	-1	13.27	13.27
