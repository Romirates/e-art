[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2938408304	# Message: void.jpg
Fixation L	1	1	2938419898	2938559308	139410	375.86	648.12	5	12	-1	12.69	12.69
Fixation R	1	1	2938419898	2938559308	139410	375.86	648.12	5	12	-1	10.35	10.35
Saccade L	1	1	2938559308	2938598893	39585	377.35	653.90	387.66	954.57	2.60	221.15	0.50	65.60	5499.90	-5000.89	3611.45
Saccade R	1	1	2938559308	2938598893	39585	377.35	653.90	387.66	954.57	2.60	221.15	0.50	65.60	5499.90	-5000.89	3611.45
Fixation L	1	2	2938598893	2938837629	238736	427.88	964.20	51	12	-1	13.31	13.31
Fixation R	1	2	2938598893	2938837629	238736	427.88	964.20	51	12	-1	11.10	11.10
Blink L	1	1	2938837629	2939096246	258617
Blink R	1	1	2938837629	2939096246	258617
Fixation L	1	3	2939096246	2939235360	139114	819.68	497.11	8	14	-1	12.78	12.78
Fixation R	1	3	2939096246	2939235360	139114	819.68	497.11	8	14	-1	11.96	11.96
Blink L	1	2	2939235360	2939474101	238741
Blink R	1	2	2939235360	2939474101	238741
Fixation L	1	4	2939474101	2940170185	696084	1029.35	537.54	16	36	-1	13.06	13.06
Fixation R	1	4	2939474101	2940170185	696084	1029.35	537.54	16	36	-1	12.57	12.57
