[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S18F56223-finaleart-1.idf
Date:	27.03.2019 15:48:01
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S18F56223
Description:	Pascale Sebillot

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2123572833	# Message: void.jpg
Saccade L	1	1	2123591775	2123731015	139240	880.38	571.25	1135.51	312.52	14.11	299.60	0.29	101.37	7490.06	-4361.53	2527.73
Saccade R	1	1	2123591775	2123731015	139240	880.38	571.25	1135.51	312.52	14.11	299.60	0.29	101.37	7490.06	-4361.53	2527.73
Fixation L	1	1	2123731015	2124049247	318232	1145.72	260.20	15	77	-1	13.00	13.00
Fixation R	1	1	2123731015	2124049247	318232	1145.72	260.20	15	77	-1	13.18	13.18
Saccade L	1	2	2124049247	2125560930	1511683	1142.40	276.08	962.25	709.02	293.50	491.61	0.91	194.16	11604.30	-11100.54	3674.56
Saccade R	1	2	2124049247	2125560930	1511683	1142.40	276.08	962.25	709.02	293.50	491.61	0.91	194.16	11604.30	-11100.54	3674.56
