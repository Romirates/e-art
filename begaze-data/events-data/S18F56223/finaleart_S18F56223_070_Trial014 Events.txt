[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S18F56223-finaleart-1.idf
Date:	27.03.2019 15:48:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S18F56223
Description:	Pascale Sebillot

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2141878990	# Message: void.jpg
Saccade L	1	1	2141890718	2142069704	178986	766.96	499.64	1020.13	255.34	15.08	361.01	0.11	84.24	2724.22	-8501.17	2000.42
Saccade R	1	1	2141890718	2142069704	178986	766.96	499.64	1020.13	255.34	15.08	361.01	0.11	84.24	2724.22	-8501.17	2000.42
Fixation L	1	1	2142069704	2142248703	178999	1021.19	269.63	8	31	-1	10.25	10.25
Fixation R	1	1	2142069704	2142248703	178999	1021.19	269.63	8	31	-1	10.86	10.86
Saccade L	1	2	2142248703	2143859879	1611176	1026.15	276.75	700.19	598.58	317.59	540.07	0.06	197.12	11307.53	-12990.70	4267.11
Saccade R	1	2	2142248703	2143859879	1611176	1026.15	276.75	700.19	598.58	317.59	540.07	0.06	197.12	11307.53	-12990.70	4267.11
