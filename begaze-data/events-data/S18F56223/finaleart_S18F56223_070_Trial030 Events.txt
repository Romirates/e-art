[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S18F56223-finaleart-1.idf
Date:	27.03.2019 15:48:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S18F56223
Description:	Pascale Sebillot

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2190769414	# Message: void.jpg
Fixation L	1	1	2190780596	2191178423	397827	310.64	1056.99	37	34	-1	12.41	12.41
Fixation R	1	1	2190780596	2191178423	397827	310.64	1056.99	37	34	-1	12.40	12.40
Saccade L	1	1	2191178423	2191277798	99375	343.70	1074.46	625.92	539.77	9.07	163.88	0.60	91.25	3709.11	-2215.21	2024.48
Saccade R	1	1	2191178423	2191277798	99375	343.70	1074.46	625.92	539.77	9.07	163.88	0.60	91.25	3709.11	-2215.21	2024.48
Fixation L	1	2	2191277798	2191695538	417740	653.24	549.91	46	44	-1	13.02	13.02
Fixation R	1	2	2191277798	2191695538	417740	653.24	549.91	46	44	-1	12.93	12.93
Saccade L	1	2	2191695538	2191934273	238735	668.33	561.15	699.96	584.50	43.74	443.50	0.17	183.21	10845.66	-8373.61	5212.39
Saccade R	1	2	2191695538	2191934273	238735	668.33	561.15	699.96	584.50	43.74	443.50	0.17	183.21	10845.66	-8373.61	5212.39
Fixation L	1	3	2191934273	2192033646	99373	701.84	571.47	6	22	-1	13.79	13.79
Fixation R	1	3	2191934273	2192033646	99373	701.84	571.47	6	22	-1	13.36	13.36
Saccade L	1	3	2192033646	2192769625	735979	700.26	561.57	718.38	637.53	141.08	563.46	0.35	191.69	9665.98	-13150.08	4416.37
Saccade R	1	3	2192033646	2192769625	735979	700.26	561.57	718.38	637.53	141.08	563.46	0.35	191.69	9665.98	-13150.08	4416.37
