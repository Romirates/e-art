[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23607420403	# Message: void.jpg
Fixation L	1	1	23607430102	23608126326	696224	1080.89	530.14	68	24	-1	17.24	17.24
Fixation R	1	1	23607430102	23608126326	696224	1080.89	530.14	68	24	-1	16.02	16.02
Saccade L	1	1	23608126326	23608146187	19861	1021.66	548.10	960.57	539.60	0.67	62.39	1.00	33.78	1464.92	-1406.11	1401.86
Saccade R	1	1	23608126326	23608146187	19861	1021.66	548.10	960.57	539.60	0.67	62.39	1.00	33.78	1464.92	-1406.11	1401.86
Fixation L	1	2	23608146187	23609419160	1272973	954.68	581.44	15	59	-1	16.40	16.40
Fixation R	1	2	23608146187	23609419160	1272973	954.68	581.44	15	59	-1	15.71	15.71
