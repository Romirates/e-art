[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23637970087	# Message: void.jpg
Fixation L	1	1	23637981402	23638478618	497216	782.15	330.97	14	59	-1	14.33	14.33
Fixation R	1	1	23637981402	23638478618	497216	782.15	330.97	14	59	-1	14.14	14.14
Saccade L	1	1	23638478618	23638637769	159151	787.16	340.76	919.84	509.18	10.36	168.17	0.50	65.12	2664.43	-3807.44	1949.43
Saccade R	1	1	23638478618	23638597998	119380	787.16	340.76	904.33	436.80	7.73	168.17	0.67	64.73	2664.43	-3807.44	2048.51
Fixation L	1	2	23638637769	23638757109	119340	932.91	494.44	26	73	-1	14.07	14.07
Fixation R	1	2	23638657619	23638757109	99490	935.09	491.98	25	73	-1	14.08	14.08
Saccade L	1	2	23638757109	23638776980	19871	940.13	497.07	950.09	452.51	0.60	52.66	1.00	30.03	228.85	-966.21	452.34
Saccade R	1	2	23638757109	23638776980	19871	940.13	497.07	950.09	452.51	0.60	52.66	1.00	30.03	228.85	-966.21	452.34
Fixation L	1	3	23638776980	23639910684	1133704	976.72	495.06	35	63	-1	15.12	15.12
Fixation R	1	3	23638776980	23639910684	1133704	976.72	495.06	35	63	-1	15.01	15.01
Saccade L	1	3	23639910684	23639970317	59633	984.59	508.15	991.37	507.84	0.18	4.08	1.00	3.03	2.47	-72.78	22.78
Saccade R	1	3	23639910684	23639970317	59633	984.59	508.15	991.37	507.84	0.18	4.08	1.00	3.03	2.47	-72.78	22.78
