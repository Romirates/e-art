[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23446475973	# Message: void.jpg
Fixation L	1	1	23446479351	23446658347	178996	867.36	581.82	64	30	-1	16.19	16.19
Fixation R	1	1	23446479351	23446658347	178996	867.36	581.82	64	30	-1	15.52	15.52
Saccade L	1	1	23446658347	23446678352	20005	838.44	596.58	835.11	600.87	0.09	5.51	1.00	4.73	-0.58	-73.76	28.28
Saccade R	1	1	23446658347	23446678352	20005	838.44	596.58	835.11	600.87	0.09	5.51	1.00	4.73	-0.58	-73.76	28.28
Fixation L	1	2	23446678352	23447095952	417600	831.62	636.00	7	69	-1	15.82	15.82
Fixation R	1	2	23446678352	23447095952	417600	831.62	636.00	7	69	-1	15.21	15.21
Blink L	1	1	23447095952	23447274944	178992
Blink R	1	1	23447095952	23447274944	178992
Fixation L	1	3	23447274944	23447971156	696212	989.15	482.35	25	64	-1	13.25	13.25
Fixation R	1	3	23447274944	23447971156	696212	989.15	482.35	25	64	-1	12.62	12.62
Saccade L	1	2	23447971156	23447991026	19870	998.93	507.55	997.27	518.94	0.13	11.65	1.00	6.53	191.77	-144.30	156.72
Saccade R	1	2	23447971156	23447991026	19870	998.93	507.55	997.27	518.94	0.13	11.65	1.00	6.53	191.77	-144.30	156.72
Fixation L	1	4	23447991026	23448468410	477384	994.62	512.17	6	16	-1	14.61	14.61
Fixation R	1	4	23447991026	23448468410	477384	994.62	512.17	6	16	-1	14.02	14.02
