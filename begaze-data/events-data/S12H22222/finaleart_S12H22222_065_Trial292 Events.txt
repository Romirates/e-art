[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:41:05
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	24187136200	# Message: void.jpg
Fixation L	1	1	24187144779	24187900608	755829	1036.64	449.96	13	84	-1	11.40	11.40
Fixation R	1	1	24187144779	24187900608	755829	1036.64	449.96	13	84	-1	11.01	11.01
Saccade L	1	1	24187900608	24187920491	19883	1042.50	486.42	1042.21	498.26	0.13	11.98	1.00	6.32	623.08	-30.79	282.94
Saccade R	1	1	24187900608	24187920491	19883	1042.50	486.42	1042.21	498.26	0.13	11.98	1.00	6.32	623.08	-30.79	282.94
Fixation L	1	2	24187920491	24188914937	994446	1016.44	518.86	51	48	-1	12.52	12.52
Fixation R	1	2	24187920491	24188914937	994446	1016.44	518.86	51	48	-1	12.13	12.13
Saccade L	1	2	24188914937	24188934813	19876	990.70	521.05	988.15	528.86	0.14	8.50	0.00	6.99	131.38	-52.77	64.58
Saccade R	1	2	24188914937	24188934813	19876	990.70	521.05	988.15	528.86	0.14	8.50	0.00	6.99	131.38	-52.77	64.58
Fixation L	1	3	24188934813	24189133810	198997	987.39	518.26	4	25	-1	11.94	11.94
Fixation R	1	3	24188934813	24189133810	198997	987.39	518.26	4	25	-1	11.87	11.87
