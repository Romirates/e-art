[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23785866134	# Message: void.jpg
Fixation L	1	1	23785885219	23787317273	1432054	950.49	700.97	18	60	-1	14.32	14.32
Fixation R	1	1	23785885219	23787317273	1432054	950.49	700.97	18	60	-1	13.62	13.62
Saccade L	1	1	23787317273	23787337146	19873	960.98	689.87	941.79	637.60	0.33	56.32	1.00	16.74	1360.02	-1323.95	924.04
Saccade R	1	1	23787317273	23787337146	19873	960.98	689.87	941.79	637.60	0.33	56.32	1.00	16.74	1360.02	-1323.95	924.04
Fixation L	1	2	23787337146	23787854382	517236	912.56	624.14	36	26	-1	15.75	15.75
Fixation R	1	2	23787337146	23787854382	517236	912.56	624.14	36	26	-1	14.98	14.98
