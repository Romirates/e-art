[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23168544318	# Message: void.jpg
Fixation L	1	1	23168554959	23168654318	99359	851.57	278.04	2	20	-1	14.45	14.45
Fixation R	1	1	23168554959	23168654318	99359	851.57	278.04	2	20	-1	13.42	13.42
Saccade L	1	1	23168654318	23168714066	59748	852.45	268.28	605.32	722.73	6.02	265.06	0.67	100.82	6532.41	-5275.83	4076.60
Saccade R	1	1	23168654318	23168714066	59748	852.45	268.28	605.32	722.73	6.02	265.06	0.67	100.82	6532.41	-5275.83	4076.60
Fixation L	1	2	23168714066	23169111781	397715	603.20	747.06	33	63	-1	14.77	14.77
Fixation R	1	2	23168714066	23169111781	397715	603.20	747.06	33	63	-1	14.25	14.25
Blink L	1	1	23169111781	23169270904	159123
Blink R	1	1	23169111781	23169270904	159123
Fixation L	1	3	23169270904	23169449923	179019	850.90	421.97	33	56	-1	14.64	14.64
Fixation R	1	3	23169270904	23169449923	179019	850.90	421.97	33	56	-1	14.23	14.23
Saccade L	1	2	23169449923	23169469894	19971	862.73	447.67	864.45	459.34	0.18	13.23	1.00	9.23	-2.05	-194.48	76.30
Saccade R	1	2	23169449923	23169469894	19971	862.73	447.67	864.45	459.34	0.18	13.23	1.00	9.23	-2.05	-194.48	76.30
Fixation L	1	4	23169469894	23170543973	1074079	882.85	435.71	32	58	-1	15.19	15.19
Fixation R	1	4	23169469894	23170543973	1074079	882.85	435.71	32	58	-1	15.07	15.07
