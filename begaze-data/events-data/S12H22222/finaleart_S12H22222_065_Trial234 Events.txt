[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:49
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23840865444	# Message: void.jpg
Fixation L	1	1	23840881342	23841020598	139256	464.89	762.89	23	74	-1	16.41	16.41
Fixation R	1	1	23840881342	23841020598	139256	464.89	762.89	23	74	-1	15.77	15.77
Saccade L	1	1	23841020598	23841040504	19906	483.09	704.14	478.48	696.11	0.47	40.25	1.00	23.47	14.70	-772.13	500.92
Saccade R	1	1	23841020598	23841040504	19906	483.09	704.14	478.48	696.11	0.47	40.25	1.00	23.47	14.70	-772.13	500.92
Fixation L	1	2	23841040504	23841299049	258545	487.16	688.08	12	20	-1	17.08	17.08
Fixation R	1	2	23841040504	23841338803	298299	487.16	696.61	12	84	-1	16.05	16.05
Blink L	1	1	23841299049	23841577550	278501
Blink R	1	1	23841338803	23841577550	238747
Fixation L	1	3	23841577550	23842054895	477345	929.20	520.39	18	81	-1	13.19	13.19
Fixation R	1	3	23841577550	23842054895	477345	929.20	520.39	18	81	-1	12.52	12.52
Saccade L	1	2	23842054895	23842074773	19878	919.76	540.75	918.98	540.78	0.11	14.71	0.00	5.63	3.01	-246.68	94.81
Saccade R	1	2	23842054895	23842074773	19878	919.76	540.75	918.98	540.78	0.11	14.71	0.00	5.63	3.01	-246.68	94.81
Fixation L	1	4	23842074773	23842850485	775712	969.73	550.76	72	18	-1	14.09	14.09
Fixation R	1	4	23842074773	23842850485	775712	969.73	550.76	72	18	-1	13.49	13.49
