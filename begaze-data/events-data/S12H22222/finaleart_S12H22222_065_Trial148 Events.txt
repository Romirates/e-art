[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:27
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23483168890	# Message: void.jpg
Fixation L	1	1	23483176630	23483614223	437593	1368.96	295.11	61	33	-1	12.07	12.07
Fixation R	1	1	23483176630	23483614223	437593	1368.96	295.11	61	33	-1	11.97	11.97
Blink L	1	1	23483614223	23483932464	318241
Blink R	1	1	23483614223	23483932464	318241
Saccade L	1	1	23483932464	23483952338	19874	798.20	439.87	875.53	439.19	0.86	78.22	1.00	43.42	816.54	-1727.58	1001.10
Saccade R	1	1	23483932464	23483952338	19874	798.20	439.87	875.53	439.19	0.86	78.22	1.00	43.42	816.54	-1727.58	1001.10
Fixation L	1	2	23483952338	23484628562	676224	921.08	445.32	59	37	-1	13.29	13.29
Fixation R	1	2	23483952338	23484628562	676224	921.08	445.32	59	37	-1	12.73	12.73
Saccade L	1	2	23484628562	23484648429	19867	934.58	455.30	929.83	568.81	0.63	114.92	1.00	31.52	2769.88	-2617.94	1797.11
Saccade R	1	2	23484628562	23484648429	19867	934.58	455.30	929.83	568.81	0.63	114.92	1.00	31.52	2769.88	-2617.94	1797.11
Fixation L	1	3	23484648429	23485165665	517236	907.92	556.39	30	28	-1	13.30	13.30
Fixation R	1	3	23484648429	23485165665	517236	907.92	556.39	30	28	-1	13.00	13.00
