[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:20
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23403682977	# Message: void.jpg
Saccade L	1	1	23403695837	23403775340	79503	1304.70	711.11	1081.31	455.59	5.01	210.58	0.75	62.96	5162.90	-4921.02	2830.69
Saccade R	1	1	23403695837	23403775340	79503	1304.70	711.11	1081.31	455.59	5.01	210.58	0.75	62.96	5162.90	-4921.02	2830.69
Fixation L	1	1	23403775340	23405664868	1889528	1080.74	449.44	23	45	-1	15.07	15.07
Fixation R	1	1	23403775340	23405664868	1889528	1080.74	449.44	23	45	-1	14.66	14.66
