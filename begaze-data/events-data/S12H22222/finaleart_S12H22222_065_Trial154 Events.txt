[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23501547505	# Message: void.jpg
Fixation L	1	1	23501555088	23502072171	517083	1091.65	329.91	45	35	-1	14.68	14.68
Fixation R	1	1	23501555088	23502072171	517083	1091.65	329.91	45	35	-1	13.60	13.60
Saccade L	1	1	23502072171	23502151808	79637	1060.18	349.86	852.63	448.41	3.64	100.30	0.50	45.67	1474.41	-874.36	759.15
Saccade R	1	1	23502072171	23502151808	79637	1060.18	349.86	852.63	448.41	3.64	100.30	0.50	45.67	1474.41	-874.36	759.15
Fixation L	1	2	23502151808	23503484368	1332560	853.88	438.54	30	69	-1	15.61	15.61
Fixation R	1	2	23502151808	23503484368	1332560	853.88	438.54	30	69	-1	14.42	14.42
Saccade L	1	2	23503484368	23503544128	59760	870.95	439.48	893.84	436.23	0.38	13.74	0.33	6.29	329.61	-301.39	196.49
Saccade R	1	2	23503484368	23503544128	59760	870.95	439.48	893.84	436.23	0.38	13.74	0.33	6.29	329.61	-301.39	196.49
