[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:39:49
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	22841118307	# Message: void.jpg
Saccade L	1	1	22841132451	22841231923	99472	458.24	699.16	1032.52	568.94	8.71	252.38	1.00	87.53	6187.56	-5502.64	3426.82
Saccade R	1	1	22841132451	22841231923	99472	458.24	699.16	1032.52	568.94	8.71	252.38	1.00	87.53	6187.56	-5502.64	3426.82
Fixation L	1	1	22841231923	22841848527	616604	1074.84	557.03	53	29	-1	14.67	14.67
Fixation R	1	1	22841231923	22841848527	616604	1074.84	557.03	53	29	-1	14.46	14.46
Saccade L	1	2	22841848527	22841868414	19887	1053.92	544.76	953.84	553.81	0.70	101.64	1.00	35.43	2439.55	-2382.66	1793.26
Saccade R	1	2	22841848527	22841868414	19887	1053.92	544.76	953.84	553.81	0.70	101.64	1.00	35.43	2439.55	-2382.66	1793.26
Fixation L	1	2	22841868414	22843101599	1233185	933.49	558.49	36	42	-1	14.87	14.87
Fixation R	1	2	22841868414	22843101599	1233185	933.49	558.49	36	42	-1	14.86	14.86
