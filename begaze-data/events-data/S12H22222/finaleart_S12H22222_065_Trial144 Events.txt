[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:26
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23470911955	# Message: void.jpg
Fixation L	1	1	23470924293	23471481272	556979	925.27	476.40	35	42	-1	12.41	12.41
Fixation R	1	1	23470924293	23471481272	556979	925.27	476.40	35	42	-1	11.91	11.91
Blink L	1	1	23471481272	23471759765	278493
Blink R	1	1	23471481272	23471759765	278493
Fixation L	1	2	23471759765	23472595091	835326	992.52	458.61	22	77	-1	13.55	13.55
Fixation R	1	2	23471759765	23472595091	835326	992.52	458.61	22	77	-1	12.90	12.90
Saccade L	1	1	23472595091	23472614954	19863	999.88	485.36	1001.81	483.81	0.06	5.19	1.00	3.09	11.28	-67.11	38.98
Saccade R	1	1	23472595091	23472614954	19863	999.88	485.36	1001.81	483.81	0.06	5.19	1.00	3.09	11.28	-67.11	38.98
Fixation L	1	3	23472614954	23472893453	278499	999.84	487.65	10	5	-1	13.30	13.30
Fixation R	1	3	23472614954	23472893453	278499	999.84	487.65	10	5	-1	12.74	12.74
