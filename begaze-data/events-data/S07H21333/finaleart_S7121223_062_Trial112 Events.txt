[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:36:51
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4315919737	# Message: void.jpg
Fixation L	1	1	4315931368	4316309214	377846	1568.86	1077.23	31	25	-1	13.68	13.68
Fixation R	1	1	4315931368	4316309214	377846	1568.86	1077.23	31	25	-1	12.64	12.64
Saccade L	1	1	4316309214	4316348977	39763	1562.20	1075.66	1133.79	708.24	4.91	353.37	1.00	123.38	8784.96	-8440.98	6816.97
Saccade R	1	1	4316309214	4316348977	39763	1562.20	1075.66	1133.79	708.24	4.91	353.37	1.00	123.38	8784.96	-8440.98	6816.97
Fixation L	1	2	4316348977	4316587705	238728	1091.82	685.83	50	48	-1	14.21	14.21
Fixation R	1	2	4316348977	4316587705	238728	1091.82	685.83	50	48	-1	13.58	13.58
Saccade L	1	2	4316587705	4316647331	59626	1085.17	673.42	857.02	483.75	3.73	142.18	0.67	62.61	3455.18	-2495.26	2137.85
Saccade R	1	2	4316587705	4316647331	59626	1085.17	673.42	857.02	483.75	3.73	142.18	0.67	62.61	3455.18	-2495.26	2137.85
Fixation L	1	3	4316647331	4317920256	1272925	833.62	471.15	29	63	-1	14.99	14.99
Fixation R	1	3	4316647331	4317920256	1272925	833.62	471.15	29	63	-1	15.30	15.30
