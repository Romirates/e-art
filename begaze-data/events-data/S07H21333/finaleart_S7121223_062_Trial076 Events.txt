[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:36:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4055868155	# Message: void.jpg
Fixation L	1	1	4055875793	4056352976	477183	1121.80	876.51	12	13	-1	17.78	17.78
Fixation R	1	1	4055875793	4056352976	477183	1121.80	876.51	12	13	-1	17.20	17.20
Saccade L	1	1	4056352976	4056432596	79620	1124.79	873.77	845.63	470.26	6.09	194.94	0.25	76.50	4821.19	-2632.87	2664.38
Saccade R	1	1	4056352976	4056432596	79620	1124.79	873.77	845.63	470.26	6.09	194.94	0.25	76.50	4821.19	-2632.87	2664.38
Fixation L	1	2	4056432596	4057864646	1432050	835.94	444.91	26	43	-1	15.06	15.06
Fixation R	1	2	4056432596	4057864646	1432050	835.94	444.91	26	43	-1	15.32	15.32
