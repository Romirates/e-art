[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5129727400	# Message: void.jpg
Fixation L	1	1	5129738324	5130354921	616597	499.85	972.71	24	17	-1	12.73	12.73
Fixation R	1	1	5129738324	5130354921	616597	499.85	972.71	24	17	-1	13.39	13.39
Saccade L	1	1	5130354921	5130434416	79495	503.06	975.87	1093.87	692.13	7.80	286.11	0.50	98.14	7109.24	-3993.57	3651.72
Saccade R	1	1	5130354921	5130434416	79495	503.06	975.87	1093.87	692.13	7.80	286.11	0.50	98.14	7109.24	-3993.57	3651.72
Fixation L	1	2	5130434416	5130772514	338098	1091.84	682.95	18	21	-1	13.31	13.31
Fixation R	1	2	5130434416	5130772514	338098	1091.84	682.95	18	21	-1	13.79	13.79
Saccade L	1	2	5130772514	5130852129	79615	1077.75	680.28	797.71	532.59	4.66	157.77	0.25	58.56	3795.57	-2395.28	1871.40
Saccade R	1	2	5130772514	5130852129	79615	1077.75	680.28	797.71	532.59	4.66	157.77	0.25	58.56	3795.57	-2395.28	1871.40
Fixation L	1	3	5130852129	5131727228	875099	821.41	535.59	48	28	-1	14.12	14.12
Fixation R	1	3	5130852129	5131727228	875099	821.41	535.59	48	28	-1	14.51	14.51
