[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:04
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4838467743	# Message: void.jpg
Fixation L	1	1	4838472461	4838771185	298724	755.05	583.22	86	13	-1	14.53	14.53
Fixation R	1	1	4838472461	4838771185	298724	755.05	583.22	86	13	-1	14.68	14.68
Saccade L	1	1	4838771185	4838791079	19894	815.10	588.52	816.48	591.50	0.15	10.31	0.00	7.33	7.40	-174.60	101.52
Saccade R	1	1	4838771185	4838791079	19894	815.10	588.52	816.48	591.50	0.15	10.31	0.00	7.33	7.40	-174.60	101.52
Fixation L	1	2	4838791079	4839408407	617328	826.35	592.15	75	22	-1	14.49	14.49
Fixation R	1	2	4838791079	4839408407	617328	826.35	592.15	75	22	-1	14.75	14.75
Saccade L	1	2	4839408407	4839428285	19878	892.04	601.23	899.48	617.79	0.40	37.98	0.00	19.96	86.76	-734.28	309.29
Saccade R	1	2	4839408407	4839428285	19878	892.04	601.23	899.48	617.79	0.40	37.98	0.00	19.96	86.76	-734.28	309.29
Fixation L	1	3	4839428285	4840463739	1035454	928.65	615.83	46	19	-1	15.41	15.41
Fixation R	1	3	4839428285	4840463739	1035454	928.65	615.83	46	19	-1	15.45	15.45
