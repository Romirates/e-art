[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5509964243	# Message: void.jpg
Saccade L	1	1	5509974683	5510054184	79501	1104.05	820.88	422.24	801.07	9.50	270.35	0.75	119.52	6730.85	-6097.67	3950.76
Saccade R	1	1	5509974683	5510054184	79501	1104.05	820.88	422.24	801.07	9.50	270.35	0.75	119.52	6730.85	-6097.67	3950.76
Fixation L	1	1	5510054184	5510253157	198973	391.52	781.46	52	28	-1	13.48	13.48
Fixation R	1	1	5510054184	5510253157	198973	391.52	781.46	52	28	-1	13.81	13.81
Saccade L	1	2	5510253157	5510273024	19867	369.68	772.58	351.55	764.71	0.36	26.20	1.00	18.16	427.43	-281.48	258.36
Saccade R	1	2	5510253157	5510273024	19867	369.68	772.58	351.55	764.71	0.36	26.20	1.00	18.16	427.43	-281.48	258.36
Fixation L	1	2	5510273024	5511048746	775722	332.36	741.83	40	30	-1	13.62	13.62
Fixation R	1	2	5510273024	5511048746	775722	332.36	741.83	40	30	-1	14.55	14.55
Saccade L	1	3	5511048746	5511088483	39737	363.05	741.33	788.59	582.24	4.38	249.61	0.50	110.13	6162.85	-4680.09	5033.47
Saccade R	1	3	5511048746	5511088483	39737	363.05	741.33	788.59	582.24	4.38	249.61	0.50	110.13	6162.85	-4680.09	5033.47
Fixation L	1	3	5511088483	5511963567	875084	798.19	554.45	25	66	-1	14.61	14.61
Fixation R	1	3	5511088483	5511963567	875084	798.19	554.45	25	66	-1	15.19	15.19
