[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:14
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5123603158	# Message: void.jpg
Fixation L	1	1	5123612395	5123791389	178994	895.13	661.89	26	12	-1	12.41	12.41
Fixation R	1	1	5123612395	5123791389	178994	895.13	661.89	26	12	-1	12.58	12.58
Saccade L	1	1	5123791389	5123831130	39741	901.45	657.72	1081.44	618.07	1.73	97.31	1.00	43.55	2414.39	-2185.99	2188.77
Saccade R	1	1	5123791389	5123831130	39741	901.45	657.72	1081.44	618.07	1.73	97.31	1.00	43.55	2414.39	-2185.99	2188.77
Fixation L	1	2	5123831130	5124825704	994574	1073.65	600.97	40	44	-1	12.94	12.94
Fixation R	1	2	5123831130	5124825704	994574	1073.65	600.97	40	44	-1	12.64	12.64
Saccade L	1	2	5124825704	5124845571	19867	1045.40	605.19	879.11	597.68	1.18	168.34	1.00	59.27	4149.60	-4169.65	2806.71
Saccade R	1	2	5124825704	5124845571	19867	1045.40	605.19	879.11	597.68	1.18	168.34	1.00	59.27	4149.60	-4169.65	2806.71
Fixation L	1	3	5124845571	5125601282	755711	846.61	555.82	39	52	-1	13.80	13.80
Fixation R	1	3	5124845571	5125601282	755711	846.61	555.82	39	52	-1	14.13	14.13
