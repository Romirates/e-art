[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5522179760	# Message: void.jpg
Fixation L	1	1	5522186672	5523956820	1770148	567.99	549.71	54	38	-1	13.42	13.42
Fixation R	1	1	5522186672	5523956820	1770148	567.99	549.71	54	38	-1	13.99	13.99
Saccade L	1	1	5523956820	5523976700	19880	606.24	555.88	652.11	572.48	0.47	49.34	1.00	23.40	1201.83	-1142.27	1100.58
Saccade R	1	1	5523956820	5523976700	19880	606.24	555.88	652.11	572.48	0.47	49.34	1.00	23.40	1201.83	-1142.27	1100.58
Fixation L	1	2	5523976700	5524175687	198987	655.82	565.06	6	20	-1	14.71	14.71
Fixation R	1	2	5523976700	5524175687	198987	655.82	565.06	6	20	-1	15.43	15.43
