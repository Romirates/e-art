[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:11
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5062465291	# Message: void.jpg
Fixation L	1	1	5062472574	5063765367	1292793	611.77	528.65	48	33	-1	14.96	14.96
Fixation R	1	1	5062472574	5063765367	1292793	611.77	528.65	48	33	-1	15.37	15.37
Saccade L	1	1	5063765367	5063805276	39909	648.25	536.93	1003.60	478.79	3.46	219.47	0.50	86.75	5445.10	-4814.34	4003.69
Saccade R	1	1	5063765367	5063805276	39909	648.25	536.93	1003.60	478.79	3.46	219.47	0.50	86.75	5445.10	-4814.34	4003.69
Fixation L	1	2	5063805276	5064024004	218728	992.64	428.60	14	66	-1	15.53	15.53
Fixation R	1	2	5063805276	5064024004	218728	992.64	428.60	14	66	-1	16.08	16.08
Saccade L	1	2	5064024004	5064043850	19846	989.51	426.68	923.19	442.32	0.50	68.93	1.00	25.06	1709.98	-1364.56	1229.11
Saccade R	1	2	5064024004	5064043850	19846	989.51	426.68	923.19	442.32	0.50	68.93	1.00	25.06	1709.98	-1364.56	1229.11
Fixation L	1	3	5064043850	5064461584	417734	884.86	448.21	44	37	-1	15.30	15.30
Fixation R	1	3	5064043850	5064461584	417734	884.86	448.21	44	37	-1	15.35	15.35
