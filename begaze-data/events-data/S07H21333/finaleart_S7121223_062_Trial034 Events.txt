[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:36:36
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3756979092	# Message: void.jpg
Saccade L	1	1	3756982314	3757022041	39727	896.50	718.96	990.98	643.57	1.77	119.78	1.00	44.61	2994.44	-2211.11	1634.70
Saccade R	1	1	3756982314	3757022041	39727	896.50	718.96	990.98	643.57	1.77	119.78	1.00	44.61	2994.44	-2211.11	1634.70
Fixation L	1	1	3757022041	3757261043	239002	995.12	662.12	33	51	-1	13.19	13.19
Fixation R	1	1	3757022041	3757261043	239002	995.12	662.12	33	51	-1	14.47	14.47
Saccade L	1	2	3757261043	3757280900	19857	996.82	662.03	1108.30	664.75	0.96	112.80	1.00	48.12	2254.58	-2506.33	1866.69
Saccade R	1	2	3757261043	3757280900	19857	996.82	662.03	1108.30	664.75	0.96	112.80	1.00	48.12	2254.58	-2506.33	1866.69
Fixation L	1	2	3757280900	3757938137	657237	1165.82	649.79	63	27	-1	13.29	13.29
Fixation R	1	2	3757280900	3757938137	657237	1165.82	649.79	63	27	-1	14.41	14.41
Saccade L	1	3	3757938137	3757997740	59603	1164.95	660.63	937.49	542.09	3.17	124.17	0.33	53.16	2966.91	-1777.64	1812.49
Saccade R	1	3	3757938137	3757997740	59603	1164.95	660.63	937.49	542.09	3.17	124.17	0.33	53.16	2966.91	-1777.64	1812.49
Fixation L	1	3	3757997740	3758973580	975840	945.41	522.04	35	52	-1	15.28	15.28
Fixation R	1	3	3757997740	3758973580	975840	945.41	522.04	35	52	-1	16.56	16.56
