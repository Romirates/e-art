[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4869007673	# Message: void.jpg
Fixation L	1	1	4869018381	4869177606	159225	760.44	673.59	5	27	-1	16.88	16.88
Fixation R	1	1	4869018381	4869177606	159225	760.44	673.59	5	27	-1	16.53	16.53
Saccade L	1	1	4869177606	4869197512	19906	759.72	684.21	616.88	685.56	0.75	144.47	1.00	37.73	3600.80	-3529.71	2379.33
Saccade R	1	1	4869177606	4869197512	19906	759.72	684.21	616.88	685.56	0.75	144.47	1.00	37.73	3600.80	-3529.71	2379.33
Fixation L	1	2	4869197512	4870053813	856301	604.95	659.35	23	58	-1	15.41	15.41
Fixation R	1	2	4869197512	4870053813	856301	604.95	659.35	23	58	-1	15.54	15.54
Saccade L	1	2	4870053813	4870113557	59744	604.10	636.55	938.23	516.48	4.34	183.35	0.33	72.63	4525.74	-2864.16	2934.47
Saccade R	1	2	4870053813	4870113557	59744	604.10	636.55	938.23	516.48	4.34	183.35	0.33	72.63	4525.74	-2864.16	2934.47
Fixation L	1	3	4870113557	4870989642	876085	952.91	510.73	54	36	-1	15.17	15.17
Fixation R	1	3	4870113557	4870989642	876085	952.91	510.73	54	36	-1	15.16	15.16
