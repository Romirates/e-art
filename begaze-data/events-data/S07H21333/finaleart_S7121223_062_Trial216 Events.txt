[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5056347695	# Message: void.jpg
Fixation L	1	1	5056366640	5056943351	576711	744.89	657.14	11	30	-1	14.57	14.57
Fixation R	1	1	5056366640	5056943351	576711	744.89	657.14	11	30	-1	14.94	14.94
Saccade L	1	1	5056943351	5056983286	39935	749.68	673.21	602.57	481.62	2.60	154.62	1.00	65.12	3720.50	-3706.77	3063.29
Saccade R	1	1	5056943351	5056983286	39935	749.68	673.21	602.57	481.62	2.60	154.62	1.00	65.12	3720.50	-3706.77	3063.29
Fixation L	1	2	5056983286	5057798677	815391	596.32	441.44	11	53	-1	16.06	16.06
Fixation R	1	2	5056983286	5057798677	815391	596.32	441.44	11	53	-1	16.23	16.23
Saccade L	1	2	5057798677	5057838427	39750	600.45	458.97	775.34	417.74	1.52	111.64	1.00	38.25	2763.26	-2557.96	2189.78
Saccade R	1	2	5057798677	5057838427	39750	600.45	458.97	775.34	417.74	1.52	111.64	1.00	38.25	2763.26	-2557.96	2189.78
Fixation L	1	3	5057838427	5058335648	497221	797.26	405.82	28	31	-1	16.41	16.41
Fixation R	1	3	5057838427	5058335648	497221	797.26	405.82	28	31	-1	16.69	16.69
