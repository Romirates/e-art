[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5050255484	# Message: void.jpg
Saccade L	1	1	5050260579	5050280454	19875	502.25	526.18	499.91	627.45	0.72	102.46	1.00	36.02	140.06	-2459.50	866.52
Saccade R	1	1	5050260579	5050280454	19875	502.25	526.18	499.91	627.45	0.72	102.46	1.00	36.02	140.06	-2459.50	866.52
Fixation L	1	1	5050280454	5050857307	576853	491.08	616.96	37	56	-1	14.24	14.24
Fixation R	1	1	5050280454	5050857307	576853	491.08	616.96	37	56	-1	14.67	14.67
Saccade L	1	2	5050857307	5050976558	119251	503.02	631.25	1029.05	520.29	7.95	220.76	0.33	66.66	5468.95	-5266.53	2429.21
Saccade R	1	2	5050857307	5050976558	119251	503.02	631.25	1029.05	520.29	7.95	220.76	0.33	66.66	5468.95	-5266.53	2429.21
Fixation L	1	2	5050976558	5052110235	1133677	1046.92	509.25	57	40	-1	15.21	15.21
Fixation R	1	2	5050976558	5052110235	1133677	1046.92	509.25	57	40	-1	14.99	14.99
Saccade L	1	3	5052110235	5052130224	19989	1003.90	518.77	998.76	519.29	0.10	6.12	1.00	4.92	-3.93	-30.70	18.95
Saccade R	1	3	5052110235	5052130224	19989	1003.90	518.77	998.76	519.29	0.10	6.12	1.00	4.92	-3.93	-30.70	18.95
Fixation L	1	3	5052130224	5052249473	119249	998.66	516.61	4	4	-1	15.84	15.84
Fixation R	1	3	5052130224	5052249473	119249	998.66	516.61	4	4	-1	15.84	15.84
