[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5166361108	# Message: void.jpg
Fixation L	1	1	5166368063	5166527283	159220	644.81	276.19	21	24	-1	13.13	13.13
Fixation R	1	1	5166368063	5166527283	159220	644.81	276.19	21	24	-1	15.33	15.33
Saccade L	1	1	5166527283	5166547277	19994	627.29	261.72	572.62	271.85	0.53	56.24	1.00	26.62	1365.14	-948.69	921.58
Saccade R	1	1	5166527283	5166547277	19994	627.29	261.72	572.62	271.85	0.53	56.24	1.00	26.62	1365.14	-948.69	921.58
Fixation L	1	2	5166547277	5166766156	218879	566.22	240.69	12	41	-1	13.61	13.61
Fixation R	1	2	5166547277	5166766156	218879	566.22	240.69	12	41	-1	15.52	15.52
Saccade L	1	2	5166766156	5166885631	119475	571.82	230.95	980.10	571.17	8.19	178.11	0.33	68.57	4333.67	-2844.39	1807.09
Saccade R	1	2	5166766156	5166885631	119475	571.82	230.95	980.10	571.17	8.19	178.11	0.33	68.57	4333.67	-2844.39	1807.09
Fixation L	1	3	5166885631	5167144380	258749	989.30	615.98	15	57	-1	13.96	13.96
Fixation R	1	3	5166885631	5167144380	258749	989.30	615.98	15	57	-1	16.02	16.02
Blink L	1	1	5167144380	5167343361	198981
Blink R	1	1	5167144380	5167343361	198981
Fixation L	1	4	5167343361	5168358434	1015073	1027.24	462.25	37	16	-1	15.04	15.04
Fixation R	1	4	5167343361	5168358434	1015073	1027.24	462.25	37	16	-1	14.91	14.91
