[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:36:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3769205284	# Message: void.jpg
Saccade L	1	1	3769208536	3769248412	39876	801.08	765.74	438.97	765.98	4.27	257.37	1.00	107.16	6434.22	-6126.59	3432.05
Saccade R	1	1	3769208536	3769248412	39876	801.08	765.74	438.97	765.98	4.27	257.37	1.00	107.16	6434.22	-6126.59	3432.05
Fixation L	1	1	3769248412	3769865649	617237	398.19	759.46	61	32	-1	15.08	15.08
Fixation R	1	1	3769248412	3769865649	617237	398.19	759.46	61	32	-1	17.85	17.85
Saccade L	1	2	3769865649	3769925389	59740	394.44	750.42	828.29	523.49	5.41	247.62	0.33	90.60	6100.35	-4109.48	3772.96
Saccade R	1	2	3769865649	3769925389	59740	394.44	750.42	828.29	523.49	5.41	247.62	0.33	90.60	6100.35	-4109.48	3772.96
Fixation L	1	2	3769925389	3770283855	358466	893.46	522.71	77	20	-1	15.73	15.73
Fixation R	1	2	3769925389	3770283855	358466	893.46	522.71	77	20	-1	18.38	18.38
Saccade L	1	3	3770283855	3770303724	19869	867.90	537.83	788.14	554.06	0.74	82.33	1.00	37.00	1997.51	-1809.16	1409.72
Saccade R	1	3	3770283855	3770303724	19869	867.90	537.83	788.14	554.06	0.74	82.33	1.00	37.00	1997.51	-1809.16	1409.72
Fixation L	1	3	3770303724	3771199813	896089	765.15	580.39	31	36	-1	17.19	17.19
Fixation R	1	3	3770303724	3771199813	896089	765.15	580.39	31	36	-1	19.47	19.47
