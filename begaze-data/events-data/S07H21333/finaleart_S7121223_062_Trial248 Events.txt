[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5154178413	# Message: void.jpg
Saccade L	1	1	5154186804	5154206801	19997	1453.53	652.81	1214.95	651.12	1.85	241.22	1.00	92.28	890.79	-5824.07	2238.29
Saccade R	1	1	5154186804	5154206801	19997	1453.53	652.81	1214.95	651.12	1.85	241.22	1.00	92.28	890.79	-5824.07	2238.29
Fixation L	1	1	5154206801	5155002905	796104	1170.38	619.28	53	45	-1	13.05	13.05
Fixation R	1	1	5154206801	5155002905	796104	1170.38	619.28	53	45	-1	14.58	14.58
Saccade L	1	2	5155002905	5155122374	119469	1161.99	616.92	832.51	510.96	5.34	152.94	0.83	44.73	3729.48	-3605.70	1608.99
Saccade R	1	2	5155002905	5155122374	119469	1161.99	616.92	832.51	510.96	5.34	152.94	0.83	44.73	3729.48	-3605.70	1608.99
Fixation L	1	2	5155122374	5155898577	776203	822.41	511.57	16	31	-1	14.04	14.04
Fixation R	1	2	5155122374	5155898577	776203	822.41	511.57	16	31	-1	15.83	15.83
Saccade L	1	3	5155898577	5155918450	19873	831.63	518.13	892.65	505.46	0.51	63.04	1.00	25.69	1547.96	-1487.06	1194.65
Saccade R	1	3	5155898577	5155918450	19873	831.63	518.13	892.65	505.46	0.51	63.04	1.00	25.69	1547.96	-1487.06	1194.65
Fixation L	1	3	5155918450	5156177193	258743	921.01	480.23	36	35	-1	14.32	14.32
Fixation R	1	3	5155918450	5156177193	258743	921.01	480.23	36	35	-1	16.33	16.33
