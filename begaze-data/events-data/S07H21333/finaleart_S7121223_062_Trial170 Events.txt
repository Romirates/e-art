[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:02
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4795799222	# Message: void.jpg
Fixation L	1	1	4795799934	4795939403	139469	1228.33	562.83	11	28	-1	14.80	14.80
Fixation R	1	1	4795799934	4795939403	139469	1228.33	562.83	11	28	-1	13.40	13.40
Saccade L	1	1	4795939403	4795959390	19987	1221.55	578.97	1175.65	594.02	0.38	48.86	1.00	19.09	1137.58	-718.54	653.20
Saccade R	1	1	4795939403	4795959390	19987	1221.55	578.97	1175.65	594.02	0.38	48.86	1.00	19.09	1137.58	-718.54	653.20
Fixation L	1	2	4795959390	4796775627	816237	1143.24	613.62	40	34	-1	15.40	15.40
Fixation R	1	2	4795959390	4796775627	816237	1143.24	613.62	40	34	-1	13.81	13.81
Saccade L	1	2	4796775627	4796815509	39882	1136.07	623.14	911.99	636.80	2.30	120.87	0.50	57.57	2958.73	-2210.04	2348.26
Saccade R	1	2	4796775627	4796815509	39882	1136.07	623.14	911.99	636.80	2.30	120.87	0.50	57.57	2958.73	-2210.04	2348.26
Fixation L	1	3	4796815509	4797791200	975691	842.43	638.99	76	12	-1	16.16	16.16
Fixation R	1	3	4796815509	4797791200	975691	842.43	638.99	76	12	-1	15.24	15.24
