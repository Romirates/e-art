[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4298722655	# Message: void.jpg
Fixation L	1	1	4298730545	4298929528	198983	571.88	496.73	18	72	-1	13.81	13.81
Fixation R	1	1	4298730545	4298929528	198983	571.88	496.73	18	72	-1	14.59	14.59
Saccade L	1	1	4298929528	4298949417	19889	583.07	511.37	724.45	524.38	0.96	143.60	1.00	48.45	3059.77	-3244.36	2244.42
Saccade R	1	1	4298929528	4298949417	19889	583.07	511.37	724.45	524.38	0.96	143.60	1.00	48.45	3059.77	-3244.36	2244.42
Fixation L	1	2	4298949417	4299844499	895082	735.39	552.07	32	35	-1	14.34	14.34
Fixation R	1	2	4298949417	4299844499	895082	735.39	552.07	32	35	-1	14.83	14.83
Saccade L	1	2	4299844499	4299884232	39733	754.64	557.75	1003.34	546.35	2.26	128.16	0.50	56.90	3072.47	-3004.22	2924.32
Saccade R	1	2	4299844499	4299884232	39733	754.64	557.75	1003.34	546.35	2.26	128.16	0.50	56.90	3072.47	-3004.22	2924.32
Fixation L	1	3	4299884232	4300719566	835334	1009.11	506.50	9	59	-1	14.15	14.15
Fixation R	1	3	4299884232	4300719566	835334	1009.11	506.50	9	59	-1	14.21	14.21
