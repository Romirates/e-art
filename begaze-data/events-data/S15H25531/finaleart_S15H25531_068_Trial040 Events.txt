[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3443200563	# Message: void.jpg
Fixation L	1	1	3443208687	3443467287	258600	838.37	581.32	17	12	-1	14.72	14.72
Fixation R	1	1	3443208687	3443467287	258600	838.37	581.32	17	12	-1	16.58	16.58
Saccade L	1	1	3443467287	3443487164	19877	848.22	574.87	962.05	550.19	0.75	117.81	1.00	37.86	2879.13	-2876.47	2101.54
Saccade R	1	1	3443467287	3443487164	19877	848.22	574.87	962.05	550.19	0.75	117.81	1.00	37.86	2879.13	-2876.47	2101.54
Fixation L	1	2	3443487164	3445197709	1710545	936.72	572.85	38	51	-1	15.00	15.00
Fixation R	1	2	3443487164	3445197709	1710545	936.72	572.85	38	51	-1	16.95	16.95
