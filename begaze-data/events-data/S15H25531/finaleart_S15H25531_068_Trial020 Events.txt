[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:26
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3382120263	# Message: void.jpg
Fixation L	1	1	3382126470	3383160689	1034219	1037.21	598.93	13	34	-1	16.10	16.10
Fixation R	1	1	3382126470	3383160689	1034219	1037.21	598.93	13	34	-1	17.71	17.71
Saccade L	1	1	3383160689	3383180670	19981	1033.33	604.49	871.41	618.58	0.93	164.37	1.00	46.44	4067.87	-3992.32	2763.33
Saccade R	1	1	3383160689	3383180670	19981	1033.33	604.49	871.41	618.58	0.93	164.37	1.00	46.44	4067.87	-3992.32	2763.33
Fixation L	1	2	3383180670	3384016010	835340	824.58	629.01	63	35	-1	16.52	16.52
Fixation R	1	2	3383180670	3384016010	835340	824.58	629.01	63	35	-1	18.10	18.10
Saccade L	1	2	3384016010	3384115497	99487	808.23	634.01	273.53	626.43	8.52	223.94	0.20	85.65	5593.47	-3040.03	2398.92
Saccade R	1	2	3384016010	3384115497	99487	808.23	634.01	273.53	626.43	8.52	223.94	0.20	85.65	5593.47	-3040.03	2398.92
