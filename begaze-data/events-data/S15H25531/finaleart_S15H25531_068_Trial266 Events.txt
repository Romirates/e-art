[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4662182983	# Message: void.jpg
Fixation L	1	1	4662194551	4662691893	497342	1086.14	530.20	12	49	-1	14.96	14.96
Fixation R	1	1	4662194551	4662691893	497342	1086.14	530.20	12	49	-1	15.74	15.74
Blink L	1	1	4662691893	4662811198	119305
Saccade R	1	1	4662691893	4662791281	99388	1076.72	528.74	674.26	557.95	8.70	204.20	0.60	87.51	2986.67	-2896.15	2227.27
Fixation R	1	2	4662791281	4663586852	795571	663.24	552.65	32	46	-1	14.55	14.55
Fixation L	1	2	4662811198	4663586852	775654	662.96	552.52	32	46	-1	13.17	13.17
Saccade L	1	1	4663586852	4663626719	39867	656.32	578.54	854.52	528.89	1.95	108.51	1.00	48.95	2539.65	-2435.34	2293.14
Saccade R	1	2	4663586852	4663626719	39867	656.32	578.54	854.52	528.89	1.95	108.51	1.00	48.95	2539.65	-2435.34	2293.14
Fixation L	1	3	4663626719	4663885213	258494	860.80	533.69	12	25	-1	14.19	14.19
Fixation R	1	3	4663626719	4663885213	258494	860.80	533.69	12	25	-1	15.39	15.39
Blink L	1	2	4663885213	4664044331	159118
Blink R	1	1	4663885213	4664044331	159118
Fixation L	1	4	4664044331	4664183581	139250	920.92	521.42	28	16	-1	14.90	14.90
Fixation R	1	4	4664044331	4664183581	139250	920.92	521.42	28	16	-1	16.69	16.69
