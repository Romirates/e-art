[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3856591769	# Message: void.jpg
Fixation L	1	1	3856597676	3857492627	894951	798.01	594.21	19	61	-1	11.95	11.95
Fixation R	1	1	3856597676	3857492627	894951	798.01	594.21	19	61	-1	12.22	12.22
Blink L	1	1	3857492627	3857651743	159116
Blink R	1	1	3857492627	3857651743	159116
Fixation L	1	2	3857651743	3858586571	934828	804.46	603.96	21	39	-1	13.29	13.29
Fixation R	1	2	3857651743	3858586571	934828	804.46	603.96	21	39	-1	13.79	13.79
