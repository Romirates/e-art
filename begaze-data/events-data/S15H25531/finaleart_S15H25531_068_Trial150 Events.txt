[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4108779433	# Message: void.jpg
Fixation L	1	1	4108787247	4109284456	497209	1148.55	673.61	22	61	-1	13.68	13.68
Fixation R	1	1	4108787247	4109284456	497209	1148.55	673.61	22	61	-1	14.82	14.82
Blink L	1	1	4109284456	4109682312	397856
Blink R	1	1	4109284456	4109682312	397856
Fixation L	1	2	4109682312	4110477910	795598	804.69	635.70	27	69	-1	13.71	13.71
Fixation R	1	2	4109682312	4110477910	795598	804.69	635.70	27	69	-1	15.04	15.04
Saccade L	1	1	4110477910	4110497775	19865	821.46	672.13	822.56	675.30	0.03	3.39	1.00	1.46	74.06	-46.36	46.91
Saccade R	1	1	4110477910	4110497775	19865	821.46	672.13	822.56	675.30	0.03	3.39	1.00	1.46	74.06	-46.36	46.91
Fixation L	1	3	4110497775	4110776261	278486	817.40	663.67	14	30	-1	13.72	13.72
Fixation R	1	3	4110497775	4110776261	278486	817.40	663.67	14	30	-1	15.41	15.41
