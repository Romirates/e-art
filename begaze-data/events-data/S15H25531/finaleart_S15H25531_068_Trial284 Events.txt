[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:54
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4717083605	# Message: void.jpg
Fixation L	1	1	4717091050	4717667881	576831	871.52	509.97	17	80	-1	18.97	18.97
Fixation R	1	1	4717091050	4717667881	576831	871.52	509.97	17	80	-1	19.33	19.33
Saccade L	1	1	4717667881	4717687760	19879	874.83	476.00	873.94	472.74	0.07	5.64	1.00	3.47	63.84	15.00	40.23
Saccade R	1	1	4717667881	4717687760	19879	874.83	476.00	873.94	472.74	0.07	5.64	1.00	3.47	63.84	15.00	40.23
Fixation L	1	2	4717687760	4717866758	178998	869.92	464.02	9	19	-1	15.72	15.72
Fixation R	1	2	4717687760	4717866758	178998	869.92	464.02	9	19	-1	16.07	16.07
Blink L	1	1	4717866758	4718383981	517223
Blink R	1	1	4717866758	4718383981	517223
Fixation L	1	3	4718383981	4719080071	696090	905.05	552.10	23	45	-1	13.52	13.52
Fixation R	1	3	4718383981	4719080071	696090	905.05	552.10	23	45	-1	14.93	14.93
