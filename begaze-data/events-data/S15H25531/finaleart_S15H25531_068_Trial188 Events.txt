[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4292637197	# Message: void.jpg
Fixation L	1	1	4292644206	4294434362	1790156	912.82	500.52	25	74	-1	14.31	14.31
Fixation R	1	1	4292644206	4294434362	1790156	912.82	500.52	25	74	-1	14.19	14.19
Saccade L	1	1	4294434362	4294454249	19887	914.78	471.75	918.09	465.59	0.10	7.07	1.00	4.84	50.98	-88.05	60.57
Saccade R	1	1	4294434362	4294454249	19887	914.78	471.75	918.09	465.59	0.10	7.07	1.00	4.84	50.98	-88.05	60.57
Fixation L	1	2	4294454249	4294633232	178983	918.75	458.95	2	10	-1	13.60	13.60
Fixation R	1	2	4294454249	4294633232	178983	918.75	458.95	2	10	-1	13.69	13.69
