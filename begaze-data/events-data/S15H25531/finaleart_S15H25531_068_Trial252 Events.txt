[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4582159859	# Message: void.jpg
Saccade L	1	1	4582171871	4582251493	79622	965.59	661.72	973.91	673.52	0.52	21.32	1.00	6.54	429.15	0.00	181.84
Fixation R	1	1	4582171871	4584141025	1969154	963.51	697.24	28	59	-1	18.03	18.03
Fixation L	1	1	4582311464	4584141025	1829561	963.07	699.17	28	45	-1	16.89	16.89
