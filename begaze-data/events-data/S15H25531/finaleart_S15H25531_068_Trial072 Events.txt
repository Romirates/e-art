[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3813895843	# Message: void.jpg
Saccade L	1	1	3813896721	3813936483	39762	918.25	521.10	781.86	535.55	1.69	139.30	1.00	42.46	3482.60	-3435.36	1854.30
Saccade R	1	1	3813896721	3813936483	39762	918.25	521.10	781.86	535.55	1.69	139.30	1.00	42.46	3482.60	-3435.36	1854.30
Fixation L	1	1	3813936483	3815129904	1193421	794.31	547.74	27	48	-1	13.64	13.64
Fixation R	1	1	3813936483	3815129904	1193421	794.31	547.74	27	48	-1	15.08	15.08
Saccade R	1	2	3815129904	3815229268	99364	796.07	580.94	779.38	618.39	4.62	128.14	0.20	46.55	3167.83	-2899.72	1961.59
Saccade L	1	2	3815189524	3815229268	39744	779.84	582.02	779.38	618.39	1.90	127.35	1.00	47.85	808.77	-2677.42	1142.20
Fixation L	1	2	3815229268	3815885743	656475	782.05	659.78	13	50	-1	14.46	14.46
Fixation R	1	2	3815229268	3815885743	656475	782.05	659.78	13	50	-1	15.70	15.70
