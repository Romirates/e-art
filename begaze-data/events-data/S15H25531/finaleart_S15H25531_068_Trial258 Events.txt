[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4600435742	# Message: void.jpg
Saccade L	1	1	4600441010	4600500626	59616	951.10	478.63	1023.24	627.20	4.28	150.62	0.33	71.82	2162.01	-2233.53	1454.47
Saccade R	1	1	4600441010	4600500626	59616	951.10	478.63	1023.24	627.20	4.28	150.62	0.33	71.82	2162.01	-2233.53	1454.47
Fixation L	1	1	4600500626	4600798984	298358	1009.37	617.17	71	25	-1	10.84	10.84
Fixation R	1	1	4600500626	4600798984	298358	1009.37	617.17	71	25	-1	11.89	11.89
Saccade L	1	2	4600798984	4600818848	19864	973.88	613.82	971.35	598.03	0.31	32.74	1.00	15.84	-304.88	-414.08	354.82
Saccade R	1	2	4600798984	4600818848	19864	973.88	613.82	971.35	598.03	0.31	32.74	1.00	15.84	-304.88	-414.08	354.82
Fixation L	1	2	4600818848	4600958092	139244	968.47	595.62	9	27	-1	10.78	10.78
Fixation R	1	2	4600818848	4600958092	139244	968.47	595.62	9	27	-1	11.18	11.18
Blink L	1	1	4600958092	4601798801	840709
Blink R	1	1	4600958092	4601798801	840709
Fixation L	1	3	4601798801	4602435272	636471	941.71	453.00	24	16	-1	11.55	11.55
Fixation R	1	3	4601798801	4602435272	636471	941.71	453.00	24	16	-1	12.18	12.18
