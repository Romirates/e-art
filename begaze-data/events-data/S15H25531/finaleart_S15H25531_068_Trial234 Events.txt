[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4527156048	# Message: void.jpg
Fixation L	1	1	4527156282	4527434606	278324	555.56	625.53	8	16	-1	11.64	11.64
Fixation R	1	1	4527156282	4527434606	278324	555.56	625.53	8	16	-1	12.72	12.72
Saccade L	1	1	4527434606	4527454481	19875	555.77	627.88	757.36	582.00	1.08	209.06	1.00	54.55	5217.38	-4762.94	3337.20
Saccade R	1	1	4527434606	4527454481	19875	555.77	627.88	757.36	582.00	1.08	209.06	1.00	54.55	5217.38	-4762.94	3337.20
Fixation L	1	2	4527454481	4527713085	258604	762.56	557.81	11	43	-1	10.77	10.77
Fixation R	1	2	4527454481	4527713085	258604	762.56	557.81	11	43	-1	12.63	12.63
Blink L	1	1	4527713085	4528011457	298372
Blink R	1	1	4527713085	4528011457	298372
Fixation L	1	3	4528011457	4528568299	556842	1062.90	441.45	35	47	-1	13.38	13.38
Fixation R	1	3	4528011457	4528568299	556842	1062.90	441.45	35	47	-1	13.96	13.96
Saccade L	1	2	4528568299	4528608174	39875	1063.81	416.31	822.37	441.27	2.07	126.71	1.00	51.90	3108.19	-2964.52	3004.56
Saccade R	1	2	4528568299	4528608174	39875	1063.81	416.31	822.37	441.27	2.07	126.71	1.00	51.90	3108.19	-2964.52	3004.56
Fixation L	1	4	4528608174	4529145150	536976	822.30	474.60	8	47	-1	13.14	13.14
Fixation R	1	4	4528608174	4529145150	536976	822.30	474.60	8	47	-1	13.97	13.97
