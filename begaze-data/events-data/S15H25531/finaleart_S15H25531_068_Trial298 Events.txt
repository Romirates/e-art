[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4759764927	# Message: void.jpg
Fixation L	1	1	4759775011	4760272354	497343	872.16	456.13	16	27	-1	14.47	14.47
Fixation R	1	1	4759775011	4760272354	497343	872.16	456.13	16	27	-1	15.79	15.79
Blink L	1	1	4760272354	4761112939	840585
Blink R	1	1	4760272354	4761112939	840585
Fixation L	1	2	4761112939	4761749408	636469	873.02	569.96	11	29	-1	11.58	11.58
Fixation R	1	2	4761112939	4761749408	636469	873.02	569.96	11	29	-1	11.86	11.86
