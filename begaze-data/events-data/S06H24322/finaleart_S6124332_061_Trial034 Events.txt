[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:35:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11529180589	# Message: void.jpg
Fixation L	1	1	11529180705	11529856909	676204	1174.69	711.29	36	41	-1	17.75	17.75
Fixation R	1	1	11529180705	11529856909	676204	1174.69	711.29	36	41	-1	17.18	17.18
Saccade L	1	1	11529856909	11529916533	59624	1147.62	715.12	956.44	752.84	2.66	106.05	0.33	44.64	2185.61	-1364.17	1151.43
Saccade R	1	1	11529856909	11529916533	59624	1147.62	715.12	956.44	752.84	2.66	106.05	0.33	44.64	2185.61	-1364.17	1151.43
Fixation L	1	2	11529916533	11531169585	1253052	927.41	767.64	37	30	-1	18.74	18.74
Fixation R	1	2	11529916533	11531169585	1253052	927.41	767.64	37	30	-1	18.55	18.55
