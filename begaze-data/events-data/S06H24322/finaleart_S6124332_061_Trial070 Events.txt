[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:35:49
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11746358074	# Message: void.jpg
Fixation L	1	1	11746366483	11746644836	278353	1624.68	743.23	16	24	-1	18.54	18.54
Fixation R	1	1	11746366483	11746644836	278353	1624.68	743.23	16	24	-1	17.26	17.26
Saccade L	1	1	11746644836	11746684705	39869	1618.38	753.62	1511.54	976.92	2.56	182.85	1.00	64.19	4492.59	-4219.05	2600.49
Saccade R	1	1	11746644836	11746684705	39869	1618.38	753.62	1511.54	976.92	2.56	182.85	1.00	64.19	4492.59	-4219.05	2600.49
Fixation L	1	2	11746684705	11746903446	218741	1540.27	927.62	35	61	-1	19.47	19.47
Fixation R	1	2	11746684705	11746903446	218741	1540.27	927.62	35	61	-1	18.16	18.16
Saccade L	1	2	11746903446	11746963062	59616	1544.73	915.06	1078.29	783.31	5.14	209.85	0.33	86.23	5188.79	-4808.71	3975.43
Saccade R	1	2	11746903446	11746963062	59616	1544.73	915.06	1078.29	783.31	5.14	209.85	0.33	86.23	5188.79	-4808.71	3975.43
Fixation L	1	3	11746963062	11748355348	1392286	1076.84	753.96	17	52	-1	17.90	17.90
Fixation R	1	3	11746963062	11748355348	1392286	1076.84	753.96	17	52	-1	16.64	16.64
