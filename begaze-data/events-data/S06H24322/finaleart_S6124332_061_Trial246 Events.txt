[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12605955551	# Message: void.jpg
Fixation L	1	1	12605956037	12606135008	178971	834.55	303.00	66	25	-1	21.03	21.03
Fixation R	1	1	12605956037	12606135008	178971	834.55	303.00	66	25	-1	20.49	20.49
Saccade L	1	1	12606135008	12606154886	19878	873.75	318.93	871.95	334.72	0.28	24.45	1.00	14.04	399.05	-224.83	282.25
Saccade R	1	1	12606135008	12606154886	19878	873.75	318.93	871.95	334.72	0.28	24.45	1.00	14.04	399.05	-224.83	282.25
Fixation L	1	2	12606154886	12607487429	1332543	878.26	342.77	46	52	-1	21.93	21.93
Fixation R	1	2	12606154886	12607487429	1332543	878.26	342.77	46	52	-1	21.41	21.41
Saccade L	1	2	12607487429	12607507435	20006	851.12	312.58	850.46	309.08	0.09	10.48	1.00	4.51	29.37	-172.01	88.60
Saccade R	1	2	12607487429	12607507435	20006	851.12	312.58	850.46	309.08	0.09	10.48	1.00	4.51	29.37	-172.01	88.60
Fixation L	1	3	12607507435	12607944900	437465	839.36	304.27	27	35	-1	22.48	22.48
Fixation R	1	3	12607507435	12607944900	437465	839.36	304.27	27	35	-1	22.01	22.01
