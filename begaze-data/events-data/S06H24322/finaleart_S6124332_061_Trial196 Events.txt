[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:09
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12402606996	# Message: void.jpg
Fixation L	1	1	12402611168	12402730537	119369	888.28	345.01	16	15	-1	17.82	17.82
Fixation R	1	1	12402611168	12402730537	119369	888.28	345.01	16	15	-1	17.01	17.01
Saccade L	1	1	12402730537	12402750534	19997	901.69	353.42	997.30	528.85	1.34	202.03	1.00	66.99	4992.20	-4873.84	3460.88
Saccade R	1	1	12402730537	12402750534	19997	901.69	353.42	997.30	528.85	1.34	202.03	1.00	66.99	4992.20	-4873.84	3460.88
Fixation L	1	2	12402750534	12403128265	377731	1013.82	586.74	20	78	-1	18.85	18.85
Fixation R	1	2	12402750534	12403128265	377731	1013.82	586.74	20	78	-1	17.86	17.86
Saccade L	1	2	12403128265	12403148252	19987	1015.96	607.39	1016.70	611.57	0.07	6.03	1.00	3.40	126.78	65.56	101.14
Saccade R	1	2	12403128265	12403148252	19987	1015.96	607.39	1016.70	611.57	0.07	6.03	1.00	3.40	126.78	65.56	101.14
Fixation L	1	3	12403148252	12404600164	1451912	1008.45	606.60	18	49	-1	20.27	20.27
Fixation R	1	3	12403148252	12404600164	1451912	1008.45	606.60	18	49	-1	19.23	19.23
