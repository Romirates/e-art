[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:35:55
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11857079579	# Message: void.jpg
Fixation L	1	1	11857079680	11857775757	696077	907.16	472.44	36	61	-1	17.61	17.61
Fixation R	1	1	11857079680	11857775757	696077	907.16	472.44	36	61	-1	18.99	18.99
Saccade L	1	1	11857775757	11857795625	19868	901.24	499.52	899.80	506.59	0.14	9.28	1.00	7.20	109.37	-88.27	82.32
Saccade R	1	1	11857775757	11857795625	19868	901.24	499.52	899.80	506.59	0.14	9.28	1.00	7.20	109.37	-88.27	82.32
Fixation L	1	2	11857795625	11859128291	1332666	897.06	560.68	8	77	-1	17.69	17.69
Fixation R	1	2	11857795625	11859128291	1332666	897.06	560.68	8	77	-1	18.35	18.35
