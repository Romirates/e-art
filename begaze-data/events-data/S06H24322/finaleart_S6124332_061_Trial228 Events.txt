[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12550379136	# Message: void.jpg
Fixation L	1	1	12550384008	12551398301	1014293	1099.81	739.62	23	34	-1	20.34	20.34
Fixation R	1	1	12550384008	12551398301	1014293	1099.81	739.62	23	34	-1	19.48	19.48
Saccade L	1	1	12551398301	12551438062	39761	1084.67	747.49	873.27	721.02	2.12	137.30	0.50	53.36	3362.10	-2575.06	2331.33
Saccade R	1	1	12551398301	12551438062	39761	1084.67	747.49	873.27	721.02	2.12	137.30	0.50	53.36	3362.10	-2575.06	2331.33
Fixation L	1	2	12551438062	12551875655	437593	858.03	690.80	33	36	-1	21.04	21.04
Fixation R	1	2	12551438062	12551875655	437593	858.03	690.80	33	36	-1	19.98	19.98
Saccade L	1	2	12551875655	12551915407	39752	880.53	691.26	993.75	571.27	2.26	102.57	1.00	56.77	2294.36	-1895.16	1748.95
Saccade R	1	2	12551875655	12551915407	39752	880.53	691.26	993.75	571.27	2.26	102.57	1.00	56.77	2294.36	-1895.16	1748.95
Fixation L	1	3	12551915407	12552412743	497336	1009.90	609.07	22	49	-1	20.88	20.88
Fixation R	1	3	12551915407	12552412743	497336	1009.90	609.07	22	49	-1	19.13	19.13
