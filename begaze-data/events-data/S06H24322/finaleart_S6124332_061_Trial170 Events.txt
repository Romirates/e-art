[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:05
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12322622622	# Message: void.jpg
Fixation L	1	1	12322634362	12323111703	477341	1475.87	849.04	49	32	-1	19.13	19.13
Fixation R	1	1	12322634362	12323111703	477341	1475.87	849.04	49	32	-1	17.35	17.35
Saccade L	1	1	12323111703	12323171443	59740	1480.67	863.20	1093.51	718.13	4.93	203.70	0.67	82.46	4985.44	-4231.87	3505.88
Saccade R	1	1	12323111703	12323171443	59740	1480.67	863.20	1093.51	718.13	4.93	203.70	0.67	82.46	4985.44	-4231.87	3505.88
Fixation L	1	2	12323171443	12323390185	218742	1068.53	677.46	31	55	-1	19.05	19.05
Fixation R	1	2	12323171443	12323390185	218742	1068.53	677.46	31	55	-1	18.33	18.33
Blink L	1	1	12323390185	12323648794	258609
Blink R	1	1	12323390185	12323648794	258609
Saccade L	1	2	12323648794	12323728285	79491	932.12	625.67	932.79	640.95	0.75	27.59	1.00	9.42	170.18	-527.40	195.68
Fixation R	1	3	12323648794	12323887409	238615	934.07	636.61	6	21	-1	17.88	17.88
Saccade L	1	3	12323768158	12323788037	19879	937.35	637.33	937.10	637.40	0.09	10.05	0.00	4.65	76.10	-206.92	146.52
Saccade L	1	4	12323807907	12324026651	218744	935.76	636.26	937.18	636.27	14.06	228.90	0.64	64.28	5178.14	-5676.28	1765.68
Saccade R	1	2	12323887409	12323967016	79607	933.94	635.33	934.27	633.71	9.92	228.90	0.75	124.64	5178.14	-5676.28	3730.97
Fixation R	1	4	12323967016	12324623369	656353	928.50	628.03	15	23	-1	16.91	16.91
Fixation L	1	3	12324046519	12324623369	576850	927.85	627.29	13	23	-1	16.80	16.77
