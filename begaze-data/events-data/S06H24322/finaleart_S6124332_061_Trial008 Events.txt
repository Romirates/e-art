[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:35:38
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11449019788	# Message: void.jpg
Fixation L	1	1	11449024940	11449581753	556813	698.25	806.99	80	17	-1	16.94	16.94
Fixation R	1	1	11449024940	11449581753	556813	698.25	806.99	80	17	-1	17.73	17.73
Saccade L	1	1	11449581753	11449740865	159112	770.47	812.95	864.88	690.83	22.61	1105.85	1.00	142.08	26425.97	-2099.77	3345.43
Saccade R	1	1	11449581753	11449740865	159112	770.47	812.95	864.88	690.83	22.61	1105.85	1.00	142.08	26425.97	-2099.77	3345.43
Blink L	1	1	11449740865	11449959731	218866
Blink R	1	1	11449740865	11449959731	218866
Fixation L	1	2	11449959731	11450596204	636473	898.53	677.40	22	49	-1	17.71	17.71
Fixation R	1	2	11449959731	11450596204	636473	898.53	677.40	22	49	-1	17.52	17.52
Saccade L	1	2	11450596204	11450616071	19867	917.83	694.99	950.63	702.43	0.32	34.02	1.00	16.20	831.85	-662.90	615.53
Saccade R	1	2	11450596204	11450616071	19867	917.83	694.99	950.63	702.43	0.32	34.02	1.00	16.20	831.85	-662.90	615.53
Fixation L	1	3	11450616071	11451013927	397856	969.37	718.69	24	26	-1	16.98	16.98
Fixation R	1	3	11450616071	11451013927	397856	969.37	718.69	24	26	-1	16.88	16.88
