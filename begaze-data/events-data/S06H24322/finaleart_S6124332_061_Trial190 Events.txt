[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12384161637	# Message: void.jpg
Fixation L	1	1	12384173396	12384670613	497217	1284.42	470.63	15	32	-1	16.99	16.99
Fixation R	1	1	12384173396	12384690482	517086	1283.53	470.51	31	32	-1	15.94	15.94
Blink L	1	1	12384670613	12384770117	99504
Saccade R	1	1	12384690482	12384730229	39747	1260.53	467.37	1081.90	474.30	3.29	185.02	0.50	82.68	4382.00	-3665.40	3666.67
Fixation R	1	2	12384730229	12384869467	139238	1041.89	498.73	48	35	-1	16.66	16.66
Fixation L	1	2	12384770117	12385406571	636454	1031.73	508.89	23	75	-1	17.36	17.36
Saccade R	1	2	12384869467	12384889349	19882	1035.40	508.31	1034.55	454.30	0.60	56.98	1.00	30.37	1351.48	-1356.43	1300.12
Fixation R	1	3	12384889349	12385406571	517222	1030.93	510.00	23	75	-1	17.20	17.20
Saccade L	1	1	12385406571	12385426442	19871	1018.50	530.26	1019.03	530.88	0.04	3.73	0.00	1.82	16.45	-62.07	31.68
Saccade R	1	3	12385406571	12385426442	19871	1018.50	530.26	1019.03	530.88	0.04	3.73	0.00	1.82	16.45	-62.07	31.68
Fixation L	1	3	12385426442	12386222018	795576	1023.49	540.99	14	22	-1	18.64	18.64
Fixation R	1	4	12385426442	12386222018	795576	1023.49	540.99	14	22	-1	18.12	18.12
