[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:02
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12098302397	# Message: void.jpg
Saccade L	1	1	12098308253	12098387880	79627	322.43	549.43	447.01	636.88	2.10	147.08	1.00	26.37	3588.49	-3254.70	1170.16
Saccade R	1	1	12098308253	12098387880	79627	322.43	549.43	447.01	636.88	2.10	147.08	1.00	26.37	3588.49	-3254.70	1170.16
Fixation L	1	1	12098387880	12099183462	795582	454.39	636.33	25	51	-1	16.78	16.78
Fixation R	1	1	12098387880	12099183462	795582	454.39	636.33	25	51	-1	17.78	17.78
Saccade L	1	2	12099183462	12099302686	119224	469.74	661.11	680.03	792.61	5.49	138.32	0.17	46.04	3413.51	-2558.85	1680.06
Saccade R	1	2	12099183462	12099302686	119224	469.74	661.11	680.03	792.61	5.49	138.32	0.17	46.04	3413.51	-2558.85	1680.06
Fixation L	1	2	12099302686	12100158004	855318	725.34	824.55	54	40	-1	18.45	18.45
Fixation R	1	2	12099302686	12100158004	855318	725.34	824.55	54	40	-1	18.75	18.75
Saccade L	1	3	12100158004	12100177889	19885	733.21	831.57	734.71	838.68	0.09	7.36	1.00	4.42	128.70	-133.62	106.43
Saccade R	1	3	12100158004	12100177889	19885	733.21	831.57	734.71	838.68	0.09	7.36	1.00	4.42	128.70	-133.62	106.43
Fixation L	1	3	12100177889	12100337005	159116	737.43	835.45	5	10	-1	18.27	18.27
Fixation R	1	3	12100177889	12100337005	159116	737.43	835.45	5	10	-1	18.49	18.49
