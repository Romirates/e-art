[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:27
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6677489033	# Message: void.jpg
Saccade L	1	1	6677499049	6677618582	119533	975.89	518.48	1215.54	385.60	4.52	144.11	1.00	37.79	3500.58	-3476.69	1575.34
Saccade R	1	1	6677499049	6677618582	119533	975.89	518.48	1215.54	385.60	4.52	144.11	1.00	37.79	3500.58	-3476.69	1575.34
Fixation L	1	1	6677618582	6678413990	795408	1218.23	357.46	29	60	-1	11.94	11.94
Fixation R	1	1	6677618582	6678413990	795408	1218.23	357.46	29	60	-1	11.23	11.23
Saccade L	1	2	6678413990	6678453744	39754	1204.65	382.97	1108.79	440.04	1.03	72.81	1.00	25.79	1745.92	-1382.22	1203.52
Saccade R	1	2	6678413990	6678453744	39754	1204.65	382.97	1108.79	440.04	1.03	72.81	1.00	25.79	1745.92	-1382.22	1203.52
Fixation L	1	2	6678453744	6678990719	536975	1095.98	451.92	18	24	-1	13.18	13.18
Fixation R	1	2	6678453744	6678990719	536975	1095.98	451.92	18	24	-1	12.54	12.54
Saccade L	1	3	6678990719	6679030570	39851	1092.46	448.51	991.65	619.41	1.68	117.81	0.50	42.26	2876.99	-2874.12	2425.63
Saccade R	1	3	6678990719	6679030570	39851	1092.46	448.51	991.65	619.41	1.68	117.81	0.50	42.26	2876.99	-2874.12	2425.63
Fixation L	1	3	6679030570	6679488055	457485	991.34	626.30	12	16	-1	13.58	13.58
Fixation R	1	3	6679030570	6679488055	457485	991.34	626.30	12	16	-1	13.12	13.12
