[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7781612759	# Message: void.jpg
Fixation L	1	1	7781627664	7781886276	258612	961.60	451.42	11	58	-1	15.98	15.98
Fixation R	1	1	7781627664	7781886276	258612	961.60	451.42	11	58	-1	15.64	15.64
Saccade L	1	1	7781886276	7781945893	59617	957.25	444.14	1363.01	479.97	5.20	216.13	0.33	87.26	5074.16	-4279.73	3445.85
Saccade R	1	1	7781886276	7781945893	59617	957.25	444.14	1363.01	479.97	5.20	216.13	0.33	87.26	5074.16	-4279.73	3445.85
Fixation L	1	2	7781945893	7782085130	139237	1382.56	485.29	45	38	-1	16.11	16.11
Fixation R	1	2	7781945893	7782085130	139237	1382.56	485.29	45	38	-1	15.53	15.53
Saccade L	1	2	7782085130	7782105002	19872	1374.09	500.62	1368.52	516.88	0.21	17.39	1.00	10.70	115.97	40.15	73.61
Saccade R	1	2	7782085130	7782105002	19872	1374.09	500.62	1368.52	516.88	0.21	17.39	1.00	10.70	115.97	40.15	73.61
Fixation L	1	3	7782105002	7783517172	1412170	1344.86	493.04	45	53	-1	15.41	15.41
Fixation R	1	3	7782105002	7783517172	1412170	1344.86	493.04	45	53	-1	14.84	14.84
Saccade L	1	3	7783517172	7783616704	99532	1325.53	512.72	1320.80	527.26	0.49	9.57	0.00	4.94	71.58	-150.00	49.80
Saccade R	1	3	7783517172	7783616704	99532	1325.53	512.72	1320.80	527.26	0.49	9.57	0.00	4.94	71.58	-150.00	49.80
