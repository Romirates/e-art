[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:46
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7824820254	# Message: void.jpg
Fixation L	1	1	7824823825	7825102163	278338	1269.53	851.27	8	38	-1	14.59	14.59
Fixation R	1	1	7824823825	7825102163	278338	1269.53	851.27	8	38	-1	14.35	14.35
Saccade L	1	1	7825102163	7825142031	39868	1265.59	832.87	1140.35	528.45	3.18	277.21	0.50	79.64	6502.96	-6708.76	3641.39
Saccade R	1	1	7825102163	7825142031	39868	1265.59	832.87	1140.35	528.45	3.18	277.21	0.50	79.64	6502.96	-6708.76	3641.39
Fixation L	1	2	7825142031	7826812681	1670650	1131.49	506.44	37	44	-1	14.61	14.61
Fixation R	1	2	7825142031	7826812681	1670650	1131.49	506.44	37	44	-1	15.08	15.08
