[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6983209065	# Message: void.jpg
Fixation L	1	1	6983226087	6983643804	417717	1274.22	532.24	27	31	-1	12.77	12.77
Fixation R	1	1	6983226087	6983643804	417717	1274.22	532.24	27	31	-1	13.29	13.29
Blink L	1	1	6983643804	6983922164	278360
Blink R	1	1	6983643804	6983922164	278360
Fixation L	1	2	6983922164	6985195212	1273048	951.59	271.22	29	33	-1	13.14	13.14
Fixation R	1	2	6983922164	6985195212	1273048	951.59	271.22	29	33	-1	12.11	12.11
