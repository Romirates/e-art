[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6970916899	# Message: void.jpg
Fixation L	1	1	6970934208	6971411542	477334	955.65	731.31	25	69	-1	17.35	17.35
Fixation R	1	1	6970934208	6971411542	477334	955.65	731.31	25	69	-1	16.09	16.09
Blink L	1	1	6971411542	6971630409	218867
Blink R	1	1	6971411542	6971630409	218867
Fixation L	1	2	6971630409	6972903340	1272931	1064.23	562.26	36	57	-1	14.79	14.79
Fixation R	1	2	6971630409	6972903340	1272931	1064.23	562.26	36	57	-1	15.50	15.50
