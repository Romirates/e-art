[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:36
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7238825408	# Message: void.jpg
Fixation L	1	1	7238841906	7239060637	218731	600.70	446.97	6	21	-1	11.46	11.46
Fixation R	1	1	7238841906	7239060637	218731	600.70	446.97	6	21	-1	12.55	12.55
Saccade L	1	1	7239060637	7239100516	39879	602.67	442.78	904.66	439.60	2.78	191.17	0.50	69.75	4675.39	-3977.65	3492.28
Saccade R	1	1	7239060637	7239100516	39879	602.67	442.78	904.66	439.60	2.78	191.17	0.50	69.75	4675.39	-3977.65	3492.28
Fixation L	1	2	7239100516	7239219748	119232	938.96	423.20	46	25	-1	11.45	11.45
Fixation R	1	2	7239100516	7239219748	119232	938.96	423.20	46	25	-1	12.21	12.21
Blink L	1	1	7239219748	7239518125	298377
Blink R	1	1	7239219748	7239478369	258621
Fixation R	1	3	7239478369	7240194440	716071	1052.77	437.00	37	49	-1	12.20	12.20
Fixation L	1	3	7239518125	7240194440	676315	1052.11	436.86	32	49	-1	12.52	12.52
Saccade L	1	2	7240194440	7240293822	99382	1029.42	449.19	929.68	434.21	5.22	254.10	1.00	52.51	28371.07	-499.02	5168.28
Saccade R	1	2	7240194440	7240293822	99382	1029.42	449.19	929.68	434.21	5.22	254.10	1.00	52.51	28371.07	-499.02	5168.28
Blink L	1	2	7240293822	7240492807	198985
Blink R	1	2	7240293822	7240492807	198985
