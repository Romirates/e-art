[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:52
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8111303371	# Message: void.jpg
Fixation L	1	1	8111313503	8111452937	139434	678.03	631.06	8	6	-1	12.42	12.42
Fixation R	1	1	8111313503	8111452937	139434	678.03	631.06	8	6	-1	12.46	12.46
Saccade L	1	1	8111452937	8111492495	39558	675.28	627.36	1083.56	412.40	3.80	239.68	1.00	95.95	5951.52	-5932.95	5747.43
Saccade R	1	1	8111452937	8111492495	39558	675.28	627.36	1083.56	412.40	3.80	239.68	1.00	95.95	5951.52	-5932.95	5747.43
Fixation L	1	2	8111492495	8111751118	258623	1072.23	414.44	19	16	-1	12.53	12.53
Fixation R	1	2	8111492495	8111751118	258623	1072.23	414.44	19	16	-1	12.05	12.05
Blink L	1	1	8111751118	8112148830	397712
Blink R	1	1	8111751118	8112148830	397712
Fixation L	1	3	8112148830	8112586421	437591	1026.40	600.15	19	80	-1	13.84	13.84
Fixation R	1	3	8112148830	8112586421	437591	1026.40	600.15	19	80	-1	13.42	13.42
Saccade L	1	2	8112586421	8112606306	19885	1029.62	569.13	1029.49	554.68	0.29	23.48	1.00	14.70	247.49	-232.82	222.73
Saccade R	1	2	8112586421	8112606306	19885	1029.62	569.13	1029.49	554.68	0.29	23.48	1.00	14.70	247.49	-232.82	222.73
Fixation L	1	4	8112606306	8113302509	696203	1031.62	557.91	16	40	-1	12.74	12.74
Fixation R	1	4	8112606306	8113302509	696203	1031.62	557.91	16	40	-1	12.24	12.24
Saccade L	1	3	8113302509	8113322379	19870	1043.09	556.62	1093.58	559.67	0.43	51.17	1.00	21.52	1234.65	0.00	617.33
Saccade R	1	3	8113302509	8113322379	19870	1043.09	556.62	1093.58	559.67	0.43	51.17	1.00	21.52	1234.65	0.00	617.33
