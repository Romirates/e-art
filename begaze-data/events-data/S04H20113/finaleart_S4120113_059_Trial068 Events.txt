[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:24
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6597550298	# Message: void.jpg
Fixation L	1	1	6597562128	6597780847	218719	953.30	468.75	62	25	-1	11.53	11.53
Fixation R	1	1	6597562128	6597780847	218719	953.30	468.75	62	25	-1	13.78	13.78
Saccade L	1	1	6597780847	6597820607	39760	1009.54	452.51	1410.44	359.83	3.98	293.34	0.50	100.20	7124.59	-7046.32	4650.25
Saccade R	1	1	6597780847	6597820607	39760	1009.54	452.51	1410.44	359.83	3.98	293.34	0.50	100.20	7124.59	-7046.32	4650.25
Fixation L	1	2	6597820607	6598417333	596726	1396.71	376.31	30	66	-1	11.79	11.79
Fixation R	1	2	6597820607	6598417333	596726	1396.71	376.31	30	66	-1	13.18	13.18
Saccade L	1	2	6598417333	6598476941	59608	1379.68	401.50	1173.05	529.82	3.05	145.66	1.00	51.13	3531.56	-3113.39	2443.15
Saccade R	1	2	6598417333	6598476941	59608	1379.68	401.50	1173.05	529.82	3.05	145.66	1.00	51.13	3531.56	-3113.39	2443.15
Fixation L	1	3	6598476941	6599551001	1074060	1157.78	518.67	20	65	-1	13.02	13.02
Fixation R	1	3	6598476941	6599551001	1074060	1157.78	518.67	20	65	-1	14.27	14.27
