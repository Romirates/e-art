[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:45
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7775512175	# Message: void.jpg
Fixation L	1	1	7775521528	7776655211	1133683	1423.67	512.64	44	55	-1	13.45	13.45
Fixation R	1	1	7775521528	7776655211	1133683	1423.67	512.64	44	55	-1	12.96	12.96
Saccade L	1	1	7776655211	7776675078	19867	1399.82	542.17	1404.01	545.99	0.15	12.12	1.00	7.57	58.89	-159.76	81.63
Saccade R	1	1	7776655211	7776675078	19867	1399.82	542.17	1404.01	545.99	0.15	12.12	1.00	7.57	58.89	-159.76	81.63
Fixation L	1	2	7776675078	7777510538	835460	1386.17	557.40	34	24	-1	14.98	14.98
Fixation R	1	2	7776675078	7777510538	835460	1386.17	557.40	34	24	-1	14.55	14.55
