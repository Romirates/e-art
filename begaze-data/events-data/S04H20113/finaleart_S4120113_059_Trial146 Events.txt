[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:34
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7019990893	# Message: void.jpg
Fixation L	1	1	7020002244	7020141640	139396	645.75	451.76	4	38	-1	14.00	14.00
Fixation R	1	1	7020002244	7020141640	139396	645.75	451.76	4	38	-1	13.76	13.76
Saccade L	1	1	7020141640	7020181231	39591	646.68	474.30	421.50	393.40	2.47	164.84	0.50	62.46	3740.21	-3133.94	2759.86
Saccade R	1	1	7020141640	7020181231	39591	646.68	474.30	421.50	393.40	2.47	164.84	0.50	62.46	3740.21	-3133.94	2759.86
Fixation L	1	2	7020181231	7020817734	636503	421.44	363.56	26	72	-1	12.69	12.69
Fixation R	1	2	7020181231	7020817734	636503	421.44	363.56	26	72	-1	12.60	12.60
Saccade L	1	2	7020817734	7020877438	59704	423.10	326.08	741.36	334.68	3.48	162.58	0.67	58.23	3950.18	-3657.00	2390.22
Saccade R	1	2	7020817734	7020877438	59704	423.10	326.08	741.36	334.68	3.48	162.58	0.67	58.23	3950.18	-3657.00	2390.22
Fixation L	1	3	7020877438	7021116051	238613	752.41	346.82	17	26	-1	11.77	11.77
Fixation R	1	3	7020877438	7021116051	238613	752.41	346.82	17	26	-1	11.64	11.64
Blink L	1	1	7021116051	7021434280	318229
Blink R	1	1	7021116051	7021434280	318229
Fixation L	1	4	7021434280	7021991248	556968	895.07	482.08	12	18	-1	14.20	14.20
Fixation R	1	4	7021434280	7021991248	556968	895.07	482.08	12	18	-1	13.46	13.46
