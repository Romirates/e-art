[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:21
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6349138070	# Message: void.jpg
Fixation L	1	1	6349141377	6349479473	338096	589.51	938.37	18	76	-1	11.48	11.48
Fixation R	1	1	6349141377	6349479473	338096	589.51	938.37	18	76	-1	13.01	13.01
Blink L	1	1	6349479473	6349658460	178987
Saccade R	1	1	6349479473	6349519219	39746	592.23	945.08	787.54	951.68	2.72	144.17	1.00	68.39	3399.00	-3043.74	2177.93
Blink R	1	1	6349519219	6349797715	278496
Blink L	1	2	6349678330	6349797715	119385
Fixation L	1	2	6349797715	6351130371	1332656	976.46	374.81	20	75	-1	11.17	11.17
Fixation R	1	2	6349797715	6351130371	1332656	976.46	374.81	20	75	-1	11.95	11.95
