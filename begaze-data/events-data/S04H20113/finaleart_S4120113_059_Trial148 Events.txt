[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:34
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7026177475	# Message: void.jpg
Fixation L	1	1	7026188029	7026287479	99450	957.54	1071.24	56	19	-1	13.51	13.51
Fixation R	1	1	7026188029	7026287479	99450	957.54	1071.24	56	19	-1	12.67	12.67
Saccade L	1	1	7026287479	7026366977	79498	986.44	1076.11	986.64	877.57	3.51	180.21	1.00	44.14	4014.03	-4230.05	1684.87
Saccade R	1	1	7026287479	7026366977	79498	986.44	1076.11	986.64	877.57	3.51	180.21	1.00	44.14	4014.03	-4230.05	1684.87
Fixation L	1	2	7026366977	7026526096	159119	993.04	850.78	24	42	-1	13.41	13.41
Fixation R	1	2	7026366977	7026526096	159119	993.04	850.78	24	42	-1	11.78	11.78
Blink L	1	1	7026526096	7027043313	517217
Blink R	1	1	7026526096	7027043313	517217
Fixation L	1	3	7027043313	7028176996	1133683	863.24	482.63	36	49	-1	12.21	12.21
Fixation R	1	3	7027043313	7028176996	1133683	863.24	482.63	36	49	-1	12.56	12.56
