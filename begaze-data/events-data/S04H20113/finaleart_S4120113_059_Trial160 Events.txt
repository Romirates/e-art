[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7063006985	# Message: void.jpg
Fixation L	1	1	7063023902	7063202877	178975	739.05	708.69	16	48	-1	11.83	11.83
Fixation R	1	1	7063023902	7063202877	178975	739.05	708.69	16	48	-1	12.46	12.46
Saccade L	1	1	7063202877	7063222752	19875	731.53	700.10	736.75	769.78	0.87	70.68	1.00	43.95	835.08	-1544.94	1004.31
Saccade R	1	1	7063202877	7063222752	19875	731.53	700.10	736.75	769.78	0.87	70.68	1.00	43.95	835.08	-1544.94	1004.31
Fixation L	1	2	7063222752	7063322247	99495	765.72	800.44	45	47	-1	11.60	11.60
Fixation R	1	2	7063222752	7063322247	99495	765.72	800.44	45	47	-1	12.32	12.32
Saccade L	1	2	7063322247	7063342138	19891	776.39	798.15	791.37	784.12	0.23	20.76	1.00	11.47	323.67	-369.75	249.39
Saccade R	1	2	7063322247	7063342138	19891	776.39	798.15	791.37	784.12	0.23	20.76	1.00	11.47	323.67	-369.75	249.39
Fixation L	1	3	7063342138	7063461488	119350	788.22	793.38	5	32	-1	11.65	11.65
Fixation R	1	3	7063342138	7063461488	119350	788.22	793.38	5	32	-1	12.40	12.40
Blink L	1	1	7063461488	7063779714	318226
Blink R	1	1	7063461488	7063779714	318226
Fixation L	1	4	7063779714	7064993024	1213310	1011.31	548.31	44	44	-1	11.32	11.32
Fixation R	1	4	7063779714	7064993024	1213310	1011.31	548.31	44	44	-1	11.57	11.57
