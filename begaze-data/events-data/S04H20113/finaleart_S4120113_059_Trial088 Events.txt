[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:27
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6658992611	# Message: void.jpg
Fixation L	1	1	6659001544	6659180522	178978	1482.34	592.11	31	16	-1	14.02	14.02
Fixation R	1	1	6659001544	6659180522	178978	1482.34	592.11	31	16	-1	12.69	12.69
Saccade L	1	1	6659180522	6659200404	19882	1456.31	589.08	1298.19	605.82	1.04	160.80	1.00	52.32	3965.79	-3869.68	2749.31
Saccade R	1	1	6659180522	6659200404	19882	1456.31	589.08	1298.19	605.82	1.04	160.80	1.00	52.32	3965.79	-3869.68	2749.31
Fixation L	1	2	6659200404	6659458999	258595	1309.58	609.14	17	24	-1	14.09	14.09
Fixation R	1	2	6659200404	6659458999	258595	1309.58	609.14	17	24	-1	12.96	12.96
Blink L	1	1	6659458999	6659717611	258612
Blink R	1	1	6659458999	6659717611	258612
Fixation L	1	3	6659717611	6660990540	1272929	987.76	479.06	38	51	-1	13.62	13.62
Fixation R	1	3	6659717611	6660990540	1272929	987.76	479.06	38	51	-1	13.92	13.92
