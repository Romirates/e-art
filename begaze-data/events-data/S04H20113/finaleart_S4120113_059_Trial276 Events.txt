[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7994390111	# Message: void.jpg
Fixation L	1	1	7994401490	7994839030	437540	1342.89	775.07	29	39	-1	15.23	15.23
Fixation R	1	1	7994401490	7994839030	437540	1342.89	775.07	29	39	-1	15.25	15.25
Blink L	1	1	7994839030	7995256768	417738
Blink R	1	1	7994839030	7995256768	417738
Fixation L	1	2	7995256768	7996131826	875058	1075.06	511.71	31	45	-1	11.95	11.95
Fixation R	1	2	7995256768	7996131826	875058	1075.06	511.71	31	45	-1	11.84	11.84
Saccade L	1	1	7996131826	7996151718	19892	1078.66	533.42	1075.51	563.70	0.37	37.85	1.00	18.48	906.94	-478.34	687.59
Saccade R	1	1	7996131826	7996151718	19892	1078.66	533.42	1075.51	563.70	0.37	37.85	1.00	18.48	906.94	-478.34	687.59
Fixation L	1	3	7996151718	7996390453	238735	1071.59	546.01	13	45	-1	12.65	12.65
Fixation R	1	3	7996151718	7996390453	238735	1071.59	546.01	13	45	-1	12.91	12.91
