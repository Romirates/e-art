[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:26
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6634318604	# Message: void.jpg
Saccade L	1	1	6634338277	6634457660	119383	1006.23	438.56	1299.80	467.96	5.15	177.52	0.83	43.12	4221.57	-4028.23	1763.38
Saccade R	1	1	6634338277	6634457660	119383	1006.23	438.56	1299.80	467.96	5.15	177.52	0.83	43.12	4221.57	-4028.23	1763.38
Fixation L	1	1	6634457660	6634795750	338090	1304.46	502.89	15	56	-1	12.70	12.70
Fixation R	1	1	6634457660	6634795750	338090	1304.46	502.89	15	56	-1	12.59	12.59
Blink L	1	1	6634795750	6635153724	357974
Blink R	1	1	6634795750	6635153724	357974
Fixation L	1	2	6635153724	6635770318	616594	1050.68	684.84	43	36	-1	14.14	14.14
Fixation R	1	2	6635153724	6635770318	616594	1050.68	684.84	43	36	-1	13.30	13.30
Blink L	1	2	6635770318	6635929426	159108
Blink R	1	2	6635770318	6635929426	159108
Fixation L	1	3	6635929426	6636307276	377850	1029.64	456.77	28	49	-1	13.25	13.25
Fixation R	1	3	6635929426	6636307276	377850	1029.64	456.77	28	49	-1	12.91	12.91
