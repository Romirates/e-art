[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7744663946	# Message: void.jpg
Fixation L	1	1	7744672520	7745070242	397722	955.08	607.60	11	31	-1	14.83	14.83
Fixation R	1	1	7744672520	7745070242	397722	955.08	607.60	11	31	-1	15.24	15.24
Blink L	1	1	7745070242	7745328842	258600
Blink R	1	1	7745070242	7745328842	258600
Fixation L	1	2	7745328842	7745786336	457494	1030.35	482.95	20	76	-1	12.38	12.38
Fixation R	1	2	7745328842	7745786336	457494	1030.35	482.95	20	76	-1	11.12	11.12
Saccade L	1	1	7745786336	7745806191	19855	1020.20	493.55	1015.23	508.30	0.18	15.75	1.00	9.21	256.29	-212.80	195.79
Saccade R	1	1	7745786336	7745806191	19855	1020.20	493.55	1015.23	508.30	0.18	15.75	1.00	9.21	256.29	-212.80	195.79
Fixation L	1	3	7745806191	7746661523	855332	1013.38	526.22	22	68	-1	14.02	14.02
Fixation R	1	3	7745806191	7746661523	855332	1013.38	526.22	22	68	-1	13.11	13.11
