[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7831043522	# Message: void.jpg
Fixation L	1	1	7831049305	7832004002	954697	972.15	528.26	34	64	-1	12.34	12.34
Fixation R	1	1	7831049305	7832004002	954697	972.15	528.26	34	64	-1	12.94	12.94
Saccade L	1	1	7832004002	7832023870	19868	974.89	563.36	974.49	567.58	0.11	9.01	1.00	5.52	78.56	-49.66	54.12
Saccade R	1	1	7832004002	7832023870	19868	974.89	563.36	974.49	567.58	0.11	9.01	1.00	5.52	78.56	-49.66	54.12
Fixation L	1	2	7832023870	7833038183	1014313	965.00	566.46	21	31	-1	13.84	13.84
Fixation R	1	2	7832023870	7833038183	1014313	965.00	566.46	21	31	-1	14.39	14.39
