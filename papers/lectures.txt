References list:

READ:

Vidal -> [Balbi et al., 2016] Balbi, B., Protti, F., & Montanari, R. Driven by Caravaggio Through His Painting. Cognitive, 2016. 

Vidal -> [Quian Quiroga & Pedreira, 2011] Quian Quiroga, R., & Pedreira, C. (2011). How do we see art: an eye-tracker study. Frontiers in human neuroscience, 5, 98.

Romain -> Behavior
research methods, 45(1), 251-266. Bannier, K., Jain, E., & Meur, O. L. (2018,
June).

ONGOING:

ALL --> Borji, A., & Itti, L. (2013). State-of-the-art in visual attention
modeling. IEEE transactions on pattern analysis and machine intelligence, 35(1),
185-207.


TO READ:

-----> Modèle d'attention visuelle:  Deepcomics: saliency estimation for comics. In Proceedings of the 2018
ACM Symposium on Eye Tracking Research & Applications (p. 49). ACM.

-----> Modèle d'esthétique: Badea, M., Florea, C., Florea, L., & Vertan, C.
(2018). Can we teach computers to understand art? Domain adaptation for
enhancing deep networks capacity to de-abstract art. Image and Vision Computing,
77, 21-32. 
Lemarchand, F. (2018). Fundamental visual features for aesthetic
classification of photographs across datasets. Pattern Recognition Letters.
Deng, Y., Loy, C. C., & Tang, X. (2017). Image aesthetic assessment: An
experimental survey. 
IEEE Signal Processing Magazine, 34(4), 80-106. Lihua, G.,
& Fudi, L. (2015). 
Image aesthetic evaluation using paralleled deep convolution
neural network. arXiv preprint arXiv:1505.05225. Kong, S., Shen, X., Lin, Z.,
Mech, R., & Fowlkes, C. (2016, October). 
Photo aesthetics ranking network with
attributes and content adaptation. In European Conference on Computer Vision
(pp. 662-679). Springer, Cham. Git Hub:
https://github.com/aimerykong/deepImageAestheticsAnalysis (soft dispo)
Computational aesthetic and applications Yihang Bo, Jinhui Yu and Kang Zhang



[Villani et al., 2015] Villani, D., Morganti, F., Cipresso, P., Ruggi, S., Riva,
G., & Gilli, G. (2015). Visual exploration patterns of human figures in action:
an eye tracker study with art paintings. Frontiers in psychology, 6, 1636.

[Walker et al., 2017] Walker, F., Bucker, B., Anderson, N. C., Schreij, D., &
Theeuwes, J. (2017). Looking at paintings in the Vincent Van Gogh Museum: Eye
movement patterns of children and adults. PloS one, 12(6), e0178912.

